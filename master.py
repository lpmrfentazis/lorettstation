from __future__ import annotations
from shared.abstract.threadSafeSingleton import ThreadSafeSingleton
from config.models.configGeneralModel import ConfigGeneralModel
from verboselogs import VerboseLogger

from threading import Thread, Event
from time import sleep
from datetime import datetime

from recievers.MasterReciever import MasterReciever

__version__ = "2.0.0 15.08.2024"


class Master(metaclass=ThreadSafeSingleton): # type: ignore[misc]  
    """
        It's is Singleton
    """
    def __init__(self, config: ConfigGeneralModel, logger: VerboseLogger):
        self.config = config
        self.logger = logger
        
        self.__mainThread: Thread = Thread(name="MasterMainLoop", target=self.__mainThreadLoop)
        self.__stopMainThreadEvent: Event = Event()

        self.masterReciever = MasterReciever(logger=self.logger)
    
    def __mainThreadLoop(self) -> None:
        while not self.__stopMainThreadEvent.is_set():
            try:
                ...
                sleep(self.config.service.masterLoop.sleep)
                
            except KeyboardInterrupt:
                self.stop()
            
            except Exception as e:
                self.logger.error(f"[{type(self).__name__}]: Unknown exception {e}")
                sleep(self.config.service.masterLoop.afterErrorSleep)
    
    def stop(self) -> bool:
        self.__stopMainThreadEvent.set()
        return True
    
    def start(self) -> bool:        
        if self.__mainThread.is_alive():
            self.logger.warning(f"[{type(self).__name__}]: Attempt to start Master.start() while the main thread is still active")
            return False
        
        self.__stopMainThreadEvent.clear()
        
        self.logger.debug(f"[{type(self).__name__}]: start master watching")
        self.__mainThread.start()
        
        return True
    
    def __enter__(self) -> Master:
        self.start()
        
        return self
    
    def __exit__(self, exceptionType: type, exceptionValue: str, exceptionVraceback: str) -> None:
        #Exception handling here
        self.stop()
    
    def __del__(self) -> None:
        self.stop()


def getMasterInstance() -> Master:
    try:
        return Master() # type: ignore[call-arg]
    
    except TypeError:
        raise NotImplementedError("Attempt to get instance of Master before initialization")