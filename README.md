# lorettStation

## Description
Software to ensure the operation of the lorett satellite data receiving station. Provides schedule planning, signal recording, antenna control. It also provides a web interface.

## Installation
Install SoapySDR (https://github.com/pothosware/SoapySDR)
```
git clone https://gitlab.com/lpmrfentazis/lorettstation.git
cd lorettstation
python3 -m pip install -r requirements.txt
```

## Usage
```
python3 server.py
```

## Support
email: lpmrfentazis@mail.ru

## Authors and acknowledgment
MrFentazis - owner

## License
GPLv3 License

