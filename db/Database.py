from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.future import select
from pathlib import Path
from typing import Optional, List
from logging import Logger

import asyncio

from .models import RoleDB, roleByNameDB, roleByIDDB, UserDB, BaseDB, defaultPasswords, defaultUsers



class Database:
    def __init__(self, path: Path | str, *, logger: Optional[Logger] = None, createDefaultUsers: bool = True):
        if isinstance(path, str):
            path = Path(path)

        self.path = path
        self.dbRoot = self.path.absolute()

        self.createDefaultUsers = createDefaultUsers

        self.logger = logger

        self.useLogger = False
        if isinstance(self.logger, Logger):
            self.useLogger = True

        if self.useLogger:
            self.logger.info("Init database")
            self.logger.info(f"DB root: {self.dbRoot}")

        self.db = create_async_engine(f"sqlite+aiosqlite:///{self.dbRoot}", echo=False)
        self.connected = False
            
    async def connect(self) -> None:
        async with self.db.begin() as conn:
            await conn.run_sync(BaseDB.metadata.create_all)
        
        async with AsyncSession(self.db) as session:
            async with session.begin():

                await self.__initRoles()

                if self.createDefaultUsers:
                    if self.useLogger:
                        self.logger.warning(f"You are using the default users. To disable it you need Database(..., createDefaultUsers=False)")
                
                    await self.__initUsers()
        
        self.connected = True
                
        
    async def __initRoles(self) -> None:
        async with AsyncSession(self.db) as session:
            async with session.begin():
                # FIXME
                q = select(RoleDB).filter(RoleDB.name.in_(roleByNameDB.keys()))
                existingRolesNames = await session.execute(q)
                existingRolesNames = [i for i in existingRolesNames.scalars()]
                
                for entry in existingRolesNames:
                    if entry.id != roleByNameDB[entry.name]:
                        if self.useLogger:
                            self.logger.warning(f'Delete "{entry.name}" role duplicate with wrong id {entry.id}')

                        await session.delete(entry)

                q = select(RoleDB).filter(RoleDB.id.in_(roleByIDDB.keys()))
                existingRoles = await session.execute(q)
                existingRoles = [i for i in existingRoles.scalars()]
                
                for entry in existingRoles:
                    if entry.name != roleByIDDB[entry.id]:
                        if self.useLogger:
                            self.logger.warning(f"Wrong role name: replace {entry.__tablename__}.name from {entry.name} to {roleByIDDB[entry.id]}")

                        entry.name = roleByIDDB[entry.id]
                
                # FIXME
                for roleID in filter(lambda x: x not in [i.id for i in existingRoles], roleByIDDB.keys()):
                    if self.useLogger:
                        self.logger.warning(f"Create absent role: {roleByIDDB[roleID]}")

                    session.add(RoleDB(name=roleByIDDB[roleID], id=roleID))

    async def __initUsers(self) -> None:
        async with AsyncSession(self.db) as session:
            async with session.begin():
                q = select(UserDB).filter(UserDB.username.in_(defaultUsers.keys()))
                existingUsers = await session.execute(q)
                existingUsers = [i for i in existingUsers.scalars()]
                
                for user in existingUsers:
                    changed = []

                    if user.name != defaultUsers[user.username].name:
                        changed.append("name")

                        user.name = defaultUsers[user.username].name
                    
                    if not defaultUsers[user.username].checkPassword(defaultPasswords[user.username]):
                        changed.append("password")

                        user.setPassword(defaultPasswords[user.username])

                    if user.roleID != defaultUsers[user.username].roleID:
                        changed.append("roleID")

                        user.roleID = defaultUsers[user.username].roleID

                    if self.useLogger:
                        if len(changed) > 0:
                            self.logger.warning(f"Change {', '.join(changed)} values for {user.username}")
                
                # FIXME
                for _, user in filter(lambda x: x[0] not in [i.username for i in existingUsers], defaultUsers.items()):
                    session.add(user)

                    if self.useLogger:
                        self.logger.warning(f"Create absent default user: {user.username}")

    async def getUsers(self, limit: int = 10, offset: int = 0) -> List[UserDB]:
        if limit > 1000:
            limit = 1000

            if self.useLogger:
                self.logger.warning(f"Database.getUsers limit mast been < 1000, but given {limit}")

        
        if offset < 0:
            offset = 0

            if self.useLogger:
                self.logger.warning(f"Database.getUsers offset mast been > 0, but given {limit}")

        async with AsyncSession(self.db) as session:
            res = await session.scalars(select(UserDB).limit(limit).offset(offset))
            
            return res.all()
        
    async def getRoles(self, limit: int = 10, offset: int = 0) -> List[UserDB]:
        if limit > 1000:
            limit = 1000

            if self.useLogger:
                self.logger.warning(f"Database.getRoles limit mast been < 1000, but given {limit}")

        
        if offset < 0:
            offset = 0

            if self.useLogger:
                self.logger.warning(f"Database.getRoles offset mast been > 0, but given {limit}")

        async with AsyncSession(self.db) as session:
            res = await session.execute(select(RoleDB).limit(limit).offset(offset))
            
            return res.scalars().all()
            
    async def getUserByID(self, id: int) -> Optional[UserDB]:
        async with AsyncSession(self.db) as session:
            res = await session.execute(select(UserDB).filter(UserDB.id == id))
            
            return res.scalars().first()
        
    async def getUserByUsername(self, username: str) -> Optional[UserDB]:
        async with AsyncSession(self.db) as session:
            res = await session.execute(select(UserDB).filter(UserDB.username == username))
            
            return res.scalars().first()
        
    async def getRoleByID(self, id: int) -> Optional[RoleDB]:
        async with AsyncSession(self.db) as session:
            res = await session.execute(select(RoleDB).filter(RoleDB.id == id))
            
            return res.scalars().first()
        
    async def getRoleByName(self, name: int) -> Optional[RoleDB]:
        async with AsyncSession(self.db) as session:
            res = await session.execute(select(RoleDB).filter(RoleDB.name == name))
            
            return res.scalars().all()
            

async def main():
    from logging import StreamHandler, Formatter, DEBUG

    logger = Logger("testDB", DEBUG)
    
    consoleHandler = StreamHandler()
    consoleHandler.setFormatter(Formatter(
            "%(asctime)s [%(levelname)s] - %(message)s"))

    logger.addHandler(consoleHandler)

    path = Path() / "db.sqlite"
    
    try:
        db = Database(path=path, logger=logger)
        await db.connect()
    except OperationalError as e:
        logger.critical(f"sqlalchemy.OperationalError: check sqlite instalition")
        exit(1)

    from pprint import pprint

    print("Roles:")
    pprint(await db.getRoles())

    print("Users:")
    pprint(await db.getUsers())
                        
if __name__ == "__main__":
    asyncio.run(main())