from .Base import BaseDB
from sqlalchemy import Integer, String, DateTime
from sqlalchemy.orm import mapped_column, Mapped
from datetime import datetime

roleByNameDB = {
    "root": -1,
    "admin": 0,
    "observer": 1,
}

roleByIDDB = {roleByNameDB[i]: i for i in roleByNameDB}


class JWTDB(BaseDB):
    __tablename__ = "jwt"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(50), nullable=False, unique=True)

    expire: Mapped[datetime] = mapped_column(DateTime(0), nullable=False)