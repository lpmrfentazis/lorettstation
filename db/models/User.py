from .Base import BaseDB
from .Role import RoleDB, roleByNameDB
from sqlalchemy import Integer, String, ForeignKey, Boolean
from sqlalchemy.orm import mapped_column, Mapped
from sqlalchemy.orm import relationship, backref
from bcrypt import gensalt, checkpw, hashpw



class UserDB(BaseDB):
    __tablename__ = 'users'

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(50), nullable=False)
    username: Mapped[str] = mapped_column(String(50), nullable=False, index=True, unique=True)
    password: Mapped[str] = mapped_column(String(128), nullable=False)
    active: Mapped[bool] = mapped_column(Boolean(), nullable=False, default=True)

    roleID: Mapped[int] = mapped_column(Integer, 
                                        ForeignKey(f"{RoleDB.__tablename__}.id"),
                                        nullable=False, 
                                        default=list(roleByNameDB.values())[-1])

    rank = relationship(RoleDB, backref=backref("users"))

    def setPassword(self, password: str | bytes) -> None:
        salt = gensalt()

        if isinstance(password, str):
            password = password.encode("utf-8")
        
        self.password = hashpw(password, salt=salt).decode("utf-8")

    def checkPassword(self, password: str | bytes) -> bool:
        if isinstance(password, str):
            password = password.encode("utf-8")

        return checkpw(password, str(self.password).encode("utf-8"))


# FIXME
# It's not safe for now,
# I haven't decided how to make it better. 
# In theory, third-party users will not be able to look at this file anyway. 

defaultUsers = {
    "lorett": UserDB(name="lorett", username="lorett", roleID=roleByNameDB["admin"]),
    "observer": UserDB(name="observer", username="observer", roleID=roleByNameDB["observer"]),
}
defaultPasswords = {i: i for i in defaultUsers.keys()}

for user in defaultUsers:
    defaultUsers[user].setPassword(defaultPasswords[user])
