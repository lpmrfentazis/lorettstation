from .Base import BaseDB
from sqlalchemy import Integer, String
from sqlalchemy.orm import mapped_column, Mapped

roleByNameDB = {
    "root": 0,
    "admin": 1,
    "observer": 2,
}

roleByIDDB = {roleByNameDB[i]: i for i in roleByNameDB}


class RoleDB(BaseDB):
    __tablename__ = "roles"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(50), nullable=False, unique=True)