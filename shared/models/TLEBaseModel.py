from annotated_types import Annotated, MinLen, MaxLen
from pydantic import BaseModel

class TLEBaseModel(BaseModel):
    TLE1: Annotated[str, MinLen(69), MaxLen(69)]
    TLE2: Annotated[str, MinLen(69), MaxLen(69)]