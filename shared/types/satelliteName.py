from annotated_types import Annotated, MinLen, MaxLen

# MaxLen(24) like NORAD SATCAT
# https://web.archive.org/web/20100128053054/http://celestrak.com/NORAD/documentation/tle-fmt.asp
SatelliteName = Annotated[str, MinLen(1), MaxLen(24)]