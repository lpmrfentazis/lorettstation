from __future__ import annotations
from fastapi import FastAPI, APIRouter, WebSocket, Depends, Form, HTTPException, status, Query
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.responses import FileResponse
from starlette.responses import RedirectResponse, FileResponse
from starlette.exceptions import HTTPException as StarletteHTTPException

import fastapi
from models import *

from datetime import datetime, timedelta
from pathlib import Path

from typing import Annotated

from Logger import Logger, loggingLevels
from Config import StationConfig, StationConfigFromDict, StationConfigLoader
from Station import Station, __version__
from plugins.PCTelemetry.PCTelemetry import PCTelemetry

import schedule
import asyncio

from time import sleep
from threading import Thread

from db import Database
from db.models import UserDB, RoleDB

from annotated_types import Annotated, Ge, Le, MinLen, MaxLen

from Auth import Auth

import json
    
import uvicorn
import re


def scheduleWhile():
    while True:
        schedule.run_pending()
        sleep(0.5)
        
class App:
    def __init__(self, config: StationConfig):
        self.config = config
        self.logger: Logger = self.config.logger
        self.logger.info(f"lorettStation version: {__version__}")
        
        self.logger.info("Get system info")
        self.pcTelemetry = PCTelemetry()
        self.pcTelemetry.update()
        self.station = None
        
        # FIXME
        schedule.every(2).seconds.do(self.pcTelemetry.update)
        Thread(target=scheduleWhile, name="ScheduleWhile", daemon=True).start()
        
        self.root = Path(__file__).parent
        self.dbPath = self.root / "db.sqlite"
        
        self.db = Database(self.dbPath, logger=self.logger)
        asyncio.run(self.db.connect())

        self.auth = Auth(self.root, self.logger, self.db)

        self.httpBearer = HTTPBearer()

        self.server: FastAPI = FastAPI(swagger_ui_parameters={"syntaxHighlight.theme": "obsidian"})
        
        # It's bad, I'll fix it later
        #FIXME
        async def validateUser(user: UserLoginRequestModel) -> UserDB:    
            userDB = await self.db.getUserByUsername(user.username)
            if userDB and userDB.checkPassword(user.password):
                if not userDB.active:
                    raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, 
                                        detail="User is inactive")
                
                return userDB
            
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, 
                                detail="Invalid username or password")
        self.__validateUser = validateUser
        
        # ===================== api =====================================
        self.apiRouter = APIRouter(prefix="/api/v1", tags=["API"])
        asyncio.run(self.__initAPI())
        
        # ===================== ws =====================================
        self.wsRouter = APIRouter(prefix="/ws")
        asyncio.run(self.__initWS())

        # ===================== jwt =====================================
        self.jwtRouter = APIRouter(prefix="/jwt", tags=["JWT"])
        asyncio.run(self.__initJWT())

        # ===================== redirect =====================================
        self.redirectRouter = APIRouter(tags=["redirect"])
        asyncio.run(self.__initRedirect())
        
        # ===================== pages =====================================
        self.staticPath = self.root / "static"
        self.pagesRouter = APIRouter(tags=["pages"])
        asyncio.run(self.__initPages())
        
        
        self.server.include_router(self.apiRouter)
        self.server.include_router(self.jwtRouter)
        #self.server.include_router(self.wsRouter)   
        #self.server.include_router(self.redirectRouter)  
        self.server.include_router(self.pagesRouter)  
        
        self.server.mount("/", StaticFiles(directory=self.staticPath), "clientStaff")
        #self.server.mount("/", StaticFiles(directory=self.staticPath, html=True), "client")
        #FIXME
        """
            origins = [
                "http://localhost.tiangolo.com",
                "https://localhost.tiangolo.com",
                "http://localhost",
                "http://localhost:8080",
            ]
        """
        origins = ["*"]
        self.server.add_middleware(
            CORSMiddleware,
            allow_origins=origins,
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )
        
    async def __initPages(self):
        self.pagesRouter.add_api_route("/", lambda: FileResponse(self.staticPath / "index.html"), methods=["GET"])
        self.pagesRouter.add_api_route("/{page}", lambda: FileResponse(self.staticPath / "index.html"), methods=["GET"])
    
    async def __initRedirect(self):
        pass
        #self.redirectRouter.add_api_route("/", lambda: RedirectResponse(url="client"), methods=["GET"])
    
    async def __initWS(self):
        async def getLastLog(webSocket: WebSocket):
            await webSocket.accept()

        self.wsRouter.add_websocket_route("/lastLog", getLastLog, "getLastLog")

    async def __initJWT(self) -> None:
        # FIXME
        async def login(user: UserDB = Depends(self.__validateUser)) -> TokenInfoModel:
            token = await self.auth.registerJWT(user)

            return token
        
        async def getCurrentAuthUser(creds: HTTPAuthorizationCredentials = Depends(self.httpBearer)) -> UserInfoModel:
            payload = await self.auth.authJWT(creds.credentials)
            
            if not payload:
                raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authorized")
            
            user = await self.db.getUserByID(payload.sub)
            
            if not user:
                raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid JWT")
            
            if payload.exp < datetime.utcnow():
                raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="JWT outdated")
            
            if user.username != payload.username:
                raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authorized")         
            
            
            role: RoleDB = await self.db.getRoleByID(user.roleID)
            role = role.name

            return UserInfoModel(name=user.name, 
                                 username=user.username, 
                                 role=role,
                                 roleID=user.roleID,
                                 active=user.active)

        # auth
        self.jwtRouter.add_api_route("/login", login, methods=["POST"])
        self.jwtRouter.add_api_route("/user", getCurrentAuthUser, methods=["GET"])

    async def __initAPI(self) -> None:
        async def getRecievedPasses(offset: Annotated[int, Query(ge=0, le=99)] = 0, limit: Annotated[int, Query(ge=1, le=100)] = 1) -> list[TinyRecordLogModel]:
            """
                Returns all available data about accepted sessions. In the TinyRecordLogModel list format
            """
            return self.station.getRecordLogModels(offset, limit, tiny=True)
        
        async def getRecievedPass(passID: Annotated[str, fastapi.Path(min_length=17, max_length=67, regex=r"^\d{8}_\d{6}_.{1,50}$")]) -> RecordLogModel:
            """
                Returns the specified session in RecordLogModel format. The pas ID can be found in TinyRecordLogModel.PassID
            """
            recordLog = self.station.getRecordLogModel(passID)
            
            if recordLog:
                return recordLog
            
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail= f'Pass "{passID}" not found')
            
        async def getSchedule(offset: Annotated[int, Query(ge=0, le=99)] = 0, limit: Annotated[int, Query(ge=1, le=100)] = 10) -> list[ScheduleRecordModel]:
            return self.station.getScheduleModels(offset, limit)   
            
        async def getTelemetrySummary() -> TelemetryModel:
            """
                Returns the full available telemetry. 
                Key names correspond to data blocks, 
                inside there is necessarily a field "type" which specifies the expected display type. 
                Currently only table is available. 
                Also there is always a nested field "values", where the data is stored.

                Fields "position" and "system" are mandatory, the rest are optional and can vary depending on the platform.
            """
            used = self.pcTelemetry.ram.used / 1024 / 1024 / 1024
            total = self.pcTelemetry.ram.total / 1024 / 1024 / 1024
            
            jsonResp = {
                        "position": {
                            "type": "table",
                            "values": {
                                "lat": round(self.station.lat, 4),
                                "lon": round(self.station.lon, 4),
                                "alt": int(self.station.alt * 1000),
                            }
                        }, 
                        "disks": {
                            "type": "table",
                            "values": { i.disk.mountpoint: f"{i.usage.percent} %" for i in self.pcTelemetry.disks}
                        },
                        "temperatures": None,
                        "TLE": {
                            "type": "table",
                            "values": { i.satellite: f"{(i.age / 60 / 60 / 24):.2f} days" for i in self.station.getTLESatellites()}
                        },
                        "system": {
                            "type": "table",
                            "values": {
                                "os": self.pcTelemetry.os,
                                "python": self.pcTelemetry.python,
                                "cpu": self.pcTelemetry.cpu.name,
                                "cpu_usage": self.pcTelemetry.cpu.usage,
                                "RAM": f"{used:.2f}/{total:.2f} GB",
                                "internet": self.pcTelemetry.internet,
                                "version": __version__
                            }
                        },
                        "reciever": None,
                        "orientation": None,
                    }
            if self.station.sdr:
                jsonResp["reciever"] = {
                            "type": "table",
                            "values": {
                                "level": f"{self.station.sdr.level:.2f} dB",
                                "signalLevel": f"{self.station.sdr.signalLevel:.2f} dB",
                            }
                        }
            
            if self.station.connection and self.station.connection.connected:
                jsonResp["orientation"] = {
                            "type": "table",
                            "values": {
                                "azimuth": f"{self.station.connection.lastAzimuth:.2f}°",
                                "elevation": f"{self.station.connection.lastElevation:.2f}°",
                            }}
            
            if len(self.pcTelemetry.temperatures):
                tmp = {}
                    
                for i in self.pcTelemetry.temperatures:
                    summ = sum([i.current for i in self.pcTelemetry.temperatures[i]])
                    tmp[i] = f"{summ / len(self.pcTelemetry.temperatures[i]):.2f} C°"
                        
                jsonResp["temperatures"] = {
                                            "type": "table",
                                            "values": tmp
                                            }
            
            return jsonResp

        async def getPCTelemetry() -> PCTelemetryModel:
            """
                Get full PC telemetry. Like os, cpu and etc.
            """
            return self.pcTelemetry.getDict()
        
        async def getStationRFState() -> RFStateModel:
            """
                Get RF state, last Noise level, lastLevel, lastSNR
            """
            return {"lastNoiseLevel": f"{(self.station.lastNoiseLevel):.1f}",
                    "lastLevel": f"{(self.station.lastLevel):.1f}",
                    "lastSNR": f"{(self.station.lastSNR):.1f}"}
            
        async def getRotatorOrientation() -> OrientationModel:
            # FIXME
            if self.station.connection:
                jsonResp = {"azimuth": f"{self.station.connection.lastAzimuth:.2f}",
                            "elevation": f"{self.station.connection.lastElevation:.2f}",
                            "azimuthCorrection": f"{self.station.azimuthCorrection:.2f}"
                            }
            else:
                jsonResp = {"azimuth": None,
                            "elevation": None,
                            "azimuthCorrection": f"{self.station.azimuthCorrection:.2f}"
                            }
            return jsonResp
        
        async def getStationSettings() -> SettingsModel:
            return SettingsModel(stationName=self.config.stationName,
                                 lat=self.config.lat,
                                 lon=self.config.lon,
                                 alt=self.config.alt,
                                 horizon=self.config.horizon,
                                 minApogee=self.config.minApogee,
                                 azimuthCorrection=f"{self.config.azimuthCorrection:.2f}")
        
        async def putStationSettings(settings: SettingsModel) -> SettingsModel:
            if not(settings.horizon <= settings.minApogee):
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                                    detail=f"horison mast be <= minAporee")
            #FIXME
            self.config.stationName = settings.stationName
            self.config.lat = settings.lat
            self.config.lon = settings.lon
            self.config.alt = settings.alt
            self.config.horizon = settings.horizon
            self.config.minApogee = settings.minApogee
            self.config.azimuthCorrection = settings.azimuthCorrection
            
            self.logger.info(f"Settings been updated by API")
            
            #FIXME
            self.config.save("config.json")
            self.logger.info(f"Settings saved to file")
            
            self.__initStation()
            
            return SettingsModel(stationName=self.config.stationName,
                                 lat=self.config.lat,
                                 lon=self.config.lon,
                                 alt=self.config.alt,
                                 horizon=self.config.horizon,
                                 minApogee=self.config.minApogee,
                                 azimuthCorrection=f"{self.config.azimuthCorrection:.2f}")
        
        async def getStationStatus() -> StatusTableModel:
            # TODO add battery from self.station.connection
            return StatusTableModel(stationName=self.station.stationName,
                                    timemoment=datetime.utcnow(),
                                    status=self.station.status,
                                    battery=None)
            
        async def getSatelliteTrack(satellite: Annotated[str, MinLen(1), MaxLen(50)],
                                    timeStart: datetime,
                                    timeEnd: datetime,
                                    step: Annotated[float, Ge(1), Le(5)]) -> SatelliteTrackModel:
            
            if satellite not in self.station.scheduler.config.satList:
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, 
                                    detail="The satellite is not followed by the station or does not exist")
        
            #TODO DEFINE ME SOMEWHERE
            if (timeEnd - timeStart) > timedelta(minutes=30):
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, 
                                     detail="The difference between timeStart and timeEnd should be less than 30 minutes")
            
            out = self.station.getSatelliteTrack(satellite, timeStart, timeEnd, step)

            if not out:
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY)
            
            return out
        
        async def getSatelliteTrajectory(satellite: Annotated[str, MinLen(1), MaxLen(50)],
                                         timeStart: datetime,
                                         timeEnd: datetime,
                                         step: Annotated[float, Ge(10), Le(60)]) -> SatelliteTrajectoryModel:
            if satellite not in self.station.scheduler.config.satList:
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, 
                                    detail="The satellite is not followed by the station or does not exist")
        
            #TODO DEFINE ME SOMEWHERE
            if (timeEnd - timeStart) > timedelta(hours=4):
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, 
                                     detail="The difference between timeStart and timeEnd should be less than 4 hours")
            
            out = self.station.getSatelliteTrajectory(satellite, timeStart, timeEnd, step)

            if not out:
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY)
            
            return out
        
        async def getLogs(offset: Annotated[int, Query(ge=0)] = 0, limit: Annotated[int, Query(ge=1, Le=300)] = 20) -> list[LogMessageModel]:
            """
                limit - offset must be < 300
                
            Args:
                offset (Annotated[int, Query, optional): _description_. Defaults to 0)]=0.
                limit (Annotated[int, Query, optional): _description_. Defaults to 1)]=20.

            Raises:
                HTTPException: _description_

            Returns:
                list[LogMessageModel]: _description_
            """
            
            #FIXME DEFINE ME?
            if limit - offset > 300:
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                                    detail="limit - offset must be < 300")
                
            #FIXME python > 3.12 must be remove all utc functions
            if offset + limit <= self.logger.messageBufferSize:
                # copy messages with offset and limit
                
                # change the sign in order to take the latest messages
                start = -offset if offset != 0 else -1
                finish = -(offset + limit) - 1
                
                messages = self.logger.memory.buffer[start: finish: -1]
                messages = [LogMessageModel(timeMoment=datetime.utcfromtimestamp(i.created),
                                            level=i.levelname,
                                            levelNumber=i.levelno,
                                            message=i.message) for i in messages]
                
            else:
                messages = []
                
                # ~ "%(asctime)s [%(levelname)s] - %(message)s" logging format 
                regEx = re.compile(r'\[\~\] (?P<timeMoment>.*?) \[(?P<level>.*?)\] - (?P<message>[\s\S]*)')
                
                with open(self.logger.file.baseFilename, "r") as f:
                    # Not the best option, but so be it for now.
                    # It should not conflict with the simultaneous writing of new lines, 
                    # does not lead to a short-term increase in memory usage due to reading the full file content
                    out = []
                    line = ""
                    
                    count = 1
                    
                    for i in f:
                        if count <= 40 and i[:3] != "[~]":
                            line += i
                            count += 1
                            
                        else:
                            if match := re.match(regEx, line):
                                try:
                                    groupDict = match.groupdict()
                                    date = datetime.strptime(groupDict["timeMoment"], "%Y-%m-%d %H:%M:%S,%f")

                                    out.append(LogMessageModel(timeMoment=date,
                                                               level=groupDict["level"],
                                                               levelNumber=loggingLevels[groupDict["level"].lower()],
                                                               message=groupDict["message"]))
                                except Exception as e:
                                    #FIXME
                                    pass

                            line = i
                            count = 1
                        
                
            return messages
        
        async def getLogFile() -> FileResponse:
            return FileResponse(path=self.logger.file.baseFilename, filename=Path(self.logger.file.baseFilename).name, media_type='multipart/form-data')    
    
        async def getStationSatlistFull() -> list[Annotated[str, MinLen(1), MaxLen(50)]]:
            return [i.satellite for i in self.station.recordConfigs.values()]

        async def getStationSatlistFullDetail() -> list[SatelliteModel]:
            #FIXME
            out = []
            for i in self.station.recordConfigs.values():
                orb = self.station.getSatelliteOrbital(i.satellite)
                if orb:
                    out.append(SatelliteModel(name=i.satellite, TLE1=orb.tle._line1, TLE2=orb.tle._line2))
            
            return out
        
        async def getStationSatlistActive() -> list[Annotated[str, MinLen(1), MaxLen(50)]]:
            return self.config.satList

        async def getStationSatlistActiveDetail() -> list[SatelliteModel]:
            out = []
            for i in self.config.satList:
                orb = self.station.getSatelliteOrbital(i)
                if orb:
                    out.append(SatelliteModel(name=i, TLE1=orb.tle._line1, TLE2=orb.tle._line2))
            
            return out
        
        async def putStationSatlistActive(satlist: list[Annotated[str, MinLen(1), MaxLen(50)]]) -> list[Annotated[str, MinLen(1), MaxLen(50)]]:
            supported = [i.satellite for i in self.station.recordConfigs.values()]
            for i in satlist:
                if i not in supported:
                    raise HTTPException(status.HTTP_404_NOT_FOUND,
                                        detail=f"Satellites for active list must be in full list. Not found {i}")
            
            if self.station.updateActiveSatList(satlist):
                return self.station.satList
            
            raise HTTPException(status.HTTP_422_UNPROCESSABLE_ENTITY, detail="Unknown error")

        
        async def putStationPassSettings(scheduleRecord: ScheduleRecordModel) -> ScheduleRecordModel:
            if not (scheduleRecord.trueTimeStart <= scheduleRecord.timeCulmination <= scheduleRecord.trueTimeEnd):
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, 
                                    detail="Wrong datetimes: must be trueTimeStart <= timeCulmination <= trueTimeEnd")
                
            if not (scheduleRecord.trueTimeStart <= scheduleRecord.timeStart <= scheduleRecord.timeEnd <= scheduleRecord.trueTimeEnd):
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, 
                                    detail="Wrong datetimes: must be trueTimeStart <= timeStart <= timeEnd <= trueTimeEnd")
            
            existsSatPass = self.station.getSatPass(satName=scheduleRecord.satName, orbit=scheduleRecord.orbit)
            
            if not existsSatPass:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, 
                                    detail=f"Not found satPass with satName={scheduleRecord.satName} and orbit={scheduleRecord.orbit} in schedule")
                
            if not(scheduleRecord.trueTimeStart == existsSatPass.trueTimeStart and scheduleRecord.trueTimeEnd == existsSatPass.trueTimeEnd):
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, 
                                    detail=f"Wrong datetimes: trueTimeStart and trueTimeEnd must match the same \
                                        satPass fields from the schedule. (perform a get request to the same address)")
            
            existsSatPass.timeStart = scheduleRecord.timeStart
            existsSatPass.timeEnd = scheduleRecord.timeEnd
            existsSatPass.status = scheduleRecord.status
            # timeCulmination and culmination it will be updated automatically
            
            if not self.station.updateSatPass(existsSatPass):
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, 
                                    detail="Incorrect values")
            
            return self.station.getSatPassModel(satName=scheduleRecord.satName, orbit=scheduleRecord.orbit)
        
        async def getScheduleLastUpdate() -> datetime:
            return self.station.lastScheduleUpdate
        
        async def getTLEs() -> list[TLEModel]:
            return self.station.getTLESatellites()
        
        async def getTLE(satellite: Annotated[str, MinLen(1), MaxLen(50)]) -> TLEModel:
            if satellite not in self.station.config.satList:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Satellite not found")
            
            return self.station.getTLESatellite(satellite)
        
        async def getDataExplorerPath(passID: Annotated[str, fastapi.Path(min_length=17, max_length=67, regex=r"^\d{8}_\d{6}_.{1,50}$")]) -> str:
            if (self.station.dataPath / "decoded" / passID).exists():
                return f"decoded/{passID}"
            
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        
        # ============================ Station control
        #FIXME
        async def postRotatorMoveToRelative(orientation: OrientationRelativeModel):
            if self.station.connection is None:
                raise HTTPException(status_code=status.HTTP_501_NOT_IMPLEMENTED, detail="Control via WEB API is not available for the current platform")
            
            if not self.station.connection.navigateRelative(orientation.azimuth, orientation.elevation, correction=False):
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="The action could not be performed")

        async def postRotatorMoveToAbsolute(orientation: OrientationModel):
            if self.station.connection is None:
                raise HTTPException(status_code=status.HTTP_501_NOT_IMPLEMENTED, detail="Control via WEB API is not available for the current platform")
            
            if not self.station.connection.navigate(orientation.azimuth, orientation.elevation, fast=True):
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="The action could not be performed")
        
        async def postRotatorCalibrate():
            if self.station.connection is None:
                raise HTTPException(status_code=status.HTTP_501_NOT_IMPLEMENTED, detail="Control via WEB API is not available for the current platform")
            
            if not self.station.connection.home():
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="The action could not be performed")

        async def postRotatorToStartPosition():
            if self.station.connection is None:
                raise HTTPException(status_code=status.HTTP_501_NOT_IMPLEMENTED, detail="Control via WEB API is not available for the current platform")
            
            if not self.station.connection.comeback():
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="The action could not be performed")

        async def postRotatorStop():
            if self.station.connection is None:
                raise HTTPException(status_code=status.HTTP_501_NOT_IMPLEMENTED, detail="Control via WEB API is not available for the current platform")
            
            if not self.station.connection.stop():
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="The action could not be performed")

            
        async def postSaveRefAzimuth(azimuth: Annotated[float, Ge(-360), Le(360)]):
            if self.station.connection is None:
                raise HTTPException(status_code=status.HTTP_501_NOT_IMPLEMENTED, detail="Control via WEB API is not available for the current platform")
            
            #FIXME
            self.station.config.azimuthCorrection = azimuth - self.station.connection.lastAzimuth
            self.station.connection.azimuthCorrection = self.station.config.azimuthCorrection 
            """
            if not self.station.connection.navigateRelative(azimuth, elevation):
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="The action could not be performed")
            """
        # FIXME tags
        self.apiRouter.add_api_route("/telemetry/computer", getPCTelemetry, methods=["GET"], tags=["telemetry"])
        self.apiRouter.add_api_route("/telemetry/logs", getLogs, methods=["GET"], tags=["telemetry"])
        self.apiRouter.add_api_route("/telemetry/logs/file", getLogFile, methods=["GET"], tags=["telemetry"])
        self.apiRouter.add_api_route("/telemetry/summary", getTelemetrySummary, methods=["GET"], tags=["telemetry"], response_model_exclude_none=True)
        
        self.apiRouter.add_api_route("/station/telemetry/rf", getStationRFState, methods=["GET"], tags=["station", "telemetry"])
        self.apiRouter.add_api_route("/station/telemetry/status", getStationStatus, methods=["GET"], tags=["station", "telemetry"])
        self.apiRouter.add_api_route("/station/telemetry/settings", getStationSettings, methods=["GET"], tags=["station", "telemetry", "settings"])
        self.apiRouter.add_api_route("/station/telemetry/settings", putStationSettings, methods=["PUT"], tags=["station", "telemetry", "settings"])
        self.apiRouter.add_api_route("/station/telemetry/settings/satlist/full", getStationSatlistFull, methods=["GET"], tags=["station", "telemetry", "settings"])
        self.apiRouter.add_api_route("/station/telemetry/settings/satlist/full/detail", getStationSatlistFullDetail, methods=["GET"], tags=["station", "telemetry", "settings"])
        self.apiRouter.add_api_route("/station/telemetry/settings/satlist/active/detail", getStationSatlistActiveDetail, methods=["GET"], tags=["station", "telemetry", "settings"])
        self.apiRouter.add_api_route("/station/telemetry/settings/satlist/active", getStationSatlistActive, methods=["GET"], tags=["station", "telemetry", "settings"])
        self.apiRouter.add_api_route("/station/telemetry/settings/satlist/active", putStationSatlistActive, methods=["PUT"], tags=["station", "telemetry", "settings"], status_code=status.HTTP_201_CREATED)

        self.apiRouter.add_api_route("/rotator/controler/movements/relative", postRotatorMoveToRelative, methods=["POST"], tags=["rotator", "control"])
        self.apiRouter.add_api_route("/rotator/controler/movements/absolute", postRotatorMoveToAbsolute, methods=["POST"], tags=["rotator", "control"])
        self.apiRouter.add_api_route("/rotator/controler/movements/stop", postRotatorStop, methods=["POST"], tags=["rotator", "control"])
        self.apiRouter.add_api_route("/rotator/controler/movements/calibrate", postRotatorCalibrate, methods=["POST"], tags=["rotator", "control"])
        self.apiRouter.add_api_route("/rotator/controler/movements/start_position", postRotatorToStartPosition, methods=["POST"], tags=["rotator", "control"])

        self.apiRouter.add_api_route("/rotator/telemetry/orientation", getRotatorOrientation, methods=["GET"], tags=["rotator", "telemetry"])
        
        self.apiRouter.add_api_route("/records/data/{passID}/explorer/path", getDataExplorerPath, methods=["GET"], tags=["records"])
        self.apiRouter.add_api_route("/records/logs", getRecievedPasses, methods=["GET"], tags=["records"])
        self.apiRouter.add_api_route("/records/logs/{passID}", getRecievedPass, methods=["GET"], tags=["records"])
        
        self.apiRouter.add_api_route("/orbitals/satellites/schedule", getSchedule, methods=["GET"], tags=["orbitals"])
        self.apiRouter.add_api_route("/orbitals/satellites/schedule", putStationPassSettings, methods=["PUT"], tags=["orbitals"], status_code=status.HTTP_201_CREATED)
        self.apiRouter.add_api_route("/orbitals/satellites/schedule/lastUpdate", getScheduleLastUpdate, methods=["GET"], tags=["orbitals"])
        self.apiRouter.add_api_route("/orbitals/satellites/track", getSatelliteTrack, methods=["GET"], tags=["orbitals"])
        self.apiRouter.add_api_route("/orbitals/satellites/trajectory", getSatelliteTrajectory, methods=["GET"], tags=["orbitals"])
        self.apiRouter.add_api_route("/orbitals/satellites/tle", getTLEs, methods=["GET"], tags=["orbitals"])
        self.apiRouter.add_api_route("/orbitals/satellites/tle/{satellite}", getTLE, methods=["GET"], tags=["orbitals"])
    
    def __initStation(self):
        if self.station:
            self.station.stop()
        self.station = None
        
        self.station: Station = Station(config=self.config)
        
        self.station.start()

    def start(self):
        self.__initStation()
        uvicorn.run(self.server, host="0.0.0.0", port=self.config.serverPort, log_level="info")

    def stationStop(self):
        self.station.stop()


def main():
    from Config import StationConfigLoader
    from pathlib import Path

    configPath = Path("config.json")
    config = StationConfigLoader(configPath)

    app = App(config)
    app.start()  


if __name__ == "__main__":
      main()
