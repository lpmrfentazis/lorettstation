from verboselogs import VerboseLogger, SPAM, NOTICE, VERBOSE
from logging import DEBUG, INFO, WARNING, ERROR, CRITICAL, LogRecord
from logging.handlers import MemoryHandler
from datetime import datetime
from pathlib import Path

from typing import List

from time import gmtime

import logging
import coloredlogs


loggingLevels = {
    "spam": SPAM,
    "debug": DEBUG,
    "verbose": VERBOSE,
    "info": INFO,
    "warning": WARNING,
    "error": ERROR,
    "critical": CRITICAL,
    SPAM: "spam",
    DEBUG: "debug",
    VERBOSE: "verbose",
    INFO: "info",
    WARNING: "warning",
    ERROR: "error",
    CRITICAL: "critical"
}


class Logger(VerboseLogger):
    def __init__(self, name: str, level: int = logging.INFO, *, path: Path | str = Path(), messageBufferSize: int = 200):
        super(Logger, self).__init__(name, level)
        
        self.level = level
        
        self.path = Path(path)
        self.path.mkdir(parents=True, exist_ok=True)
        
        fileName = datetime.utcnow().strftime(f"{name}_%d-%m-%Y") + ".log"
        self.logFile = self.path / fileName

        self.messageBufferSize = messageBufferSize
        self.memory = MemoryHandler(messageBufferSize, flushLevel=999, target=None)
        
        self.file = logging.FileHandler(str(self.logFile))
        self.fileformat = logging.Formatter(
            "[~] %(asctime)s [%(levelname)s] - %(message)s")

        self.file.setLevel(self.level)
        self.file.setFormatter(self.fileformat)
        
        super().addHandler(self.file)
        super().addHandler(self.memory)

        coloredlogs.install(level=level, logger=super(),
                            fmt='%(asctime)s [%(levelname)s] - %(message)s')
        
        logging.Formatter.converter = gmtime

        super().info('Start Logger')
        
    def getBuffer(self) -> List[LogRecord]:
        return self.memory.buffer
