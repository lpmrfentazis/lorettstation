from datetime import datetime, timedelta
from annotated_types import Annotated, MinLen, MaxLen
from shared.models.TLEBaseModel import TLEBaseModel

class TLEModel(TLEBaseModel):
    satellite: Annotated[str, MinLen(1), MaxLen(50)]
    epoch: datetime
    age: float
    
    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "satellite": "NOAA 18",
                    "TLE1": "1 28654U 05018A   24028.16958077  .00000310  00000+0  18959-3 0  9996",
                    "TLE2": "2 28654  98.8882 106.2535 0015213 103.0704 257.2167 14.13110473963349",
                    "epoch": "2024-02-11T15:58:55.582087",
                    "age": timedelta(days=1).total_seconds()
                }
            ]
        }
