from pydantic import BaseModel
from datetime import datetime
from annotated_types import MinLen, MaxLen, Annotated, Ge, Le
from typing import Optional

class StatusTableModel(BaseModel):
    stationName: Annotated[str, MinLen(1), MaxLen(50)]
    timemoment: datetime
    status: Annotated[str, MinLen(1), MaxLen(50)]
    battery: Optional[Annotated[float, Ge(0), Le(100)]]

    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "stationName": "test",
                    "timemoment": datetime(2077, 1, 2),
                    "status": "idle",
                    "battery": 100
                }
            ]
        }