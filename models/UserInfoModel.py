from pydantic import BaseModel
from annotated_types import MinLen, MaxLen, Annotated, Ge


class UserInfoModel(BaseModel):
    name: Annotated[str, MinLen(1), MaxLen(50)] = ""
    username: Annotated[str, MinLen(1), MaxLen(50)] = ""
    role: Annotated[str, MinLen(1), MaxLen(50)] = ""
    roleID: Annotated[int, Ge(-1)]
    active: bool = False

    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "name": "Jonny",
                    "username": "Silverhand",
                    "role": "Rockerboy",
                    "roleID": 0,
                    "active": True
                }
            ]
        }