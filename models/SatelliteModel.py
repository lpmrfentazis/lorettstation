from datetime import datetime
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from pydantic import BaseModel


class SatelliteModel(BaseModel):
    name: Annotated[str, MinLen(1), MaxLen(50)]
    TLE1: Annotated[str, MinLen(69), MaxLen(69)]
    TLE2: Annotated[str, MinLen(69), MaxLen(69)]

