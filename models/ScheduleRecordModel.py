from datetime import datetime
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from pydantic import BaseModel

from typing import Literal

class ScheduleRecordModel(BaseModel):
    satName: Annotated[str, MinLen(1), MaxLen(50)]
    culmination: Annotated[float, Ge(0), Le(90)]
    timeStart: datetime
    timeEnd: datetime
    timeCulmination: datetime
    
    trueCulmination: float
    
    trueTimeStart: datetime
    trueTimeEnd: datetime
    trueTimeCulmination: datetime
    
    orbit: Annotated[int, Ge(0)]
    status: Literal["wait", "collision", "banned", "old", "watching"]
    
    class Config:
        from_attributes = True
        json_schema_extra = {
                    "satName": "METOP-C",
                    "culmination": 16.003940733982727,
                    "timeStart": "2024-02-11T15:54:12.648322",
                    "timeEnd": "2024-02-11T16:03:38.865254",
                    "timeCulmination": "2024-02-11T15:58:55.582087",
                    
                    
                    "trueCulmination": 16.003940733982727,
                    
                    "trueTimeStart": "2024-02-11T15:54:12.648322",
                    "trueTimeEnd": "2024-02-11T16:03:38.865254",
                    "trueTimeCulmination": "2024-02-11T15:58:55.582087",
                    
                    "orbit": 27314,
                    "status": "watching"
                }