from pydantic import BaseModel


class RFStateModel(BaseModel):
    lastNoiseLevel: float
    lastLevel: float
    lastSNR: float
    
    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "lastNoiseLevel": 42.5,
                    "lastLevel": 31.8,
                    "lastSNR": 18.9
                }
            ]
        }