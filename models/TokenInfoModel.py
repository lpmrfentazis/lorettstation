from pydantic import BaseModel
from datetime import datetime
from annotated_types import Annotated, MinLen, MaxLen


class TokenInfoModel(BaseModel):
    token: Annotated[str, MinLen(1), MaxLen(8*1024)]
    tokenType: Annotated[str, MinLen(1), MaxLen(50)] = "Bearer"
    expire: datetime
    
    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "token": "Wake the f*** up samurai",
                    "tokenType": "Bearer",
                    "expire": datetime(2077, 1, 1)
                }
            ]
        }