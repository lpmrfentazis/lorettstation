from datetime import datetime
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from typing import Literal
from pydantic import BaseModel


class LogMessageModel(BaseModel):
    timeMoment: datetime
    level: Literal["SPAM", "DEBUG", "VERBOSE", "INFO", "WARNING", "ERROR", "CRITICAL"]
    levelNumber: Annotated[int, Ge(0), Le(50)]
    message: str
    
    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "timeMoment": "2024-02-20T23:06:12.840620",
                    "level": "VERBOSE",
                    "levelNumber": 15,
                    "message": "Waiting for Update schedule in 6 hours (02:06:13 UTC)"
                    
                }
            ]
        }
