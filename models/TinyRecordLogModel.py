from datetime import datetime
from annotated_types import Annotated, Ge, Le, Lt, MinLen, MaxLen
from pydantic import BaseModel

class PassInfo(BaseModel):
    culmination: Annotated[float, Ge(0), Le(90)]
    timeStart: datetime
    timeEnd: datetime
    TLE1: Annotated[str, MinLen(69), MaxLen(69)]
    TLE2: Annotated[str, MinLen(69), MaxLen(69)]
    orbit: Annotated[int, Ge(0)]

class StationInfo(BaseModel):
    name: Annotated[str, MinLen(1), MaxLen(50)]
    lat: Annotated[float, Ge(-90), Le(90)]
    lon: Annotated[float, Ge(-180), Le(180)]
    alt: Annotated[float, Le(9000)]

class TinyRecordLogModel(BaseModel):
    satellite: Annotated[str, MinLen(1), MaxLen(50)]
    passID: Annotated[str, MinLen(17), MaxLen(67)] # YYYYmmdd_HHMMSS_{satellite}
    passInfo: PassInfo
    stationInfo: StationInfo
    
    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "satellite": "NOAA 18",
                    "passID": "20240129_210819_NOAA 18",
                    "passInfo": {
                        "culmination": 64.15502561151615,
                        "timeStart": "2024-01-28T19:31:03.248768",
                        "timeEnd": "2024-01-28T19:44:05.959735",
                        "TLE1": "1 28654U 05018A   24028.16958077  .00000310  00000+0  18959-3 0  9996",
                        "TLE2": "2 28654  98.8882 106.2535 0015213 103.0704 257.2167 14.13110473963349",
                        "orbit": 12345
                    },
                    "stationInfo": {
                        "name": "planumLite",
                        "lat": 55.6713361,
                        "lon": 37.6253844,
                        "alt": 0.185
                    }
                }
            ]
        }
