from pydantic import BaseModel
from datetime import datetime
from annotated_types import MinLen, MaxLen, Annotated

class TokenPayloadModel(BaseModel):
    sub: int
    username: Annotated[str, MinLen(1), MaxLen(50)]
    exp: datetime
    iat: datetime

    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "sub": 1,
                    "username": "Jonny",
                    "exp": datetime(2077, 1, 2),
                    "iat": datetime(2077, 1, 1)
                }
            ]
        }