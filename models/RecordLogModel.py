from datetime import datetime
from annotated_types import Annotated, Ge, Le, Lt, MinLen, MaxLen
from .TinyRecordLogModel import TinyRecordLogModel
from pydantic import BaseModel

class RecordLogEntry(BaseModel):
    timeMoment: datetime
    azimuth: Annotated[float, Ge(0), Lt(360)]
    elevation: Annotated[float, Ge(0), Le(90)]
    level: float
    snr: Annotated[float, Ge(0)]

class RecordInfo(BaseModel):
    frequency: Annotated[float, Ge(0)]
    frequencyOffset: float
    sampleRate: Annotated[float, Ge(0)]
    gain: int
    recordConfig: Annotated[str, MinLen(1), MaxLen(50)]
    pipeline: Annotated[str, MinLen(1), MaxLen(50)]
    timeFinish: datetime
    log: list[RecordLogEntry]

class RecordLogModel(TinyRecordLogModel):
    """
        A extended version of the TinyRecordLogModel. With recordInfo
    """
    recordInfo: RecordInfo
    
    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "satellite": "NOAA 18",
                    "passID": "20240129_210819_NOAA 18",
                    "passInfo": {
                        "culmination": 64.15502561151615,
                        "timeStart": "2024-01-28T19:31:03.248768",
                        "timeEnd": "2024-01-28T19:44:05.959735",
                        "TLE1": "1 28654U 05018A   24028.16958077  .00000310  00000+0  18959-3 0  9996",
                        "TLE2": "2 28654  98.8882 106.2535 0015213 103.0704 257.2167 14.13110473963349",
                        "orbit": 12345
                    },
                    "stationInfo": {
                        "name": "planumLite",
                        "lat": 55.6713361,
                        "lon": 37.6253844,
                        "alt": 0.185
                    },
                    "recordInfo": {
                        "frequency": 1707000000.0,
                        "frequencyOffset": 0,
                        "sampleRate": 6000000.0,
                        "gain": 18,
                        "recordConfig": "NOAA 18",
                        "pipeline": "noaa_hrpt",
                        "timeFinish": "2024-01-29T12:59:41.074208",
                        "log": [
                            {
                                "timeMoment": "2024-01-28T19:31:32.669271",
                                "azimuth": 173.7360707165441,
                                "elevation": 7.153611131832054,
                                "level": -92.38419869542122,
                                "snr": 0.0
                            },
                            {
                                "timeMoment": "2024-01-28T19:31:33.673453",
                                "azimuth": 173.7635177995838,
                                "elevation": 7.230288687961753,
                                "level": -93.85291212797165,
                                "snr": 0.0
                            },
                            {
                                "timeMoment": "2024-01-28T19:31:34.677950",
                                "azimuth": 173.79110325794827,
                                "elevation": 7.307214845382276,
                                "level": -88.14980259537697,
                                "snr": 0.0
                            }
                        ]
                    }
                }
            ]
        }
