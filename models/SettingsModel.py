from pydantic import BaseModel
from annotated_types import Annotated, Gt, Ge, Le, Lt, MinLen, MaxLen


class SettingsModel(BaseModel):
    stationName: Annotated[str, MinLen(1), MaxLen(50)]
    lat: Annotated[float, Ge(-90), Le(90)]
    lon: Annotated[float, Ge(-180), Le(180)]
    alt: Annotated[int, Le(9000)]
    horizon: Annotated[float, Ge(0), Le(90)]
    minApogee: Annotated[float, Ge(0), Le(90)]
    azimuthCorrection: Annotated[float, Gt(-360), Lt(360)]

    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "stationName": "Your Station",
                    "lat": 12.3456,
                    "lon": 78.9101,
                    "alt": 2000,
                    "horizon": 30.0,
                    "minApogee": 10.0,
                    "azimuthCorrection": -45.0
                }
            ]
        }