from datetime import datetime
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from pydantic import BaseModel


class TrackPositionSubModel(BaseModel):
    timeMoment: datetime
    elevation: Annotated[float, Ge(-90), Le(90)]
    azimuth: Annotated[float, Ge(0), Le(360)]

class SatelliteTrackModel(BaseModel):
    
    satellite: Annotated[str, MinLen(1), MaxLen(50)]
    timeStart: datetime
    timeEnd: datetime
    step: Annotated[float, Ge(1), Le(5)]
    track: list[TrackPositionSubModel]
    
    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "satellite": "NOAA 18",
                    "timeStart": "2024-01-28T19:31:03.248768",
                    "timeEnd": "2024-01-28T19:44:05.959735",
                    "track": [
                        {
                            "timeMoment": "2024-01-28T19:31:32.669271",
                            "azimuth": 173.8,
                            "elevation": 7.2
                        },
                        {
                            "timeMoment": "2024-01-28T19:31:33.673453",
                            "azimuth": 174.2,
                            "elevation": 8.5
                        }
                    ]
                    
                }
            ]
        }
