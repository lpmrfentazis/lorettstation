from pydantic import BaseModel
from typing import Optional
from annotated_types import Annotated, Ge, Lt, Le


class OrientationModel(BaseModel):
    azimuth: Optional[Annotated[float, Ge(0), Lt(360)]]
    elevation: Optional[Annotated[float, Ge(0), Le(90)]]
    
    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "azimuth": 33,
                    "elevation": 12.34
                },
                {
                    "azimuth": None,
                    "elevation": None
                }
            ]
        }

class OrientationRelativeModel(BaseModel):
    azimuth: Optional[Annotated[float, Ge(-360), Lt(360)]]
    elevation: Optional[Annotated[float, Ge(-90), Le(90)]]
    
    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "azimuth": 33,
                    "elevation": 12.34
                },
                {
                    "azimuth": None,
                    "elevation": None
                }
            ]
        }