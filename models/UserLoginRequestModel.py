from pydantic import BaseModel
from annotated_types import MinLen, MaxLen, Annotated


class UserLoginRequestModel(BaseModel):
    username: Annotated[str, MinLen(1), MaxLen(50)] = ""
    password: Annotated[str, MinLen(1), MaxLen(50)] = ""

    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "username": "Silverhand",
                    "password": "****"
                }
            ]
        }