from datetime import datetime
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from pydantic import BaseModel


class TrajectoryPositionSubModel(BaseModel):
    timeMoment: datetime
    lat: Annotated[float, Ge(-90), Le(90)]
    lon: Annotated[float, Ge(-180), Le(180)]
    alt: Annotated[float, Ge(0)]

class SatelliteTrajectoryModel(BaseModel):
    
    satellite: Annotated[str, MinLen(1), MaxLen(50)]
    timeStart: datetime
    timeEnd: datetime
    step: Annotated[float, Ge(10), Le(60)]
    trajectory: list[TrajectoryPositionSubModel]
    
    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "satellite": "NOAA 18",
                    "timeStart": "2024-01-28T19:31:03.248768",
                    "timeEnd": "2024-01-28T19:44:05.959735",
                    "trajectory": [
                        {
                            "timeMoment": "2024-01-28T19:31:32.669271",
                            "lat": 54.54321,
                            "lon": 37.34567,
                            "alt": 851.857
                        },
                        {
                            "timeMoment": "2024-01-28T19:31:33.673453",
                            "lat": 54.54332,
                            "lon": 37.34578,
                            "alt": 851.856
                        }
                    ]
                    
                }
            ]
        }
