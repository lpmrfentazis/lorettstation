from pydantic import BaseModel
from typing import Optional, Dict
from annotated_types import Annotated, Gt, Ge, Le, Lt, MinLen, MaxLen


class PositionValuesModel(BaseModel):
    lat: Annotated[float, Ge(-90), Le(90)]
    lon: Annotated[float, Ge(-180), Le(180)]
    alt: Annotated[int, Le(9000)]

class PositionModel(BaseModel):
    type: Annotated[str, MinLen(1), MaxLen(50)]
    values: PositionValuesModel

class SystemValuesModel(BaseModel):
    os: Annotated[str, MinLen(1), MaxLen(50)]
    python: Annotated[str, MinLen(1), MaxLen(80)]
    cpu: Annotated[str, MinLen(1), MaxLen(50)]
    cpu_usage: Annotated[float, Ge(0), Le(100)]
    RAM: Annotated[str, MinLen(1), MaxLen(50)]
    internet: bool
    
class SystemModel(BaseModel):
    type: Annotated[str, MinLen(1), MaxLen(50)]
    values: SystemValuesModel

class DisksModel(BaseModel):
    type: Annotated[str, MinLen(1), MaxLen(50)]
    values: dict[str, str] #TODO Make me float?
    
class TLEsModel(BaseModel):
    type: Annotated[str, MinLen(1), MaxLen(50)]
    values: dict[Annotated[str, MinLen(1), MaxLen(50)], str]
    
class TemperatureModel(BaseModel):
    type: Annotated[str, MinLen(1), MaxLen(50)]
    values: dict[str, str]
    
class TelemetrySummaryModel(BaseModel):
    position: PositionModel
    disks: DisksModel
    temperatures: Optional[TemperatureModel]
    TLE: Optional[TLEsModel]
    system: SystemModel
    
    
    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "position": {
                        "type": "table",
                        "values": {
                            "lat": 12.3456,
                            "lon": 78.9101,
                            "alt": 2000,
                        }
                    },
                    "system": {
                        "type": "table",
                        "values": {
                            "os": "Windows",
                            "python": "3.9",
                            "cpu": "Intel Core i7",
                            "cpu_usage": 30.5,
                            "RAM": "8.00/16.00 GB",
                            "internet": True,
                        }
                    },
                },
                {
                    "position": {
                        "type": "table",
                        "values": {
                            "lat": 12.3456,
                            "lon": 78.9101,
                            "alt": 2000,
                        }
                    },
                    "system": {
                        "type": "table",
                        "values": {
                            "os": "Windows",
                            "python": "3.9",
                            "cpu": "Intel Core i7",
                            "cpu_usage": 30.5,
                            "RAM": "8.00/16.00 GB",
                            "internet": True,
                        }
                    }
                }
            ]
        }