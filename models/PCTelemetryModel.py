from pydantic import BaseModel
from typing import Dict, Optional
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen



class CPUFrequenciesModel(BaseModel):
    min: Annotated[float, Ge(0)]
    max: Annotated[float, Ge(0)]
    current: Annotated[float, Ge(0)]

class CPUModel(BaseModel):
    name: Annotated[str, MinLen(1), MaxLen(50)]
    arch: Annotated[str, MinLen(1), MaxLen(50)]
    usage: Annotated[float, Ge(0), Le(100)]
    frequencies: CPUFrequenciesModel

class RAMModel(BaseModel):
    total: Annotated[int, Ge(0)]
    used: Annotated[int, Ge(0)]
    free: Annotated[int, Ge(0)]
    percent: Annotated[float, Ge(0), Le(100)]

class DiskUsageModel(BaseModel):
    fstype: Annotated[str, MinLen(1), MaxLen(50)]
    total: Annotated[int, Ge(0)]
    used: Annotated[int, Ge(0)]
    free: Annotated[int, Ge(0)]
    percent: Annotated[float, Ge(0), Le(100)]

class TemperatureModel(BaseModel):
    current: Annotated[float, Ge(-273)]

class PCTelemetryModel(BaseModel):
    os: Annotated[str, MinLen(1), MaxLen(50)]
    python: Annotated[str, MinLen(1), MaxLen(50)]
    CPU: CPUModel
    RAM: RAMModel
    disks: Dict[Annotated[str, MinLen(1), MaxLen(50)], DiskUsageModel]
    temperatures: Optional[Dict[Annotated[str, MinLen(1), MaxLen(50)], float]]
    battery: Optional[float]
    internet: bool

    class Config:
        from_attributes = True
        json_schema_extra = {
            "examples":[{
                "os": "Windows 10",
                "python": "3.9",
                "CPU": {
                    "name": "Intel Core i7",
                    "arch": "x86_64",
                    "usage": 25.0,
                    "frequencies": {"min": 2000.0, "max": 4000.0, "current": 3500.0}
                },
                "RAM": {"total": 17179869184, "used": 8589934592, "free": 8589934592, "percent": 50.0},
                "disks": {
                    "C:\\": {"fstype": "NTFS", "total": 1024000000, "used": 512000000, "free": 512000000, "percent": 50.0},
                    "D:\\": {"fstype": "NTFS", "total": 2048000000, "used": 1024000000, "free": 1024000000, "percent": 50.0}
                },
                "temperatures": {"CPU": 60.0, "GPU": 70.0},
                "battery": None,
                "internet": True
            },
            {
            "os": "Windows 11",
                "python": "3.11.9",
                "CPU": {
                    "name": "Intel Core i5",
                    "arch": "x86_64",
                    "usage": 66.0,
                    "frequencies": {"min": 2000.0, "max": 4000.0, "current": 3500.0}
                },
                "RAM": {"total": 17179869184, "used": 8589934592, "free": 8589934592, "percent": 50.0},
                "disks": {
                    "C:\\": {"fstype": "NTFS", "total": 1024000000, "used": 512000000, "free": 512000000, "percent": 50.0},
                    "D:\\": {"fstype": "NTFS", "total": 2048000000, "used": 1024000000, "free": 1024000000, "percent": 50.0}
                },
                "temperatures": None,
                "internet": True
            }]
        }