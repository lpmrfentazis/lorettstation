from __future__ import annotations

from pathlib import Path
from json import dump, load
from dataclasses import dataclass
from pprint import pformat

from Logger import Logger, loggingLevels
from copy import copy

from sys import platform
from os import getenv



config = {
    "stationName": "test",
    "demo": False,
    "alt": 185,
    "lat": 55.6713361,
    "lon": 37.6253844,
    "azimuthCorrection": 0,
    "horizon": 5,
    "minApogee": 10,
    "port": "auto",
    "controller": "Pico - Board CDC",
    "kinematic": "exportTrack",
    "loggingLevel": "INFO",
    "satDumpPath": "C:\\lorett\\satdump\\" if platform == "win32" else "/usr/bin/",
    "dataPath": "C:\\lorett\\data\\" if platform == "win32" else "~/lorett/data/",
    "exportPath": "C:\\lorett\\tracks\\" if platform == "win32" else "~/lorett/lorettstation/tracks/",
    "exportTrackFilename": "TrackF.txt",
    "workingPath": "C:\\lorett\\lorettstation\\" if platform == "win32" else "~/lorett/lorettstation",
    "logPath": "C:\\lorett\\lorettstation\\logs\\" if platform == "win32" else "~/lorett/logs/",
    "scheduleLength": 24,
    "schedulerTimer": 6,
    "timeZone": 3,
    "timestep": 1,
    "beforeStartLimit": 20,
    "band": "L",
    "sdrType": "airspy",
    "source": ["https://celestrak.org/NORAD/elements/gp.php?GROUP=active&FORMAT=tle"],
    "satList": [
        "NOAA 18",
        "NOAA 19",
        "METOP-B",
        "METOP-C",
        "METEOR-M2 2",
        "METEOR-M2 3",
        "METEOR-M2 4"
    ],
    "logURL": "http://eus.lorett.org/eus/log_post",
    "serverPort": 80,
    "statusPort": 8080,
    "deploy": False
}


# It's pretty cumbersome. I may have to redo it in the future.
@dataclass
class StationConfig:
    stationName: str
    demo: bool
    
    alt: int
    lat: float
    lon: float
    
    azimuthCorrection: float
    
    horizon: float
    minApogee: float
    
    port: str
    controller: str
    
    kinematic: str
    
    loggingLevel: int
    
    logger: Logger
    
    satDumpPath: Path
    dataPath: Path
    exportPath: Path
    workingPath: Path
    logPath: Path
    
    band: str
    
    exportTrackFilename: str

    scheduleLength: int
    schedulerTimer: int
    
    timeZone: int
    timestep: float
    
    beforeStartLimit: int
    
    sdrType: str
    source: str
    
    satList: list[str]
    
    logURL: str
    
    statusPort: int
    serverPort: int
    
    deploy: bool
    
    def getDict(self) -> dict:
        return {
                "stationName": self.stationName,
                "demo": self.demo,
                "alt": self.alt,
                "lat": self.lat,
                "lon": self.lon,
                "azimuthCorrection": self.azimuthCorrection,
                "horizon": self.horizon,
                "minApogee": self.minApogee,
                "port": self.port,
                "controller": self.controller,
                "kinematic": self.kinematic,
                "loggingLevel": loggingLevels[self.loggingLevel],
                "satDumpPath": str(self.satDumpPath),
                "dataPath": str(self.dataPath),
                "exportPath": str(self.exportPath),
                "exportTrackFilename": self.exportTrackFilename,
                "workingPath": str(self.workingPath),
                "logPath": str(self.logPath),
                "scheduleLength": self.scheduleLength,
                "schedulerTimer": self.schedulerTimer,
                "timeZone": self.timeZone,
                "timestep": self.timestep,
                "beforeStartLimit": self.beforeStartLimit,
                "band": self.band,
                "sdrType": self.sdrType,
                "source": self.source,
                "satList": self.satList,
                "logURL": self.logURL,
                "statusPort": self.statusPort,
                "serverPort": self.serverPort,
                "deploy": self.deploy
            }
        
    def save(self, path: Path) -> None:
        with open(path, "w") as f:
            self.logger.info(f"Save config to {path}")
            
            dump(self.getDict(), f, indent=4)
            
    
    def __str__(self) -> str:
        return pformat(self.getDict(), indent=4, width=70)
        
        
    def copy(self) -> StationConfig:
        return copy(self)
    
    
    def copyFrom(self, other: StationConfig) -> None:
        # A little black python magic.
        # We copy all fields without specifying them explicitly
        for attr, value in other.__dict__.items():
            setattr(self, attr, value)
        
    
    def check(self, silent: bool = False) -> bool:
        result = 0
        
        if self.stationName == "":
            result += 1
            if not silent:
                self.logger.error(f"Empty station name")
        
        # I was thinking about returning messages with incorrect parameters, 
        # but decided that it was redundant
        # validate coordinates
        if not (-90 <= self.lat <= 90):
            result += 1
            if not silent:
                self.logger.error(f"Incorrect latitude. Most be in [-90, 90], but given {self.lat}")
                
        if not (-180 <= self.lon <= 180):
            result += 1
            if not silent:
                self.logger.error(f"Incorrect longitude. Most be in [-180, 180], but given {self.lon}")
        
        # -431 is dead sea, 9000 is Everest
        if not (-431 <= self.lat <= 9000):
            result += 1
            if not silent:
                self.logger.error(f"Incorrect altitude. Most be in [-431, 9000], but given {self.alt}")
        
        # validate number values 
        if not (self.scheduleLength > 0): 
            result += 1
            if not silent:
                self.logger.error(f"Incorrect scheduleLength. Most be > 0, but given {self.scheduleLength}")
                
        if not (self.schedulerTimer > 0): 
            result += 1
            if not silent:
                self.logger.error(f"Incorrect schedulerTimer. Most be > 0, but given {self.schedulerTimer}")
                
        if not (self.timestep > 0): 
            result += 1
            if not silent:
                self.logger.error(f"Incorrect timestep. Most be > 0, but given {self.timestep}")

        if not (self.beforeStartLimit > 0 and self.beforeStartLimit < 5*60):
            result += 1
            if not silent:
                self.logger.error(f"Incorrect beforeStartLimit. Most be > 0 and < 300, but given {self.beforeStartLimit}")
        
        if not (self.timeZone > 0): 
            result += 1
            if not silent:
                self.logger.error(f"Incorrect timeZone. Most be in [-12, 12], but given {self.timeZone}")
                
        if not (self.minApogee > self.horizon): 
            result += 1
            if not silent:
                self.logger.error(f"Incorrect horizon and minApogee. Most be minApogee > horizon, but given horizon={self.horizon}, minApogee={self.minApogee}")
        
        if not (-360 < self.azimuthCorrection < 360):
            result += 1
            if not silent:
                self.logger.error(f"Incorrect azimuthCorrection. Most be in (-360, 360), but given {self.azimuthCorrection}")
                
        return not result
       
            
class StationConfigFromDict(StationConfig):
    def __init__(self, json: dict) -> None:
        global config
        
        self.missed = tuple(i for i in config.keys() if i not in json.keys())
                
        loggingLevel = loggingLevels[json.get("loggingLevel", config["loggingLevel"]).lower()]
        self.loggingLevel = loggingLevels[getenv(loggingLevel[1:]).lower()] if str(loggingLevel)[0] == '$' else loggingLevel 
                
        logPath = json.get("logPath", config["logPath"])
        self.logPath = Path(getenv(logPath[1:])) if str(logPath)[0] == '$' else Path(logPath)
            
        self.logPath.mkdir(parents=True, exist_ok=True)
                
        stationName = json.get("stationName", config["stationName"])
        self.stationName = getenv(stationName[1:]) if str(stationName)[0] == '$' else stationName
                
        self.logger = Logger(name=self.stationName, path=self.logPath, level=self.loggingLevel)
                
        for i in self.missed:
            self.logger.warning(f"{i} key missed in config. Try use default: {config[i]}")
            json[i] = config[i]
                
        #self.logger.verbose("\n" + pformat(json, indent=4, width=120, sort_dicts=False))                          
        
        satDumpPath = json.get("satDumpPath", config["satDumpPath"])        
        self.satDumpPath = Path(getenv(satDumpPath[1:])) if str(satDumpPath)[0] == '$' else Path(satDumpPath)
        
        if not self.satDumpPath.exists():
            self.logger.warning(f"satDumpPath={self.satDumpPath} is not exists, create.")
            self.satDumpPath.mkdir(parents=True, exist_ok=True)
                
        exportPath = json.get("exportPath", config["exportPath"])
        self.exportPath = Path(getenv(exportPath[1:])) if str(exportPath)[0] == '$' else Path(exportPath)
        
        if not self.exportPath.exists():
            self.logger.warning(f"exportPath={self.exportPath} is not exists, create.")
            self.exportPath.mkdir(parents=True, exist_ok=True)
                
        workingPath = json.get("workingPath", config["workingPath"])
        self.workingPath = Path(getenv(workingPath[1:])) if str(workingPath)[0] == '$' else Path(workingPath)
        
        if not self.workingPath.exists():
            self.logger.warning(f"workingPath={self.workingPath} is not exists, create.")
            self.workingPath.mkdir(parents=True, exist_ok=True)
                    
        dataPath = json.get("dataPath", config["dataPath"])        
        self.dataPath = Path(getenv(dataPath[1:])) if str(dataPath)[0] == '$' else Path(dataPath)
        
        if not self.dataPath.exists():
            self.logger.warning(f"dataPath={self.dataPath} is not exists, create.")
            self.dataPath.mkdir(parents=True, exist_ok=True)
                
        deploy = json["deploy"]
        self.deploy = bool(getenv(deploy[1:])) if str(deploy)[0] == '$' else deploy
        
        self.demo = json["demo"]

        # FIXME    
        self.exportTrackFilename = json.get("exportTrackFilename", config["exportTrackFilename"])

        self.alt = json["alt"]
        self.lat = json["lat"]
        self.lon = json["lon"]
        
        self.beforeStartLimit = json["beforeStartLimit"]
            
        self.azimuthCorrection = json["azimuthCorrection"]
            
        self.horizon = json["horizon"]
        self.minApogee = json["minApogee"]
            
        self.port = json["port"]
        self.controller = json["controller"]
        self.kinematic = json["kinematic"]
        
        self.scheduleLength = json["scheduleLength"]
        self.schedulerTimer = json["schedulerTimer"]
            
        self.timeZone = json["timeZone"]
        self.timestep = json["timestep"]
            
        self.sdrType = json["sdrType"]
        self.source = json["source"]
        
        self.band = json["band"]
            
        self.satList = json["satList"]
            
        self.logURL = json["logURL"]

        statusPort = json["statusPort"]
        self.statusPort = int(getenv(statusPort[1:])) if str(statusPort)[0] == '$' else statusPort
        
        serverPort = json["serverPort"]
        self.serverPort = int(getenv(serverPort[1:])) if str(serverPort)[0] == '$' else serverPort

    
class StationConfigLoader(StationConfig):
    def __init__(self, path: str | Path) -> None:
        global config
        
        self.__path = Path(path)
        
        if not self.__path.exists():
            self.stationName = config["stationName"]
            self.demo = config["demo"]
            self.exportTrackFilename = config["exportTrackFilename"]
            
            self.alt = config["alt"]
            self.lat = config["lat"]
            self.lon = config["lon"]
            
            self.azimuthCorrection = config["azimuthCorrection"]
            
            self.horizon = config["horizon"]
            self.minApogee = config["minApogee"]
            
            self.port = config["port"]
            self.controller = config["controller"]
            self.kinematic = config["kinematic"]
            
            self.loggingLevel = loggingLevels[config["loggingLevel"].lower()]
            self.logPath = Path(config["logPath"])
            
            self.logPath.mkdir(parents=True, exist_ok=True)
            
            self.logger = Logger(name=self.stationName, path=self.logPath, level=self.loggingLevel)
            self.logger.error(f"File {self.__path} is not exists")
            self.logger.info("Try use default config")  
            
            self.logger.verbose("\n" + pformat(config, indent=4, width=120, sort_dicts=False))
            
            self.satDumpPath = Path(config["satDumpPath"])
            self.satDumpPath.mkdir(parents=True, exist_ok=True)
            
            self.exportPath = Path(config["exportPath"])
            self.exportPath.mkdir(parents=True, exist_ok=True)
            
            self.workingPath = Path(config["workingPath"])
            self.workingPath.mkdir(parents=True, exist_ok=True)
            
            self.dataPath = Path(config["dataPath"])
            self.dataPath.mkdir(parents=True, exist_ok=True)
            
            self.scheduleLength = config["scheduleLength"]
            self.schedulerTimer = config["schedulerTimer"]
            
            self.timeZone = config["timeZone"]
            self.timestep = config["timestep"]
            
            self.beforeStartLimit = config["beforeStartLimit"]
            
            self.sdrType = config["sdrType"]
            self.source = config["source"]
            
            self.band = config["band"]
            
            self.satList = config["satList"]
            
            self.logURL = config["logURL"]
            
            self.deploy = config["deploy"]
            
            self.statusPort = config["statusPort"]
            self.serverPort = config["serverPort"]
            
            self.save(self.__path)
            
        else:
            with open(self.__path, "r") as f:
                fromJson = load(f)
                
                config = StationConfigFromDict(fromJson)
                
                if not config.check():
                    config.logger.critical("Incorrect config. Cannot be corrected automatically. Exit")
                    exit(-1)
                
                self.copyFrom(config)
                
                if len(config.missed):
                    self.save(self.__path)