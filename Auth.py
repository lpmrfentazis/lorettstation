from logging import Logger
from db import Database
from db.models import UserDB
from pathlib import Path

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives.asymmetric.types import PrivateKeyTypes, PublicKeyTypes
from cryptography.hazmat.primitives import hashes

from models import TokenInfoModel

from datetime import datetime, timedelta
from typing import Optional

from models import TokenPayloadModel
from pydantic import ValidationError

import asyncio

import jwt


class Auth:
    def __init__(self, root: Path | str, logger: Logger , db: Database, tokenExpire: timedelta = timedelta(hours=48)):
        self.root = Path(root)
        self.logger = logger

        self.certPath = self.root / "certs"
        self.privateFileName = 'jwtPrivate.pem'
        self.publicFileName = 'jwtPublic.pem'

        self.certPath.mkdir(parents=True, exist_ok=True)
        
        self.db = db

        self.tokenExpire = tokenExpire
        
        if not self.db.connected:
            asyncio.run(self.db.connect())

        self.__initCert()

    def __initCert(self) -> None:
        if (self.certPath / self.publicFileName).exists() and (self.certPath / self.privateFileName).exists():
            
            with open(self.certPath / self.privateFileName, "rb") as f:
                pem = f.read()

            private = serialization.load_pem_private_key(
                pem,
                password=None,
            )
            
            with open(self.certPath / self.publicFileName, "rb") as f:
                public = f.read()

            if self.__verifyRS256Keys(privateKey=private, publicKey=public):
                self.privateKey = private
                self.privateKeyBytes = pem
                
                self.publicKey = private.public_key()
                self.publicKeyBytes = public

                return

        self.__generateCerts()

    def __verifyRS256Keys(self, privateKey: bytes | PrivateKeyTypes, publicKey: bytes | PublicKeyTypes) -> bool:
        if isinstance(privateKey, PrivateKeyTypes):
            privateKey = privateKey.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.PKCS8,
                encryption_algorithm=serialization.NoEncryption()
            )

        if isinstance(publicKey, PublicKeyTypes):
            publicKey = publicKey.public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo
            )

        public = serialization.load_pem_public_key(
            publicKey,
            backend=default_backend()
        )

        private = serialization.load_pem_private_key(
            privateKey,
            password=None,  # Если ключ без пароля
            backend=default_backend()
        )

        message = b"Test message"
        signature = private.sign(
            message,
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH
            ),
            hashes.SHA256()
        )

        try:
            public.verify(
                signature,
                message,
                padding.PSS(
                    mgf=padding.MGF1(hashes.SHA256()),
                    salt_length=padding.PSS.MAX_LENGTH
                ),
                hashes.SHA256()
            )

            self.logger.info(f"JWT certificates have been verified")
            
            return True
        
        except Exception as e:
            self.logger.warning(f"JWT certificates could not be verified: {e}")
            
            return False

    def __generateCerts(self) -> None:
        self.privateKey = rsa.generate_private_key(
                                public_exponent=65537,
                                key_size=2048,
                                backend=default_backend()
                            )
            
        self.publicKey = self.privateKey.public_key()

        self.privateKeyBytes = self.privateKey.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        )

        with open(self.certPath / self.privateFileName, 'wb') as f:
            f.write(self.privateKeyBytes)

        self.publicKeyBytes = self.publicKey.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )

        with open(self.certPath / self.publicFileName, "wb") as f:
            f.write(self.publicKeyBytes)

        self.logger.info(f"JWT cerificates have been generated")

    async def authJWT(self, token: str) -> Optional[TokenInfoModel]:
        try:
            payload = jwt.decode(token, 
                                    self.publicKeyBytes,  
                                    algorithms=["RS256"], options={"verify_exp": False})
            
            # DeprecationWarning: datetime.datetime.utcnow() 
            # is deprecated and scheduled for removal in a future version.
            # Use timezone-aware objects to represent datetimes in UTC: datetime.datetime.now(datetime.UTC)
            # FIXME
            exp = payload.get("exp", None)
            if exp:
                exp = datetime.utcfromtimestamp(exp)
                
            iat = payload.get("iat", None)
            if iat:
                iat = datetime.utcfromtimestamp(iat)
            
            payload = TokenPayloadModel(sub=payload.get("sub", None),
                                        username=payload.get("username", None),
                                        exp=exp,
                                        iat=iat)
            
        except AttributeError:
            # AttributeError when do pytz.utc.localize with not datetime object
            return None
        
        except (jwt.exceptions.DecodeError, ValidationError):
            return None
        
        return payload 


    async def registerJWT(self, user: UserDB) -> TokenInfoModel:
        now = datetime.utcnow()

        expire = now + self.tokenExpire

        payload = {
            "sub": user.id,
            "username": user.username,
            "exp": expire,
            "iat": now
        }
            
        encoded = jwt.encode(payload, self.privateKey, algorithm="RS256")

        return TokenInfoModel(token=encoded, expire=expire)
    