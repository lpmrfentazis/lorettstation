#!/bin/bash

# Determine the path of the current directory
SCRIPT_DIR=$(dirname "$(realpath "$0")")

# Переходим на один уровень выше
CURRENT_DIR=$(dirname "$SCRIPT_DIR")

# Check if main.py and requirements.txt exist in the current directory
if [ -f "$CURRENT_DIR/main.py" ] && [ -f "$CURRENT_DIR/requirements.txt" ]; then
    APPPATH="$CURRENT_DIR"
else
    # Check parent directories until finding main.py and requirements.txt
    PARENT_DIR=$(dirname "$CURRENT_DIR")
    if [ -f "$PARENT_DIR/main.py" ] && [ -f "$PARENT_DIR/requirements.txt" ]; then
        APPPATH="$PARENT_DIR"
    fi
    
fi

if [ -z "$APPPATH" ]; then
    echo "Error: Could not find main.py and requirements.txt in current or parent directories."
    exit 1
fi

cd $APPPATH
echo "Application path: $APPPATH"

# Path to the venv folder
VENVPATH="$APPPATH/venv"

# Check if the venv folder exists
if [ ! -d "$VENVPATH" ]; then
    echo "Creating virtual environment..."
    python3 -m venv "$VENVPATH"
fi

echo "Init git submodule's"
git submodule update --init --recursive

# Activate the virtual environment
source "$VENVPATH/bin/activate"

# Install dependencies from requirements.txt
echo "Installing dependencies..."
pip install -r "$APPPATH/requirements.txt"

echo "Installing submodules dependencies..."
git submodule --quiet foreach --recursive pip install -r requirements.txt

# Run main.py
echo "Running main.py..."
python "$APPPATH/main.py"

# Deactivate the virtual environment
deactivate
