from Connection import *
from Logger import Logger
from lorettOrbital.scheduler import Scheduler, SchedulerConfig, SatPass
from pyorbital.orbital import Orbital, tlefile
from Config import StationConfig
from pathlib import Path
from time import sleep
from threading import Thread, Event, Lock
from requests import post
from requests.exceptions import ConnectionError
from typing import List, Callable, Any
from dataclasses import dataclass
import re
import json
from datetime import timedelta

from models import (RecordLogModel,
                    RecordLogEntry, 
                    TinyRecordLogModel, 
                    ScheduleRecordModel, 
                    SatelliteTrackModel, 
                    TrajectoryPositionSubModel, 
                    SatelliteTrajectoryModel, 
                    TrackPositionSubModel, TLEModel)

from pydantic import ValidationError
from recievers.SDR.SDR import SDR, SDRConfig, supportedBands, RecordLogMessage
from recievers.MasterReciever import MasterReciever

from io import BytesIO

from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from typing import Optional

import asyncio
import shutil
import copy



__version__ = "1.0.0 05.05.2024"

statusesList = ["idle",
                "Init",
                "Recieving",
                "Preparation",
                ]

kinematicsList = ["AE",
                  "XZ",
                  "legacy", # (old style for Rosental systems)
                  "exportTrack", # (new style for Rosental systems)
                  "none"    # if there is no rotary device
                  ]

tasksTypes = ["updateTLE",
               "updateSchedule",
               "checkSchedule",
               "preparation",
               "exportTrack",
               "tracking",
               "calibrateSDR",
               "recordSDR"
               ]

@dataclass
class Task:
    """
        A class describing an task for an task manager
            name: str
            type: str from tasksTypes
            timeMoment: datetime
            target: Callible
    """
    name: str
    type: str
    timeMoment: datetime
    target: Callable
    args: Any
    kwargs: Any


def isNumber(num : str) -> bool:
    isNum = True
    try:
        float(num)
    except ValueError:
        isNum = False
    return isNum

@dataclass
class Coordinates:
    lat: float
    lon: float
    alt: float = 0
    

#FIXME In the future, lorettorbital.The Set Pass will be extended
#TODO Add setters
@dataclass
class SatPassExt(SatPass):
    trueTimeStart: datetime = None
    trueTimeEnd: datetime = None
    trueTimeCulmination: datetime = None
    trueCulmination: float = 0
    
    orbit: int = 0
    modified: bool = False
    
def isModifiedSatPassExt(satPass: SatPassExt):
    return any((satPass.timeStart != satPass.trueTimeStart,
               satPass.timeEnd != satPass.trueTimeEnd,
               satPass.timeCulmination != satPass.trueTimeCulmination,
               satPass.culmination != satPass.trueCulmination,
               satPass.status not in ("wait", "collision")))

def getWaitMessageFor(timeMoment: datetime, event: str):        
        timeLeft = (timeMoment - datetime.utcnow()).total_seconds()
        hours = int(timeLeft // 3600)
        minutes = int(timeLeft % 3600 // 60)
        seconds = int(timeLeft % 3600 % 60)
        
        times = ""
        if hours != 0:
            times += f" {hours} hours"
            
        if minutes != 0:
            times += f" {minutes} minutes"
            
        if seconds != 0:
            times += f" {seconds} seconds"
            
        if times == "":
            times += " now"
            
        return f"Waiting for {event} in" + times + f" ({timeMoment.strftime('%H:%M:%S UTC')})"
   
class AddAERecLogProcessor:
    """
        A functor for RecLogProcessor that adds the calculation of the current az, el satellite  
        
    """
    
    def __init__(self, orb: Orbital, lat: float, lon: float, alt: float) -> None:
        self.orb = orb
        self.lat = lat
        self.lon = lon
        self.alt = alt
        
    def __call__(self, message: RecordLogMessage) -> dict:
        az, el = self.orb.get_observer_look(message.timeMoment, self.lon, self.lat, self.alt)
        
        return RecordLogEntry(timeMoment=message.timeMoment, 
                              azimuth=az, 
                              elevation=el, 
                              level=message.currentLevel, 
                              snr=message.signalLevel).model_dump(mode="json")
        
     
"""
    It turned out to be some kind of bad implementation.. Probably have to completely rewrite later
"""
class TasksManager:
    def __init__(self, logger: Logger, ignoreList: List[str]) -> None:
        self.tasks: List[Task, datetime] = []
        self.threads: List[List[Task, Thread, datetime]] = []
        self.logger = logger

        self.mainThread: Thread = Thread(name="tasksManagerMain", target=self.watchThread, daemon=True)
        
        self.stopEvent: Event = Event()
        self.notPauseEvent: Event = Event()
        
        self.timeTick: float = 0.1
        
        self.ignoreList = ignoreList
        
        self.checkThreadsLock = Lock()
        self.checkTasksLock = Lock()
        
        self.lastMessage = None
        
    def loggingTask(self):
        try:
            with self.checkTasksLock:
                for i in range(len(self.tasks)):
                    if self.tasks[i][0].type in self.ignoreList:
                        continue
                    
                    timeLeft = (self.tasks[i][0].timeMoment - datetime.utcnow()).total_seconds() if self.tasks[i][0].timeMoment else 0
                    
                    if timeLeft <= self.timeTick:
                        self.logger.verbose(f"Waiting for {self.tasks[i][0].name} at now")
                        self.tasks[i][1] = datetime.utcnow()
                        
                    elif 0 < timeLeft <= 60:
                        self.logger.verbose(f"Waiting for {self.tasks[i][0].name} {'in ' + (str(int(timeLeft)) + ' seconds')}")
                        self.tasks[i][1] = datetime.utcnow()
                        break
                    
                    elif (self.tasks[i][1] == None) or (self.tasks[i][1] - datetime.utcnow()).total_seconds() > 300:
                        self.logger.verbose(getWaitMessageFor(self.tasks[i][0].timeMoment, self.tasks[i][0].name))
                        self.tasks[i][1] = datetime.utcnow()
                        
        
        except Exception as e:
            self.logger.warning(f"Exception in logging task: {e}")
            
        self.addTask("loggingTask", type="logging", timeMoment=datetime.utcnow()+timedelta(seconds=1), target=self.loggingTask)                

    def tick(self):       
        with self.checkThreadsLock:
            if len(self.threads):
                for task, thread, lastMessage in filter(lambda x: not x[1].is_alive(), self.threads):
                    self.logger.spam(f"Delete stoped thread: {task.name} - {task.type} - {task.timeMoment.isoformat(sep=' ')}. target={task.target.__name__} args={task.args} kwargs={task.kwargs}")
                
                self.threads = list(filter(lambda x: x[1].is_alive(), self.threads))
            
        with self.checkTasksLock:                    
            if len(self.tasks):
                now = datetime.utcnow()
                
                itsTimeForTask = lambda x: (x[0].timeMoment - now).total_seconds() < self.timeTick
                startedTasks = []
                
                for task, lastMessage in filter(itsTimeForTask, self.tasks):
                    with self.checkThreadsLock:
                        if len(list(filter(lambda x: x[0].name == task.name, self.threads))) == 0:
                            self.threads.append([task, Thread(name=task.name, target=task.target, daemon=True, args=task.args, kwargs=task.kwargs), None])
                            startedTasks.append(task)
                            # It would be a good idea to add a queue for tasks of the same type, 
                            # but not necessary for this work scenario. 
                            # Maybe I will add it later
                            self.logger.spam(f"Start task: {task.name} - {task.type} - {task.timeMoment.isoformat(sep=' ')}. target={task.target.__name__} args={task.args} kwargs={task.kwargs}")
                            self.threads[-1][1].start()
                            
                self.tasks = list(filter(lambda x: x[0] not in startedTasks, self.tasks))
        
        sleep(self.timeTick)

    def addTask(self, name: str, type: str, timeMoment: datetime, target: Callable, *args, **kwargs):
        task = Task(name=name, type=type, timeMoment=timeMoment, target=target, args=args, kwargs=kwargs)
        with self.checkTasksLock:
            self.logger.spam(f"Add task name={task.name} type={task.type} timeMoment={task.timeMoment.isoformat(sep=' ')} target={task.target.__name__} args={task.args} kwargs={task.kwargs}")
            
            for i in range(len(self.tasks)):
                if self.tasks[i][0].timeMoment > task.timeMoment:
                    if i == 0:
                        self.tasks.insert(0, [task, None])
                        break
                    
                    else:
                        self.tasks.insert(i-1, [task, None])
                        break
            else:
                self.tasks.append([task, None])
            

    def watchThread(self):
        while True:
            if self.stopEvent.is_set():
                break
            
            self.notPauseEvent.wait()
            
            self.tick()
                
            sleep(self.timeTick)


    def start(self) -> bool:
        """
            Start the watching eventManager
            return False if watching already started
        """
        self.notPauseEvent.set()
        
        if self.mainThread._is_stopped:
            self.stop()
            
            self.mainThread: Thread = Thread(name="tasksManagerMain", target=self.watchThread, daemon=True)
            self.logger.info("Task manager has been restarted")

        if not self.mainThread.is_alive():
            self.stopEvent.clear()
            self.notPauseEvent.set()
            
            self.addTask("loggingTask", "logging", datetime.utcnow(), target=self.loggingTask)
            
            self.mainThread.start()
            return True

        else:
            return False

    def stop(self):
        self.stopEvent.set()
        self.notPauseEvent.clear()
        
        self.mainThread.join()
        
        for task, thread, date in self.threads:
            thread.join()
        
        self.tasks: List[Task, datetime] = []
        self.threads: List[List[Task, Thread, datetime]] = []
        
    def pause(self):
        self.notPauseEvent.clear()
        
    def resume(self):
        self.stopEvent.clear()
        self.notPauseEvent.set()


class Station:
    def __init__(self, config: StationConfig) -> None:        
        self.lastWatchMessage = None
        
        self.config = config        
        self.logger = config.logger
        
        self.sdr = None
        self.masterReciever =  MasterReciever(logger=self.logger)
        
        self._parseSettings()
        
        self.status = "no init"
        
        self.logger.info(f"Use config: \n{self.config}")
        
        self.schedule: List[SatPassExt] = []
        self.lastScheduleUpdate: datetime = datetime.utcnow()

        self.tasksManager = TasksManager(logger=self.logger, ignoreList=["watching", "checkLogsTask", "logging"])
        self.connection = None

        self.lastLevel = -1
        self.lastNoiseLevel = -1
        self.lastSNR = -1
        
        self.lastWatchMessage = None
        
        self.stopUloadingLogsEvent = Event()
        self.stopTrackingEvent = Event()
        self.stopRecordEvent = Event()
        self.stopWatchingEvent = Event()
        self.stopScheduleUpdateEvent = Event()
        
        self.recordLock = Lock()
        self.trackingLock = Lock()
        self.scheduleLock = Lock()
        

    def _parseSettings(self):
        self.stationName = self.config.stationName
        self.port = self.config.port
        self.lat = self.config.lat
        self.lon = self.config.lon
        self.alt = self.config.alt / 1000
        self.azimuthCorrection = self.config.azimuthCorrection
        self.horizon = self.config.horizon
        self.minApogee = self.config.minApogee
        self.timeZone = self.config.timeZone
        self.logPath = self.config.logPath
        self.workingPath = self.config.workingPath
        self.exportPath = self.config.exportPath
        self.dataPath = self.config.dataPath
        self.schedulerTimer = self.config.schedulerTimer * 60 * 60
        self.scheduleLength = self.config.scheduleLength
        self.demo = self.config.demo
        self.device = self.config.controller
        self.kinematic = self.config.kinematic

        self.satList = self.config.satList
        self.source = self.config.source

        self.timestep = self.config.timestep
        
        self.satDumpPath = self.config.satDumpPath
        self.statusPort = self.config.statusPort
        self.band = self.config.band.upper()
        
        if self.band not in supportedBands.keys():
            self.logger.critical(f"Unsupported band {self.band}")
            exit(1)
            
        #TODO add check satlist and recordConfigs
        #FIXME
        self.recordConfigs = supportedBands[self.band]
        self.sdrConfig = SDRConfig(logger=self.logger, 
                                   lat=self.lat, lon=self.lon, alt=self.alt, 
                                   satDumpPath=Path(self.satDumpPath), statusPort=self.statusPort, 
                                   dataPath=self.dataPath,
                                   recordConfigs=self.recordConfigs,
                                   sdrSource=self.config.sdrType)
        
        self.sdr = SDR(self.sdrConfig)
        self.masterReciever.addReciever(self.sdr)

        # kinematic check
        if self.kinematic not in kinematicsList:
            self.kinematic[kinematicsList[0]]
            self.config.kinematic = kinematicsList[0]

        self.sdrType = self.config.sdrType
        # TODO ADD CHECK SDR
        
        

    def _createSubDirectories(self):
        self.dataPath.mkdir(parents=True, exist_ok=True)

        if (self.kinematic == "legacy"):
            self.exportPath.mkdir(parents=True, exist_ok=True)

        self.logPath.mkdir(parents=True, exist_ok=True)
        self.workingPath.mkdir(parents=True, exist_ok=True)  
        

    def initTask(self, *args, **kwargs):
        self.logger.info("Start init task")
        self._createSubDirectories()     

        stationType = 'rotator'
        if self.kinematic == "legacy":
            stationType = "legacy"

        self.stationType = stationType

        self.status = "init"
        schedulerConfig = SchedulerConfig(stationName=self.stationName,
                                            lat = self.lat,
                                            lon = self.lon,
                                            alt = self.alt,
                                            satList=self.satList, 
                                            horizon=self.horizon, 
                                            minApogee=self.minApogee, 
                                            stationType=self.stationType, 
                                            source=self.source,
                                            
                                            path = self.workingPath,
                                            timeZone = self.timeZone,)

        self.scheduler = Scheduler(config=schedulerConfig,
                                   logger=self.logger)
        if self.kinematic not in ["legacy", "none", "exportTrack"]:
            self.tasksManager.addTask(f"Connection to {self.port}", type="connection", timeMoment=datetime.utcnow(), target=self.connectionTask)
         
        self.status = "idle"
        
        self.tasksManager.addTask(f"Update schedule", type="updateSchedule", timeMoment=datetime.utcnow(), target=self.updateScheduleTask)
        self.tasksManager.addTask(f"checkLogsTask", type="checkLogsTask", timeMoment=datetime.utcnow(), target=self.checkLogsTask)

        
    def connectionTask(self, *args, **kwargs):
        self.connection = Connection(port = self.port, logger=self.logger, azimuthCorrection=self.azimuthCorrection, device = self.device)
            
        return self.connection.connect()
    
    def analyseSchedule(self, schedule: list[SatPassExt], *args, **kwargs) -> list[SatPassExt]:
        # TODO
        newSchedule = schedule.copy()
        
        collisionsIDs = set()

        for i in range(len(schedule)):

            if schedule[i].timeEnd < datetime.utcnow():
                schedule[i].status = "old"
                    
            elif (i > 0) and (schedule[i].timeStart < schedule[i-1].timeEnd):
                collisionsIDs.add(i)
                collisionsIDs.add(i-1)
                schedule[i].status = "collision"
                schedule[i-1].status = "collision"

            else:
                if len(collisionsIDs):
                    maxCulminationID = max(collisionsIDs, key=lambda x: schedule[x].culmination)
                    for i in collisionsIDs:
                        if i != maxCulminationID:
                            schedule[i].status = "banned"

                    schedule[maxCulminationID].status = "wait"

                    collisionsIDs.clear()
        
        return newSchedule
    
    def updateScheduleTask(self, *args, **kwargs):
        try:
            # FIXME
            with self.scheduleLock:
                if (len(self.schedule) == 0 or (self.schedule[0].timeStart - datetime.utcnow()) >= timedelta(minutes=5)):
                    schedule = self.scheduler.getSchedule(timeStart=datetime.utcnow() - timedelta(minutes=30), length=self.scheduleLength, saveSchedule=False, updateTLE=True)
                    
                    schedule = [ SatPassExt(satName=i.satName,
                                            orb=i.orb,
                                            culmination=i.culmination,
                                            trueCulmination=i.culmination,
                                            timeStart=i.timeStart,
                                            trueTimeStart=i.timeStart,
                                            timeCulmination=i.timeCulmination,
                                            trueTimeCulmination=i.timeCulmination,
                                            timeEnd=i.timeEnd,
                                            trueTimeEnd=i.timeEnd,
                                            orbit=i.orb.get_orbit_number(i.timeCulmination),
                                            status=i.status) for i in schedule]
                    
                    collisionsIDs = set()

                    for i in range(len(schedule)):

                        if (schedule[i].timeEnd - datetime.utcnow()).total_seconds() < (self.config.beforeStartLimit + 10):
                            schedule[i].status = "old"
                            
                        elif (i > 0) and (schedule[i].timeStart < schedule[i-1].timeEnd):
                            collisionsIDs.add(i)
                            collisionsIDs.add(i-1)
                            schedule[i].status = "collision"
                            schedule[i-1].status = "collision"

                        
                        else:
                            if len(collisionsIDs):
                                
                                maxCulminationID = max(collisionsIDs, key=lambda x: schedule[x].culmination)
                                for i in collisionsIDs:
                                    if i != maxCulminationID:
                                        schedule[i].status = "banned"
                                schedule[maxCulminationID].status = "wait"
                                

                                collisionsIDs.clear()
                        
                        
                    self.schedule = list(filter(lambda x: x.status != "old", schedule))
                    self.lastScheduleUpdate = datetime.utcnow()
                    self.logger.info("Update schedule")   
        except Exception as e:
            self.logger.error(f"Unknown exception in updateScheduleTask: {e}")
            
        self.tasksManager.addTask(f"Update schedule", type="updateSchedule", timeMoment=datetime.utcnow()+timedelta(hours=6, seconds=1), target=self.updateScheduleTask)
    
        
    def preparationTask(self, *args, **kwargs):
        if "satPass" in kwargs["kwargs"].keys():
            satPass = kwargs["kwargs"]["satPass"]
        else:
            raise ValueError("Missing necessary argument satPass")
        
        name = satPass.satName
        timeStart = satPass.timeStart

        now = datetime.utcnow()

        # FIXME
        if not (now < (satPass.timeStart + timedelta(seconds=self.config.beforeStartLimit + 10))) and \
            (now < (satPass.timeEnd - timedelta(seconds=self.config.beforeStartLimit + 10))):
            timeStart = now + timedelta(seconds=10 + self.config.beforeStartLimit)

        satTrack = self.scheduler.getSateliteTrack(satPass=satPass, timestep=self.timestep)
        
        self.stopTrackingEvent.clear()
        self.stopRecordEvent.clear()

        self.logger.info(f"Preparation for {satPass.satName} at {satPass.timeStart}")
        
        # FIXME
        if self.status == "idle" and (satPass.timeEnd - datetime.utcnow()).total_seconds() > self.config.beforeStartLimit:
            self.status = "Preparation"
            self.lastScheduleUpdate = datetime.utcnow()
            self.logger.debug("Status changed to preparation by preparationTask")
            
            if self.kinematic in ["legacy", "exportTrack"]:
                self.tasksManager.addTask(name=f"exportTrack for {name}", type="exportTrack",
                                        timeMoment=(timeStart-timedelta(seconds=self.config.beforeStartLimit)), target=self.exportTrackTask, kwargs = {"satPass": satPass})
            
            elif self.kinematic != "none":
                self.tasksManager.addTask(name=f"tracking for {name}", type="tracking",
                                      timeMoment=(timeStart-timedelta(seconds=self.config.beforeStartLimit)), target=self.trackingTask, kwargs = {"satPass": satPass, "satTrack": satTrack}) 
            
        
            self.tasksManager.addTask(name=f"calibrateSDR for {name}", type="calibrateSDR",
                                    timeMoment=now, target=self.calibrateSDRTask, kwargs = {"satPass": satPass})
                
            self.tasksManager.addTask(name=f"recordSDR for {name}", type="recordSDR",
                                  timeMoment=(timeStart-timedelta(seconds=self.config.beforeStartLimit - 10)), target=self.recordSDRTask, kwargs = {"satPass": satPass})  
        else:
            # FIXME
            self.logger.error("FIXME")
  
  
    def exportTrackTask(self, *args, **kwargs) -> bool:
        if "satPass" in kwargs["kwargs"].keys():
            # FIXME
            satPass:SatPassExt = kwargs["kwargs"]["satPass"]
        else:
            raise ValueError("Missing necessary argument satPass")
        
        self.logger.info(f"ExportTrack event for {satPass.satName} at {satPass.timeStart}")
        
        if (satPass.timeEnd - datetime.utcnow()).total_seconds() < self.config.beforeStartLimit:
            self.logger.warning(f"Skipped track export event for {satPass.satName} at {satPass.timeStart.strftime('%d.%m.%Y %H:%M:%S')}: soo late")
            return False

        if self.kinematic == "legacy":
            trackFile = self.scheduler.generateTrackFile(satPass, timestep=self.timestep)
            trackPath = trackFile.trackPath

        # FIXME
        elif self.kinematic == "exportTrack":
            try:
                trackPath = self.workingPath / "tracks" / f"{satPass.satName.replace(' ', '-')}_{satPass.timeStart.strftime('%Y-%m-%dT%H-%M')}.txt"
                passLenght = (satPass.timeEnd - satPass.timeStart).total_seconds()
                
                with open(trackPath, "w") as file:
                    startTime = satPass.timeStart.strftime('%Y-%m-%d   %H:%M:%S') + " UTC"

                    metaData = f"Satellite: {satPass.satName}\n" +                                        \
                                f"Start date & time: {startTime}\n" +                                      \
                                f"Orbit: {satPass.orb.get_orbit_number(satPass.timeCulmination)}\n\n"
                    metaData += "Time (UTC)   Azimuth (deg:min)   Elevation (deg:min)\n\n"

                    ## Write metadata
                    file.write(metaData)

                    time = satPass.timeStart

                    for i in range(int(passLenght)):
                        az, el = satPass.orb.get_observer_look(time, self.lon, self.lat, self.alt)
                        file.write(f"{time.strftime('%H:%M:%S')} {az:.6f}  {el:.6f}\n")
                        time += timedelta(seconds=1)
                    
            except:
                self.logger.error(f"Failed generate track for {satPass.satName}")

        else:
            self.logger.warning(f"Unknown kinematic for export track: {self.kinematic}")

            return False

        if trackPath.is_file():
            try:
                shutil.copy(trackPath, self.exportPath / self.config.exportTrackFilename)
                return True
                        
            except Exception as e:
                self.logger.error(f"Failed copy track: {repr(e)}")
                self.logger.verbose(f"{repr(e)}: {str(e)}")
                        
        else:
            self.logger.error("Failed export track: file not exist")
        
        return False


    def trackingTask(self, *args, **kwargs) -> bool:
        try:
            with self.trackingLock:
                if "satPass" in kwargs["kwargs"].keys():
                    satPass = kwargs["kwargs"]["satPass"]
                else:
                    self.logger.error("Missing necessary argument satPass in trackingTask")
                    return False
                    
                if "satTrack" in kwargs["kwargs"].keys():
                    satTrack = kwargs["kwargs"]["satTrack"]
                else:
                    self.logger.error("Missing necessary argument satTrack in trackingTask")
                    return False
                
                self.logger.info(f"Tracking event for {satPass.satName} at {satPass.timeStart}")
                
                #satTrack = self.scheduler.getSateliteTrack(satPass, timestep=self.timestep)
                now = datetime.utcnow()
                
                if now <= satPass.timeStart:
                    passLenght = int((satPass.timeEnd - satPass.timeStart).total_seconds())
                    timeStart = satPass.timeStart
                    
                else:
                    passLenght = int((satPass.timeEnd - now).total_seconds())
                    timeStart = now
                
                # TODO add check needed time 
                if passLenght < self.config.beforeStartLimit:
                    self.logger.warning(f"Skipped tracking event for {satPass.satName} at {satPass.timeStart.strftime('%d.%m.%Y %H:%M:%S')}: soo late")
                    lateness = abs(passLenght) + self.config.beforeStartLimit
                    
                    self.logger.verbose(f"Tracking event for {satPass.satName} at {satPass.timeStart.strftime('%d.%m.%Y %H:%M:%S')} was late for {lateness}")
                    return False
                
                
                # For now, let it be so, even though it's dirty. 
                # Probably then you should do a stupid restart of the entire application or even the operating system. 
                # After replacing the settings in the file. Although this is not very convenient when changing for example adjustments
                if self.stopTrackingEvent.is_set():
                    return True
                
                
                if not self.connection.comeback():
                    return False
                
                
                if self.stopTrackingEvent.is_set():
                    return True
                
                if not self.connection.home():
                    return False           
                
                
                skiped = False
                while len(satTrack) and satTrack[0].time <= (now + timedelta(seconds=self.config.beforeStartLimit)):
                    if len(satTrack) > 1:
                        satTrack.pop(0)
                        skiped = True
                    else:
                        return False

                # FIXME 
                if skiped:
                    self.logger.info(f"Skip part of track, start from az: {satTrack[0].azimuth:.2f}, el: {satTrack[0].elevation:.2f} at {satTrack[0].time}")
                
                if self.stopTrackingEvent.is_set():
                    return True
                
                if not self.connection.navigate(satTrack[0].azimuth, satTrack[0].elevation, True, True):
                    return False  
                        
                passLenght = int((satTrack[-1].time - satTrack[0].time).total_seconds())
                sleepTime = int((satTrack[0].time - datetime.utcnow()).total_seconds())
                
                if sleepTime > 0:
                    sleep(sleepTime)
                
                # tracking
                ticks = 0
                while (ticks < (passLenght // self.timestep)):
                    if self.stopTrackingEvent.is_set():
                        self.connection.comeback()
                        self.logger.info("Tracking has been stopped")
                        return True
                
                    self.connection.navigate(satTrack[ticks].azimuth, satTrack[ticks].elevation, False)
                        
                    ticks += 1
                    
                    sleep(self.timestep)
                
                self.logger.info(f"Finish tracking {satPass.satName}")
                
                sleep(5)
                
                self.connection.comeback()
                self.connection.navigate(-self.config.azimuthCorrection, 90, fast=True)
            
            return True
        
        except Exception as e:
            self.logger.error(f"Exception in tracking task: {e}")

            return False
        
        
    def calibrateSDRTask(self, *args, **kwargs) -> bool:
        if not self.demo:
            #if self.sdr.load_config(self.schedule[0].satName):
            #    self.logger.verbose(f"Load sdr config for {self.schedule[0].satName}")

            #else:
            #    self.logger.error(f"Error load sdr config for {self.schedule[0].satName}")
            #    return False

            if "satPass" in kwargs["kwargs"].keys():
                satPass = kwargs["kwargs"]["satPass"]
            else:
                self.logger.error("Missing necessary argument satPass in calibrateSDRTask")
                return False
        
            # clear rssi log
            self.sdr.clear()
            self.sdr.calibrate(satPass.satName)
            self.lastNoiseLevel = round(self.sdr.noiseLevel, 1)

            return True

        else:
            self.logger.info("Demo mode is active, calibrateSDR skipped")

        return False


    # FIXIT need remove old passes and add status
    def recordSDRTask(self, *args, **kwargs) -> bool:
        self.status = "Recieving"
        self.lastScheduleUpdate = datetime.utcnow()
        self.logger.debug("Status changed to Recieving by recordSDRTask")
        
        try:
            #with self.recordLock:
            # FIXME
            if "satPass" in kwargs["kwargs"].keys():
                satPass = kwargs["kwargs"]["satPass"]
            else:
                self.logger.error("Missing necessary argument satPass in recordSDRTask")
                return False
                
            self.logger.info(f"RecordSDR event for {satPass.satName} at {satPass.timeStart}")
                
            now = datetime.utcnow()

            if now <= satPass.timeStart:
                passLenght = int((satPass.timeEnd - satPass.timeStart).total_seconds())
                timeStart = satPass.timeStart

            else:
                passLenght = int((satPass.timeEnd - now).total_seconds())
                timeStart = now

            if passLenght < self.config.beforeStartLimit:
                self.logger.warning(f"Skipped record task for {satPass.satName} at {satPass.timeStart.strftime('%d.%m.%Y %H:%M:%S')}: soo late")
                
                lateness = abs(passLenght) + self.config.beforeStartLimit
                self.logger.verbose(f"record task for {satPass.satName} at {satPass.timeStart.strftime('%d.%m.%Y %H:%M:%S')} was late for { lateness }")
                
                self.status = "idle"
                self.lastScheduleUpdate = datetime.utcnow()
                self.logger.debug("Status changed to idle by recordSDRTask")
                return False

            if not self.demo:
                sleepTime = ((timeStart - timedelta(seconds=5)) - now).total_seconds()
                if sleepTime > 0 :
                    sleep(sleepTime)
                        
                fileName = datetime.utcnow().strftime("%Y%m%d_%H%M%S_") + satPass.satName
                    
                self.logger.info(f"Start {satPass.satName} data recive ({passLenght} second left)")
                
                functor = AddAERecLogProcessor(satPass.orb, self.lat, self.lon, self.alt)
                
                logAdditional = {"satellite": satPass.satName,
                                "passID": fileName,
                                "passInfo": {
                                    "culmination": satPass.culmination,
                                    "timeStart": satPass.timeStart,
                                    "timeEnd": satPass.timeEnd,
                                    "TLE1": satPass.orb.tle._line1,
                                    "TLE2": satPass.orb.tle._line2,
                                    "orbit": satPass.orb.get_orbit_number(satPass.timeCulmination)
                                },
                                "stationInfo": {
                                    "name": self.stationName,
                                    "lat": self.lat,
                                    "lon": self.lon,
                                    "alt": self.alt * 1000 #FIXME convert to meters
                                }
                }
                
                self.sdr.start(passID=fileName, 
                               recordConfig=satPass.satName, 
                               timeout=passLenght, 
                               recordLogPath=self.dataPath,
                               recLogProcessor=functor,
                               logAdditional=logAdditional
                               )   
                
                timeEnd = datetime.utcnow() + timedelta(seconds= passLenght + 15)
                
                # wait sdr finish
                
                while ((timeEnd - datetime.utcnow()).total_seconds() > 0):
                    sleep(1)
                    if self.sdr.status == "idle" or self.sdr.status == "processing":
                        break                
                
            else:
                self.logger.info("Demo mode is active, recordSDR skipped. sleep")
                sleepTime = (satPass.timeEnd - now).total_seconds()
                sleep(sleepTime)

            self.status = "idle"
            self.lastScheduleUpdate = datetime.utcnow()
            self.logger.debug("Status changed to idle by recordSDRTask")

            return True
        
        except Exception as e:
            self.logger.warning(f"Exception in recordSDRTask: {e}")
        
        self.status = "idle"
        self.lastScheduleUpdate = datetime.utcnow()
        self.logger.debug("Status changed to idle by recordSDRTask")
        return False


    def watchingTask(self, *args, **kwargs):
        if self.stopWatchingEvent.is_set():
            return
        
        try:
            with self.scheduleLock:    
                now = datetime.utcnow()
                
                #FIXME
                if len(self.schedule) and self.status == "idle":
                    satPass = None
                    for i in self.schedule:
                        if i.status not in ("banned", "old", "watching"):
                            satPass = i
                            break
                            
                    if (satPass.timeStart - datetime.utcnow()).total_seconds() > self.config.beforeStartLimit:
                        if self.lastWatchMessage == None:
                            self.logger.info(getWaitMessageFor(satPass.timeStart, event=f"{satPass.satName} pass"))
                            self.lastWatchMessage = datetime.utcnow()
                                
                        elif (datetime.utcnow() - self.lastWatchMessage) > timedelta(minutes=5):
                            self.logger.info(getWaitMessageFor(satPass.timeStart, event=f"{satPass.satName} pass"))
                            self.lastWatchMessage = datetime.utcnow()
                    
                
                if self.status == "idle":    
                    for i in range(len(self.schedule)):
                        if self.stopWatchingEvent.is_set():
                            break
                        
                        # FIXME
                        if (self.schedule[i].timeEnd - now).total_seconds() < self.config.beforeStartLimit:
                            # Maybe set OLD status ?
                            self.schedule.pop(i)
                            self.lastScheduleUpdate = datetime.utcnow()
                            break
                        
                        if (self.schedule[i].timeStart - now).total_seconds() < 4*60:
                            if self.schedule[i].status != "banned" and self.schedule[i].status != "watching":
                                self.schedule[i].status = "watching"
                                self.lastScheduleUpdate = datetime.utcnow()
                                        
                                satPass = self.schedule[i]
                                name = satPass.satName
                                timeStart = satPass.timeStart
                                self.tasksManager.addTask(name=f"preparation for {name}", type="preparation", timeMoment=(timeStart - timedelta(minutes=3)), target=self.preparationTask, kwargs= {"satPass": satPass})
                                break

                        if self.schedule[i].timeEnd < now:
                            self.schedule.remove(self.schedule[i])
                            self.lastScheduleUpdate = datetime.utcnow()
                            break 
        except Exception as e:
            self.logger.error(f"Unknown error in watchingTask: {e}")   
             
        self.tasksManager.addTask(name="watchingTask", type="watching", timeMoment=datetime.utcnow() + timedelta(seconds=5), target=self.watchingTask)


    def uploadLog(self, file: Path,  ftype: str = "sdr_rssi_log", url: str = "http://eus.lorett.org/eus/log_post") -> bool:
            """
                Potentially unstable. Attempt to interact with existing infrastructure
                ONE BIG CRUTCH. I'll redo it completely when I write my own telemetry portal
            """
            #FIXME
            #self.logger.info(f"Start parse log {file}")
            try:                
                with open(file, 'rb') as f:
                    try:
                        #FIXME
                        dct = json.load(f)
                        recLog = RecordLogModel(**dct)
                        
                        form = {
                            "station": recLog.stationInfo.name,
                            "ts": f":{datetime.utcnow().isoformat()}",
                            "ftype": ftype,
                        }
                        
                        content = f"#Pass ID: {file.stem}\n"
                        content += f"#Satellite: {recLog.satellite}\n"
                        content += f"#Configuration: {recLog.recordInfo.recordConfig}\n"
                        content += f"#Start time: {recLog.passInfo.timeStart}\n"
                        
                        content += "\n"
                        
                        content += f"#LorettStation: {__version__}\n"
                        content += f"#Station: {recLog.stationInfo.name}\n"
                        content += f"#Location: {recLog.stationInfo.lon} lon {recLog.stationInfo.lat} lat\n"

                        content += f"#TLE: {recLog.passInfo.TLE1}\n"
                        content += f"#TLE: {recLog.passInfo.TLE2}\n"

                        content += "\n"

                        content += "#Time\tAz\tEl\tLevel\tSNR\n"

                        for line in recLog.recordInfo.log:
                            content += f"{line.timeMoment.strftime('%Y-%m-%d %H:%M:%S.%f')}\t{line.azimuth:.1f}\t{line.elevation:.1f}\t{line.level:.1f}\t{line.snr:.1f}\n"

                        content += f"#Closed at: {recLog.recordInfo.timeFinish}"
                        
                        io = BytesIO(content.encode("utf-8"))
                        io.name = f"{file.stem}_rec.log"
                        
                        files = {
                            "file": io
                        }
                            
                        r = post(url, files=files, data=form)
                    
                        if r.status_code == 200:
                            self.logger.info(f"Upload log success {file.name}")
                            
                            f.close()
                            renamed = file.with_name(f"Sended_{file.name}")
                            file.rename(renamed)

                            return True

                        elif r.status_code == 404:
                            self.logger.warning(f"No connection with server")
                            return False
                        
                        else:
                            self.logger.warning(f"Upload log failed {file.name} ({r.status_code})")
                            return False

                    #TODO It's probably worth rewriting
                    except ValidationError as e:
                        self.logger.error(f"Record log ValidationError for {file.name} (use verbose for see more details)")
                        self.logger.verbose(e)
                        
                        f.close()
                        renamed = file.with_name(f"ValidationError_{file.name}")
                        file.rename(renamed)

                        self.logger.warning(f"Bad record log file renamed to {renamed.name}")

                    except json.JSONDecodeError as e:
                        self.logger.error(f"Record log JSONDecodeError for {file.name} (use verbose for see more details)")
                        self.logger.verbose(e)
                        
                        f.close()
                        renamed = file.with_name(f"JSONDecodeError_{file.name}")
                        file.rename(renamed)

                        self.logger.warning(f"Bad record log file renamed to {renamed.name}")
                    
                return False
            
            except ConnectionError:
                self.logger.warning(f"Upload log failed {file.name}")
                
                return False
            
            except Exception as e:    
                self.logger.warning(f"Unknown exception in uploadLog: {type(e).__name__}")
                self.logger.verbose(f"Exception if uploadLog text: {e}")
                
                return False

    def checkLogsTask(self):
        timeDelay = 1
        
        if self.stopUloadingLogsEvent.is_set():
            return
    
        try:
            regEx = re.compile(r"^\d{8}_\d{6}_.*(.json)$")
            #                                                       ddmmYYYY_HHMMSS_SATNAME.log
            files = [i for i in self.dataPath.iterdir() if regEx.match(i.name)]
                
            for file in files:
                # LOCAL NOW! because system use local time for st_mtime
                if (datetime.now() - datetime.fromtimestamp((self.dataPath / file).stat().st_mtime)).total_seconds() > 20:
                    if self.uploadLog(self.dataPath / file, ftype="sdr_rssi_log", url=self.config.logURL):
                        timeDelay = 1
                    else:
                        timeDelay = 60
                else:
                    timeDelay = 5

        except Exception as e:
            self.logger.error(f"Error in checkLogs thread: {e}")
            timeDelay = 10
        
        self.tasksManager.addTask(name="checkLogsTask", type="checkLogsTask", timeMoment=datetime.utcnow() + timedelta(seconds=timeDelay), target=self.checkLogsTask)
    
    def getSatPosition(self, satName: str, timeMoment: datetime) -> Coordinates:
        orb = Orbital(satellite=satName, tle_file=str(self.scheduler.getTLEPath()))
        
        return Coordinates(*orb.get_lonlatalt(timeMoment))
    
    def getRecordLogModel(self, passID: str) -> RecordLogModel | None:
        file = self.dataPath / f"Sended_{passID}.json"
        
        if not file.exists():
            return None
        
        with open(file, 'rb') as f:
            try:
                dct = json.load(f)
                recLog = RecordLogModel(**dct)
                
                return recLog
            
            except ValidationError as e:
                    self.logger.error(f"Record log ValidationError for {file.name} (use verbose for see more details)")
                    self.logger.verbose(e)
                        
                    f.close()
                    renamed = file.with_name(f"ValidationError_{file.name}")
                    file.rename(renamed)

                    self.logger.warning(f"Bad record log file renamed to {renamed.name}")

            except json.JSONDecodeError as e:
                self.logger.error(f"Record log JSONDecodeError for {file.name} (use verbose for see more details)")
                self.logger.verbose(e)
                        
                f.close()
                renamed = file.with_name(f"JSONDecodeError_{file.name}")
                file.rename(renamed)

                self.logger.warning(f"Bad record log file renamed to {renamed.name}")
        
        return None
            
    def getRecordLogModels(self, offset: int, limit: int, tiny: bool = True) -> list[RecordLogModel] | list[TinyRecordLogModel]:  
        regExp = re.compile(r"^(Sended_)?\d{8}_\d{6}_.*(.json)$")
        recLogsFiles = [i for i in self.config.dataPath.iterdir() if i.is_file() and regExp.match(i.name)]
        
        out = []
        
        for file in recLogsFiles:
            with open(file, 'rb') as f:
                try:
                    dct = json.load(f)
                    
                    if tiny:
                        recLog = TinyRecordLogModel(**dct)
                    else:
                        recLog = RecordLogModel(**dct)
                    
                    out.append(recLog)
                    
                except (ValidationError, ValueError) as e:
                        self.logger.error(f"Record log ValidationError for {file.name} (use verbose for see more details)")
                        self.logger.verbose(e)
                        
                        f.close()
                        renamed = file.with_name(f"ValidationError_{file.name}")
                        file.rename(renamed)

                        self.logger.warning(f"Bad record log file renamed to {renamed.name}")

                except json.JSONDecodeError as e:
                    self.logger.error(f"Record log JSONDecodeError for {file.name} (use verbose for see more details)")
                    self.logger.verbose(e)
                        
                    f.close()
                    renamed = file.with_name(f"JSONDecodeError_{file.name}")
                    file.rename(renamed)

                    self.logger.warning(f"Bad record log file renamed to {renamed.name}")
                  
        out.sort(key=lambda model: model.passInfo.timeStart, reverse=True)
          
        return out[offset: offset+limit][::-1]
    
    def getScheduleModels(self, offset: int, limit: int) -> list[ScheduleRecordModel]:
        #FIXME ?
        with self.scheduleLock:
            return [ScheduleRecordModel(satName=i.satName, 
                                        culmination=i.culmination,
                                        trueCulmination=i.trueCulmination, 
                                        timeStart=i.timeStart, 
                                        trueTimeStart=i.trueTimeStart,
                                        timeEnd=i.timeEnd, 
                                        trueTimeEnd=i.trueTimeEnd,
                                        timeCulmination=i.timeCulmination, 
                                        trueTimeCulmination=i.trueTimeCulmination, 
                                        orbit=i.orbit,
                                        status=i.status) for i in self.schedule[offset: offset+limit]]
            
    def getSatPassModel(self, satName: str, orbit: int) -> Optional[ScheduleRecordModel]:
        if satName not in self.config.satList:
            return None
        if orbit < 0:
            return None
        
        with self.scheduleLock:
            satPass = list(filter(lambda x: (x.satName == satName) and (x.orbit == orbit), self.schedule))
            
            if len(satPass):
                satPass = satPass[0]
                return ScheduleRecordModel(satName= satPass.satName, 
                                        culmination= satPass.culmination,
                                        trueCulmination= satPass.trueCulmination, 
                                        timeStart= satPass.timeStart, 
                                        trueTimeStart= satPass.trueTimeStart,
                                        timeEnd= satPass.timeEnd, 
                                        trueTimeEnd= satPass.trueTimeEnd,
                                        timeCulmination= satPass.timeCulmination, 
                                        trueTimeCulmination= satPass.trueTimeCulmination, 
                                        orbit= satPass.orbit,
                                        status= satPass.status)
            
        return None
    
    def getSatPass(self, satName: str, orbit: int) -> Optional[SatPassExt]:
        if satName not in self.config.satList:
            return None
        if orbit < 0:
            return None
        
        with self.scheduleLock:
            satPass = list(filter(lambda x: (x.satName == satName) and (x.orbit == orbit), self.schedule))
            
            if len(satPass):
                return copy.deepcopy(satPass[0])
            
        return None
    
    def updateSatPass(self, satPass: SatPassExt) -> bool:
        #FIXME?
        
        if satPass.satName not in self.config.satList:
            return False
        if satPass.orbit < 0:
            return False
        
        if not (satPass.trueTimeStart <= satPass.timeCulmination <= satPass.trueTimeEnd):
            return False
        
        if not (satPass.trueTimeStart <= satPass.timeStart <= satPass.timeEnd <= satPass.trueTimeEnd):
            return False
        
        with self.scheduleLock:
            existsSatPass = list(filter(lambda x: (x.satName == satPass.satName) and (x.orbit == satPass.orbit), 
                                self.schedule))
            
            if len(existsSatPass):
                existsSatPass = existsSatPass[0]
                if not(satPass.trueTimeStart == existsSatPass.trueTimeStart and satPass.trueTimeEnd == existsSatPass.trueTimeEnd):
                    return False
                
                if not satPass.orb:
                    return False
                
                if satPass.trueTimeCulmination > satPass.timeEnd:
                    satPass.culmination = satPass.orb.get_observer_look(satPass.timeEnd,
                                                                        lon=self.config.lon,
                                                                        lat=self.config.lat,
                                                                        alt=self.config.alt)[1]
                elif satPass.timeStart > satPass.trueTimeCulmination:
                    satPass.culmination = satPass.orb.get_observer_look(satPass.timeStart,
                                                                        lon=self.config.lon,
                                                                        lat=self.config.lat,
                                                                        alt=self.config.alt)[1]
                
                satPass.modified = isModifiedSatPassExt(satPass)
                
                #TODO
                # Cache me
                
                #TODO add enum for statuses
                if existsSatPass.status == "watching":
                    self.stopTrackingEvent.set()
                    self.sdr.stop()
                
                passID = self.schedule.index(existsSatPass)
                self.schedule[passID] = satPass
                
                self.logger.info(f"Update satPass(satName={satPass.satName}, orbit={satPass.orbit})")
                self.lastScheduleUpdate = datetime.utcnow()
                
                return True 
        
        return False
        
    def getSatelliteTrack(self, satellite: Annotated[str, MinLen(1), MaxLen(50)],
                                timeStart: datetime,
                                timeEnd: datetime,
                                step: Annotated[float, Ge(1), Le(5)]) -> Optional[SatelliteTrackModel]:
        #TODO Add Duty orbital objects collection
        #TODO Should I add a callback object or an exception in the future?

        if satellite not in self.scheduler.config.satList:
            return None
        
        #TODO DEFINE ME SOMEWHERE
        if (timeEnd - timeStart) > timedelta(minutes=30):
            return None
        

        #FIXME
        orb = Orbital(satellite, tle_file=str(self.scheduler.tlePath / "tle.txt") )

        stepsCount = int(abs((timeEnd - timeStart).total_seconds()) // step)
        timeMoment = timeStart
        
        track = []

        for _ in range(stepsCount):
            azimuth, elevation = orb.get_observer_look(timeMoment, 
                                                       self.config.lon, 
                                                       self.config.lat, 
                                                       self.config.alt)
            
            track.append(TrackPositionSubModel(timeMoment=timeMoment, 
                                             azimuth=azimuth, 
                                             elevation=elevation))
            timeMoment += timedelta(seconds=step)
        
        return SatelliteTrackModel(satellite=satellite,
                                   timeStart=timeStart,
                                   timeEnd=timeEnd,
                                   step=step,
                                   track=track)

    def getSatelliteTrajectory(self, satellite: Annotated[str, MinLen(1), MaxLen(50)],
                                     timeStart: datetime,
                                     timeEnd: datetime,
                                     step: Annotated[float, Ge(10), Le(60)]) -> Optional[SatelliteTrajectoryModel]:
        #TODO Add Duty orbital objects collection
        #TODO Should I add a callback object or an exception in the future?

        if satellite not in self.scheduler.config.satList:
            return None
        
        #TODO DEFINE ME SOMEWHERE
        if (timeEnd - timeStart) > timedelta(hours=4):
            return None
        

        #FIXME
        orb = Orbital(satellite, tle_file=str(self.scheduler.tlePath / "tle.txt") )

        stepsCount = int(abs((timeEnd - timeStart).total_seconds()) // step)
        timeMoment = timeStart
        
        trajectory = []
        
        for _ in range(stepsCount):
            lon, lat, alt = orb.get_lonlatalt(timeMoment)
            
            trajectory.append(TrajectoryPositionSubModel(timeMoment=timeMoment, 
                                             lon=lon, 
                                             lat=lat,
                                             alt=alt))
            timeMoment += timedelta(seconds=step)
        
        return SatelliteTrajectoryModel(satellite=satellite,
                                   timeStart=timeStart,
                                   timeEnd=timeEnd,
                                   step=step,
                                   trajectory=trajectory)
    
    def getSatelliteOrbital(self, satname: str) -> Orbital | None:
        #FIXME
        try:
            return Orbital(satellite=satname, tle_file=str(self.scheduler.getTLEPath()))
        except:
            return None
        
    def getTLESatellite(self, satellite: str) -> Optional[TLEModel]:
        if satellite not in self.config.satList:
            return None
        
        tle = tlefile.read(platform=satellite, tle_file=str(self.scheduler.getTLEPath()))
        
        return TLEModel(satellite=satellite,
                        TLE1=tle._line1,
                        TLE2=tle._line2,
                        epoch=tle.epoch.astype('M8[ms]').astype(datetime),
                        age=(datetime.utcnow() - tle.epoch.astype('M8[ms]').astype(datetime)).total_seconds())

    def getTLESatellites(self) -> list[TLEModel]:
        out = []
        
        for i in self.config.satList:
            out.append(self.getTLESatellite(i))
        
        return out
    
    def updateActiveSatList(self, satList: list[str]) -> bool:
        #FIXME
        satListFull = [i.satellite for i in self.recordConfigs.values()]
        for i in satList:
            if i not in satListFull:
                return False
            
        self.logger.info(f"Update active satelites list")
        self.logger.verbose(f"{self.config.satList} -> {satList}")

        self.config.satList = satList
        self._parseSettings()
        
        schedulerConfig = SchedulerConfig(stationName=self.stationName,
                                            lat = self.lat,
                                            lon = self.lon,
                                            alt = self.alt,
                                            satList=self.satList, 
                                            horizon=self.horizon, 
                                            minApogee=self.minApogee, 
                                            stationType=self.stationType, 
                                            source=self.source,
                                            
                                            path = self.workingPath,
                                            timeZone = self.timeZone,)

        self.scheduler = Scheduler(config=schedulerConfig,
                                   logger=self.logger)
        
        self.tasksManager.addTask(f"Update schedule", type="updateSchedule", timeMoment=datetime.utcnow(), target=self.updateScheduleTask)

        return True
    
    def start(self):        
        self.stopTrackingEvent.clear()
        self.stopRecordEvent.clear()
        self.stopWatchingEvent.clear()
        self.stopScheduleUpdateEvent.clear()
        self.stopUloadingLogsEvent.clear()
        
        self.tasksManager.start()

        def waitServer():
            asyncio.run(self.masterReciever.start())

        self.tasksManager.addTask(name="initTask", type="init", timeMoment=datetime.utcnow(), target=self.initTask)
        self.tasksManager.addTask(f"Start TCP async server", type="startTCP", timeMoment=datetime.utcnow(), target=waitServer)
        self.tasksManager.addTask(name="watchingTask", type="watching", timeMoment=datetime.utcnow(), target=self.watchingTask)


    def stop(self):
        self.logger.info("Station stop")
        
        self.stopTrackingEvent.set()
        self.stopRecordEvent.set()
        self.stopWatchingEvent.set()
        self.stopScheduleUpdateEvent.set()
        
        self.tasksManager.stop()
        
        if self.masterReciever:
            asyncio.run(self.masterReciever.stop())

    def __del__(self):
        try:
            self.sdr.stop()
            self.sdr = None
        except: pass
