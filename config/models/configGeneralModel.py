from __future__ import annotations

from .configBaseModel import ConfigBaseModel
from .logging import ConfigLoggingModel
from .database import ConfigDatabaseModel
from .webServer import ConfigWebServerModel

from .rotator.platforms import SupportedPlatform, supportedPlatformsDiscriminator
from .rotator.configRotatorLorettAntennaPlatform import ConfigRotatorLorettAntennaPlatformModel
from .scheduler.configSchedulerModel import ConfigSchedulerModel

from .scheduler.defaultSatellitesLists import defaultSatellitesLists

from .service.configServiceModel import ConfigServiceModel

from pathlib import Path
from shared.types import ExpandUserPath
from pydantic import model_validator, Field

from sys import platform
import json
from typing import Any



class ConfigGeneralModel(ConfigBaseModel):
    __defaultCachePath = Path("C:\\lorett\\cache\\" if platform == "win32" else "~/lorett/cache")
    
    cachePath: ExpandUserPath = __defaultCachePath
    logging: ConfigLoggingModel = ConfigLoggingModel()
    
    webServer: ConfigWebServerModel = ConfigWebServerModel()
    database: ConfigDatabaseModel = ConfigDatabaseModel()
    rotator: SupportedPlatform = Field(discriminator=supportedPlatformsDiscriminator,
                                       default=ConfigRotatorLorettAntennaPlatformModel())
    scheduler: ConfigSchedulerModel = ConfigSchedulerModel()

    service: ConfigServiceModel = ConfigServiceModel()
    
    @model_validator(mode="after") # type: ignore
    def modelAfterValidation(self) -> ConfigGeneralModel:

        if len(self.scheduler.satellitesFull) == 0:
            self.scheduler.satellitesFull = defaultSatellitesLists[self.scheduler.defaultSatellitesList]

            self.scheduler.satellitesActive = [i.name for i in self.scheduler.satellitesFull]

        return self
    
    def save(self, path: Path) -> None:
        """
        Save the configuration to a file.

        Args:
            path (Path): Path to the file where the configuration will be saved.
        """
        with open(path, "w") as f:
            f.write(self.model_dump_json(indent=4))
    
    @classmethod
    def fromFile(cls, path: Path) -> ConfigGeneralModel:
        """
        Load the configuration from a file.

        Args:
            path (Path): Path to the file containing the configuration.

        Returns:
            ConfigMainModel: Loaded configuration model.
        """
        default  = cls()
        with open (path, "r") as f:
            data: dict[str, Any] = json.load(f)
            default.model_dump(mode="json").update(data)
            
            return cls(**data)
        
    