from pydantic import BaseModel, ConfigDict, model_validator
from typing import Optional, Self

from datetime import datetime
import pytz


class ConfigBaseModel(BaseModel): # type: ignore
    """
    Base configuration model for the application.

    Attributes:
        __lastUpdate - timestamp with last update model. Only current model
    
    
    ## Only the current model is changed, 
    ## the model containing the current model does not change its lastUpdate.
    
        Unfortunately, I haven't found how to make pydantic run model validation 
        if the model that is its field has been updated. 
        But in general it's not surprising, it's too specific.

    ## For dynamic tracking of updates in the configuration, it will be necessary to check the _lastUpdate field of all nested models.
    """
    _lastUpdate: Optional[datetime] = None 

    model_config = ConfigDict(
        from_attributes = True,
        validate_default = True,
        use_enum_values=True,
        validate_assignment=True
    )
    
    @model_validator(mode='after')
    def updateLastUpdate(self) -> Self:
        self._lastUpdate = datetime.now(tz=pytz.utc)
        return self