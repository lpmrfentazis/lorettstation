from ..configSatelliteModel import ConfigSatelliteModel
from ..scheduling.configSchedulingTLETypeModel import ConfigSchedulingTLETypeModel


LWeather: list[ConfigSatelliteModel] = [
    ConfigSatelliteModel(name="NOAA 18",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=28654
                         )),

    ConfigSatelliteModel(name="NOAA 19",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=33591
                         )),

    ConfigSatelliteModel(name="METOP-B",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=38771
                         )),

    ConfigSatelliteModel(name="METOP-C",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=43689
                         )),
                         
    ConfigSatelliteModel(name="METEOR-M2 3",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=57166
                         )),
                         
    ConfigSatelliteModel(name="METEOR-M2 4",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=59051
                         ))
]