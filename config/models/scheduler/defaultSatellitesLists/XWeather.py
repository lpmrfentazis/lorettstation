from ..configSatelliteModel import ConfigSatelliteModel
from ..scheduling.configSchedulingTLETypeModel import ConfigSchedulingTLETypeModel
from ..priority.configSatellitePriority import ConfigSatellitePriorityModel


XWeather: list[ConfigSatelliteModel] = [
    ConfigSatelliteModel(name="TERRA",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=25994
                         ),
                         priority=ConfigSatellitePriorityModel(priority=10)),

    ConfigSatelliteModel(name="AQUA",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=27424
                         ),
                         priority=ConfigSatellitePriorityModel(priority=1)),

    ConfigSatelliteModel(name="AURA",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=28376
                         ),
                         priority=ConfigSatellitePriorityModel(priority=3)),

    ConfigSatelliteModel(name="FENGYUN 3D",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=43010
                         ),
                         priority=ConfigSatellitePriorityModel(priority=2)),
                         
    ConfigSatelliteModel(name="FENGYUN 3E",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=49008
                         ),
                         priority=ConfigSatellitePriorityModel(priority=2)),
                         
    ConfigSatelliteModel(name="FENGYUN 3F",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=57490
                         ),
                         priority=ConfigSatellitePriorityModel(priority=3)),

    ConfigSatelliteModel(name="FENGYUN 3G",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=56232
                         ),
                         priority=ConfigSatellitePriorityModel(priority=2)),
                         
    ConfigSatelliteModel(name="SUOMI NPP",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=37849
                         ),
                         priority=ConfigSatellitePriorityModel(priority=2)),
                         
    ConfigSatelliteModel(name="NOAA 20 (JPSS-1)",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=43013
                         ),
                         priority=ConfigSatellitePriorityModel(priority=2)),
                         
    ConfigSatelliteModel(name="NOAA 21 (JPSS-2)",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=54234
                         ),
                         priority=ConfigSatellitePriorityModel(priority=2))
]