from ..configSatelliteModel import ConfigSatelliteModel
from ..scheduling.configSchedulingTLETypeModel import ConfigSchedulingTLETypeModel


APTWeather: list[ConfigSatelliteModel] = [
    ConfigSatelliteModel(name="NOAA 15",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=25338
                         )),

    ConfigSatelliteModel(name="NOAA 18",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=28654
                         )),

    ConfigSatelliteModel(name="NOAA 19",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=33591
                         )),

    ConfigSatelliteModel(name="METEOR-M2 3",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=57166
                         )),
                         
    ConfigSatelliteModel(name="METEOR-M2 4",
                         schedulingBy=ConfigSchedulingTLETypeModel(
                             noradID=59051
                         ))
]