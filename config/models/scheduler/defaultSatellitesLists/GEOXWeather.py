from ..configSatelliteModel import ConfigSatelliteModel
from ..scheduling.configSchedulingTLEWithScheduleTypeModel import ConfigSchedulingTLEWithScheduleTypeModel
from ..scheduling.configSchedulingStaticTypeModel import ConfigSchedulingStaticTypeModel
from ..priority.configSatellitePriority import ConfigSatellitePriorityModel


GEOXWeather: list[ConfigSatelliteModel] = [
    ConfigSatelliteModel(name="ELEKTRO-L2",
                         schedulingBy=ConfigSchedulingTLEWithScheduleTypeModel(
                             noradID=41105,
                             timestep=0
                         ),
                         priority=ConfigSatellitePriorityModel(priority=10)),

    ConfigSatelliteModel(name="ELEKTRO-L3",
                         schedulingBy=ConfigSchedulingTLEWithScheduleTypeModel(
                             noradID=44903,
                             timestep=0
                         ),
                         pority=ConfigSatellitePriorityModel(priority=10)),
                         
    ConfigSatelliteModel(name="ELEKTRO-L4",
                         schedulingBy=ConfigSchedulingTLEWithScheduleTypeModel(
                             noradID=55506,
                             timestep=0
                         ),
                         priority=ConfigSatellitePriorityModel(priority=10)),
]