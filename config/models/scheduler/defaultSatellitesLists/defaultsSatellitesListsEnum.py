from enum import StrEnum


class defaultSatellitesListsEnum(StrEnum):
    APTWeather = "APTWeather"
    LWeather = "LWeather"
    XWeather = "XWeather"
    GEOXWeather = "GEOXWeather"