from ..configSatelliteModel import ConfigSatelliteModel

from .defaultsSatellitesListsEnum import defaultSatellitesListsEnum

from .APTWeather import APTWeather
from .LWeather import LWeather
from .XWeather import XWeather
from .GEOXWeather import GEOXWeather


defaultSatellitesLists: dict[defaultSatellitesListsEnum, ConfigSatelliteModel] = {
    defaultSatellitesListsEnum.XWeather: XWeather,
    defaultSatellitesListsEnum.LWeather: LWeather,
    defaultSatellitesListsEnum.APTWeather: APTWeather,
    defaultSatellitesListsEnum.GEOXWeather: GEOXWeather
}