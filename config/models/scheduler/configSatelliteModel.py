from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel
from .priority.configSatellitePriority import ConfigSatellitePriorityModel

from annotated_types import Ge, Le, MinLen, MaxLen, Annotated
from typing import Optional
from .scheduling import SchedulingModels, schedulingModelsDiscriminator

from pydantic import Field

from shared.types import SatelliteName



class ConfigSatelliteModel(ConfigBaseModel):
    name: SatelliteName

    schedulingBy: SchedulingModels = Field(discriminator=schedulingModelsDiscriminator)

    priority: ConfigSatellitePriorityModel = ConfigSatellitePriorityModel()