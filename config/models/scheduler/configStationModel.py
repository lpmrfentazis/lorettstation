from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel

from annotated_types import Ge, Le, MinLen, MaxLen, Annotated



class ConfigStationModel(ConfigBaseModel):
    stationName: Annotated[str, MinLen(1), MaxLen(50)] = "stationName"
    
    lat: Annotated[float, Ge(-90), Le(90)] = 55.755864
    lon: Annotated[float, Ge(-180), Le(180)] = 37.617698
    alt: Annotated[float, Ge(-500), Le(9000)] = 180 # -431 is dead sea, 9000 is Everest
  