from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel
from .configSatelliteCulminationPriority import ConfigSatelliteCulminationPriorityModel
from .configSatellitePassDirectionPriority import ConfigSatellitePassDirectionPriorityModel

from annotated_types import Ge, Le, MinLen, MaxLen, Annotated



class ConfigSatellitePriorityModel(ConfigBaseModel):
    priority: int = 1 # Less = Higher
    culmination: ConfigSatelliteCulminationPriorityModel = ConfigSatelliteCulminationPriorityModel()
    passDirection: ConfigSatellitePassDirectionPriorityModel = ConfigSatellitePassDirectionPriorityModel()
    
    