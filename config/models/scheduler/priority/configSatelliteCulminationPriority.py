from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel
from .configSatellitePriorityBaseModel import ConfigSatellitePriorityBaseModel

from annotated_types import Ge, Le, MinLen, MaxLen, Annotated



class ConfigSatelliteCulminationPriorityModel(ConfigSatellitePriorityBaseModel):
    order: int = 1
    