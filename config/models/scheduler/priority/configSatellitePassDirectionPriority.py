from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel
from .configSatellitePriorityBaseModel import ConfigSatellitePriorityBaseModel

from annotated_types import Ge, Le, MinLen, MaxLen, Annotated

from enum import Enum



class Direction(str, Enum):
    west = "west"
    east = "east"



class ConfigSatellitePassDirectionPriorityModel(ConfigSatellitePriorityBaseModel):
    direction: Direction = Direction.east
    active: bool = False
    order: int = 2
    