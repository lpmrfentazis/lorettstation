from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel

from annotated_types import Ge, Le, MinLen, MaxLen, Annotated



class ConfigSatellitePriorityBaseModel(ConfigBaseModel):
    inverted: bool = False
    active: bool = True
    order: int = 0
    