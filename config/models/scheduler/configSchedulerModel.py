from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel
from .configStationModel import ConfigStationModel
from .configSatelliteModel import ConfigSatelliteModel
from .defaultSatellitesLists import defaultSatellitesListsEnum

from annotated_types import Ge, Le, Lt, Le, MinLen, MaxLen, Annotated

from typing import Self

from pydantic import model_validator, AnyHttpUrl

from shared.types import SatelliteName


class ConfigSchedulerModel(ConfigBaseModel):
    station: ConfigStationModel = ConfigStationModel()
    
    timestep: Annotated[float, Ge(0)] = 1 # 0 means that the point will be calculated once and will be held.
    horizon: Annotated[float, Ge(0), Le(90)] = 5
    minCulmination: Annotated[float, Ge(0), Le(90)] = 10
    
    # in hours
    #TODO validate me
    scheduleLength: Annotated[int, Ge(2), Le(96)] = 24
    scheduleTimer: Annotated[int, Le(1), Le(24)] = 6
    
    sources: list[AnyHttpUrl] = [
        "https://celestrak.org/NORAD/elements/gp.php?GROUP=active&FORMAT=tle"
    ]
    
    # for schedule's files
    timezone: Annotated[int, Ge(-12), Le(12)] = 0
    
    # if satellites full is list empty -> use default list
    defaultSatellitesList: defaultSatellitesListsEnum = defaultSatellitesListsEnum.LWeather
    
    satellitesFull: list[ConfigSatelliteModel] = []
    satellitesActive: list[SatelliteName] = []
    
    @model_validator(mode="after")
    def horizonAndCulminationValidator(self) -> Self:
        if not (self.horizon <= self.minCulmination):
            raise ValueError("horizon must me <= minCulmination")
        
        return self