from typing import Annotated, Union, Literal
from annotated_types import Ge, Le
from typing import Optional, Self

from config.models.configBaseModel import ConfigBaseModel
from .configSchedulingBaseTypeModel import ConfigSchedulingBaseTypeModel
from .configSchedulingByTypes import SchedulingByTypes

from datetime import time, timedelta
from pydantic import model_validator




class EveryScheduling(ConfigBaseModel):
    startingAt: time

    every: timedelta
    duration: timedelta


class ByScheduleScheduling(ConfigBaseModel): # )) make sense
    points: list[time]  # 00:00, 00:15, 00:30 etc
    duration: timedelta


class ConfigSchedulingBaseByScheduleTypeModel(ConfigSchedulingBaseTypeModel):
    schedulingBy: Literal[SchedulingByTypes.static] = SchedulingByTypes.static

    everyScheduling: Optional[EveryScheduling] = None # priority
    byScheduleScheduling: Optional[ByScheduleScheduling] = None

    @model_validator(mode="after")
    def schedulingValidator(self) -> Self:
        if self.everyScheduling is None and \
              self.schedulingValidator is None:
            raise ValueError("Scheduling type must be specified")
        
        return self