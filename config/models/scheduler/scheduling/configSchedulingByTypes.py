from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel

from annotated_types import Ge, Le, MinLen, MaxLen, Annotated
from typing import Optional
from enum import StrEnum


class SchedulingByTypes(StrEnum):
    none = "none"                       # No tracking
    TLE = "TLE"                         # Tracking by TLE by passes
    TLEWithSchedule = "TLEWithSchedule" # Tracking by TLE with specific schedule if possible. For old GEO example
    static = "static"                   # Tracking to static point with specific schedule
