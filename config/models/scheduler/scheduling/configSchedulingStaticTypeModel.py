from typing import Annotated, Union, Literal
from annotated_types import Ge, Lt, Le
from typing import Union

from .configSchedulingBaseByScheduleTypeModel import ConfigSchedulingBaseByScheduleTypeModel
from config.models.configBaseModel import ConfigBaseModel
from .configSchedulingByTypes import SchedulingByTypes


class PositionModel(ConfigBaseModel):
    azimuth: Annotated[float, Ge(0), Lt(360)]
    elevation: Annotated[float, Ge(0), Lt(90)]

class ConfigSchedulingStaticTypeModel(ConfigSchedulingBaseByScheduleTypeModel):
    schedulingBy: Literal[SchedulingByTypes.static] = SchedulingByTypes.static

    position: PositionModel

