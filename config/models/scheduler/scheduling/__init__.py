from typing import Union

from .configSchedulingBaseTypeModel import ConfigSchedulingBaseTypeModel

from .configScheduligNoneTypeModel import ConfigSchedulingNoneTypeModel
from .configSchedulingStaticTypeModel import ConfigSchedulingStaticTypeModel
from .configSchedulingTLETypeModel import ConfigSchedulingTLETypeModel
from .configSchedulingTLEWithScheduleTypeModel import ConfigSchedulingTLEWithScheduleTypeModel

SchedulingModels = Union[ConfigSchedulingNoneTypeModel,
                         ConfigSchedulingStaticTypeModel,
                         ConfigSchedulingTLETypeModel,
                         ConfigSchedulingTLEWithScheduleTypeModel]

schedulingModels = SchedulingModels.__args__
schedulingModelsDiscriminator = "schedulingBy"