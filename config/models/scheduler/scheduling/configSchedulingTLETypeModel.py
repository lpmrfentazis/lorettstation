from typing import Annotated, Union, Literal
from annotated_types import Ge, Le, MaxLen, MinLen
from typing import Optional, Self

from pydantic import AnyHttpUrl, model_validator
from .configSchedulingBaseTypeModel import ConfigSchedulingBaseTypeModel
from .configSchedulingByTypes import SchedulingByTypes
from config.models.configBaseModel import ConfigBaseModel

from datetime import timedelta

from shared.models import TLEBaseModel



class TLESource(ConfigBaseModel):
    url: Optional[AnyHttpUrl] = None

    default: Optional[TLEBaseModel] = None

       
class ConfigSchedulingTLETypeModel(ConfigSchedulingBaseTypeModel):
    schedulingBy: Literal[SchedulingByTypes.TLE] = SchedulingByTypes.TLE

    noradID: Optional[Annotated[int, Ge(0)]] # optional: priority is noradID, name use only like alias

    # use scheduler default if all none
    source: TLESource = TLESource()

    # for override scheduler settings
    timestep: Optional[Annotated[float, Ge(0)]] = None
    #   only when horizon > scheduler.horizon and etc
    horizon: Optional[Annotated[float, Ge(0), Le(90)]] = None
    minCulmination: Optional[Annotated[float, Ge(0), Le(90)]] = None

    @model_validator(mode="after")
    def horizonAndCulminationValidator(self) -> Self:
        if self.horizon and self.minCulmination:
            if not (self.horizon <= self.minCulmination):
                raise ValueError("horizon must me <= minCulmination")
        
        return self
