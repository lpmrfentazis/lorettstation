from typing import Annotated, Union
from annotated_types import Ge, Le
from typing import Union

from config.models.configBaseModel import ConfigBaseModel
from .configSchedulingByTypes import SchedulingByTypes


       
class ConfigSchedulingBaseTypeModel(ConfigBaseModel):
    schedulingBy: SchedulingByTypes = SchedulingByTypes.none
