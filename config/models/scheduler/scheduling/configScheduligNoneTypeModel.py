from typing import Annotated, Union
from annotated_types import Ge, Le
from typing import Literal

from .configSchedulingBaseTypeModel import ConfigSchedulingBaseTypeModel
from .configSchedulingByTypes import SchedulingByTypes


       
class ConfigSchedulingNoneTypeModel(ConfigSchedulingBaseTypeModel):
    schedulingBy: Literal[SchedulingByTypes.none] = SchedulingByTypes.none
