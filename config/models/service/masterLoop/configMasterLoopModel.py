from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel
from annotated_types import Ge, Le, Lt, Le, Gt, MinLen, MaxLen, Annotated



class ConfigMasterLoopModel(ConfigBaseModel):
    sleep: Annotated[float, Gt(0), Le(5)] = 0.1
    afterErrorSleep: Annotated[float, Ge(1), Le(15)] = 5