from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel
from annotated_types import Ge, Le, Lt, Le, Gt, MinLen, MaxLen, Annotated

from .masterLoop.configMasterLoopModel import ConfigMasterLoopModel



class ConfigServiceModel(ConfigBaseModel):
    masterLoop: ConfigMasterLoopModel = ConfigMasterLoopModel()