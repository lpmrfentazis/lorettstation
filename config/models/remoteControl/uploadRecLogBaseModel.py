from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel

from .platformType import RemoteControlPlatformType

from annotated_types import Ge, Le, Lt, MinLen, MaxLen, Annotated
from typing import Literal
from pydantic import AnyHttpUrl



class UploadRecLogBaseModel(ConfigBaseModel):
    fileEditingTimeout: Annotated[float, Ge(0.5), Le(60)] = 20
    fileEditingDelay: Annotated[float, Ge(0.5), Le(60)] = 5
    
    uploadSuccessfulDelay: Annotated[float, Ge(0.5), Le(60)] = 1
    uploadUnsuccessfulDelay: Annotated[float, Ge(0.5), Le(180)] = 60
    errorDelay: Annotated[float, Ge(0.5), Le(60)] = 10
    
    