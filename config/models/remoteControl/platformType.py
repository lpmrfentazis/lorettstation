from __future__ import annotations
from enum import Enum


class PlatformType(str, Enum):
    legacy = "legacy"
    