from __future__ import annotations

from config.models.configBaseModel import ConfigBaseModel

from annotated_types import Ge, Le, Lt, MinLen, MaxLen, Annotated

from .platformType import PlatformType
from .uploadRecLogBaseModel import UploadRecLogBaseModel


class ConfigRemoteControlBaseModel(ConfigBaseModel):
    platform: PlatformType = PlatformType.legacy
    
    uploadRecLog: UploadRecLogBaseModel = UploadRecLogBaseModel()
    #beforeUpload