from __future__ import annotations

from ..configRemoteControlBaseModel import ConfigRemoteControlBaseModel

from ..platformType import PlatformType

from annotated_types import Ge, Le, Lt, MinLen, MaxLen, Annotated
from typing import Literal



class ConfigRemoteControlLegacyModel(ConfigRemoteControlBaseModel):
    platform: Literal[PlatformType.legacy] = PlatformType.legacy
    
    
    