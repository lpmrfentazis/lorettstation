from ..uploadRecLogBaseModel import UploadRecLogBaseModel
from pydantic import AnyHttpUrl


class UploadRecLogLegacyModel(UploadRecLogBaseModel):
    logPostURL: AnyHttpUrl = "http://eus.lorett.org/eus/log_post"