from .configRotatorLorettAntennaLegacyPlatform import ConfigRotatorLorettAntennaLegacyPlatformModel
from .configRotatorLorettAntennaPlatform import ConfigRotatorLorettAntennaPlatformModel
from .configRotatorLorettRotatorPlatform import ConfigRotatorLorettRotatorPlatformModel
from .configRotatorNonePlatform import ConfigRotatorNonePlatformModel

from typing import Union


SupportedPlatform = Union[ConfigRotatorLorettAntennaLegacyPlatformModel,
                            ConfigRotatorLorettAntennaPlatformModel,
                            ConfigRotatorLorettRotatorPlatformModel,
                            ConfigRotatorNonePlatformModel]

supportedPlatforms = SupportedPlatform.__args__

supportedPlatformsDiscriminator = "platform"