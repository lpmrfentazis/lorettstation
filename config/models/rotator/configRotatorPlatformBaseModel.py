from ..configBaseModel import ConfigBaseModel
from .configRotatorPlatformType import ConfigRotatorPlatformType
from .connection import ConfigRotatorConnectionBaseModel
from .connection import ConfigRotatorFileExportConnectionModel

from typing import Annotated, Union
from annotated_types import Ge, Le
from pydantic import Field
from typing import Union


       
class ConfigRotatorPlatformBaseModel(ConfigBaseModel):
    platform: ConfigRotatorPlatformType = ConfigRotatorPlatformType.lorettAntenna
    connection: ConfigRotatorConnectionBaseModel = ConfigRotatorFileExportConnectionModel()
    getTelemetryTimeout: Annotated[float, Ge(0), Le(5)] = 0 # no telemetry/feedback
