from .configRotatorPlatformBaseModel import ConfigRotatorPlatformBaseModel
from .configRotatorPlatformType import ConfigRotatorPlatformType
from .connection import ConfigRotatorSerialConnectionModel

from typing import Annotated, Literal
from annotated_types import Ge, Le


       
class ConfigRotatorLorettRotatorPlatformModel(ConfigRotatorPlatformBaseModel):
    platform: Literal[ConfigRotatorPlatformType.lorettRotator] = ConfigRotatorPlatformType.lorettRotator
    connection: ConfigRotatorSerialConnectionModel = ConfigRotatorSerialConnectionModel()
    getTelemetryTimeout: Annotated[float, Ge(0), Le(5)] = 0.5
