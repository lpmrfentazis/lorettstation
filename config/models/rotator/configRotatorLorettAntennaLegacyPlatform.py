from .configRotatorPlatformBaseModel import ConfigRotatorPlatformBaseModel
from .configRotatorPlatformType import ConfigRotatorPlatformType
from .connection import ConfigRotatorFileExportConnectionModel

from typing import Annotated, Literal
from annotated_types import Ge, Le

from pathlib import Path
from sys import platform



class ConfigRotatorLorettAntennaLegacyPlatformModel(ConfigRotatorPlatformBaseModel):
    __filePath = Path("C:\\lorett\\tracks\\Track.txt" if platform == "win32" else "~/lorett/tracks/Track.txt")     
    
    platform: Literal[ConfigRotatorPlatformType.lorettAntennaLegacy] = ConfigRotatorPlatformType.lorettAntennaLegacy
    connection: ConfigRotatorFileExportConnectionModel = ConfigRotatorFileExportConnectionModel(timeBeforeStart=80, filePath=__filePath)
