from .configRotatorPlatformBaseModel import ConfigRotatorPlatformBaseModel
from .configRotatorPlatformType import ConfigRotatorPlatformType
from .connection import ConfigRotatorFileExportConnectionModel

from typing import Annotated, Literal
from annotated_types import Ge, Le


       
class ConfigRotatorLorettAntennaPlatformModel(ConfigRotatorPlatformBaseModel):
    platform: Literal[ConfigRotatorPlatformType.lorettAntenna] = ConfigRotatorPlatformType.lorettAntenna
    connection: ConfigRotatorFileExportConnectionModel = ConfigRotatorFileExportConnectionModel()
