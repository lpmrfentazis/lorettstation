from enum import Enum


class ConfigRotatorPlatformType(str, Enum):
    none = "none"
    lorettAntenna = "lorettAntenna"
    lorettAntennaLegacy = "lorettAntennaLegacy"
    lorettRotator = "lorettRotator"    