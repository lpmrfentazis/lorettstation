from .configRotatorConnectionType import ConfigRotatorConnectionType
from .configRotatorConnectionBaseModel import ConfigRotatorConnectionBaseModel

from typing import Annotated, Optional
from annotated_types import Ge, Le
from pydantic import Field
from typing import Literal, Union


    
class ConfigRotatorNoneConnectionModel(ConfigRotatorConnectionBaseModel):
    type: Literal[ConfigRotatorConnectionType.none] = ConfigRotatorConnectionType.none
    timeBeforeStart: Annotated[int, Ge(0), Le(120)] = 0