from .configRotatorConnectionType import ConfigRotatorConnectionType
from .configRotatorConnectionBaseModel import ConfigRotatorConnectionBaseModel

from typing import Annotated, Optional
from annotated_types import Ge, Le
from pydantic import Field
from typing import Literal, Union


    
class ConfigRotatorSerialConnectionModel(ConfigRotatorConnectionBaseModel):
    type: Literal[ConfigRotatorConnectionType.serial] = ConfigRotatorConnectionType.serial
    timeout: Annotated[float, Ge(0), Le(5)] = 1
    timeBeforeStart: Annotated[int, Ge(0), Le(120)] = 0
    
    port: str = "auto"
    baudrate: int = 115200
    description: Optional[str] = "Pico - Board CDC"
    deviceVID: Optional[int] = 0