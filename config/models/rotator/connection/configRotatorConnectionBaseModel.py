from config.models.configBaseModel import ConfigBaseModel
from .configRotatorConnectionType import ConfigRotatorConnectionType

from annotated_types import Annotated, Ge, Le


class ConfigRotatorConnectionBaseModel(ConfigBaseModel):
    type: ConfigRotatorConnectionType = ConfigRotatorConnectionType.none
    timeBeforeStart: Annotated[int, Ge(0), Le(120)] = 0
    timeout: Annotated[float, Ge(0), Le(5)] = 0 # 0 => no real connection
