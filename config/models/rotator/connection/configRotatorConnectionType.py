from enum import Enum

class ConfigRotatorConnectionType(str, Enum):
    none = "none"
    fileExport = "fileExport"
    serial = "serial"
