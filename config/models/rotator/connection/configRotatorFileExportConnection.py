from __future__ import annotations

from .configRotatorConnectionType import ConfigRotatorConnectionType
from .configRotatorConnectionBaseModel import ConfigRotatorConnectionBaseModel

from typing import Annotated, Literal
from annotated_types import Ge, Le
from shared.types import ExpandUserPath

from pydantic import model_validator

from sys import platform
from pathlib import Path

    
class ConfigRotatorFileExportConnectionModel(ConfigRotatorConnectionBaseModel):
    type: Literal[ConfigRotatorConnectionType.fileExport] = ConfigRotatorConnectionType.fileExport
    timeBeforeStart: Annotated[int, Ge(0), Le(120)] = 20
    
    filePath: ExpandUserPath = Path("C:\\lorett\\tracks\\TrackF.txt" if platform == "win32" else "~/lorett/tracks/TrackF.txt")
    
    @model_validator(mode="after") # type: ignore
    def modelAfterValidation(self) -> ConfigRotatorFileExportConnectionModel:
        
        self.filePath.parent.mkdir(exist_ok=True, parents=True)
        
        return self