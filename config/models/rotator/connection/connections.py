from .configRotatorNoneConnection import ConfigRotatorNoneConnectionModel
from .configRotatorFileExportConnection import ConfigRotatorFileExportConnectionModel
from .configRotatorSerialConnection import ConfigRotatorSerialConnectionModel

from typing import Union


SupportedConnectionType = Union[ConfigRotatorNoneConnectionModel, 
                   ConfigRotatorSerialConnectionModel, 
                   ConfigRotatorFileExportConnectionModel]

supportedConnectionTypes = SupportedConnectionType.__args__
supportedConnectionTypeDiscriminator = "type"