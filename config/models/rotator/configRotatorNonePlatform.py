from .configRotatorPlatformBaseModel import ConfigRotatorPlatformBaseModel
from .configRotatorPlatformType import ConfigRotatorPlatformType
from .connection import ConfigRotatorNoneConnectionModel

from typing import Annotated, Literal
from annotated_types import Ge, Le


       
class ConfigRotatorNonePlatformModel(ConfigRotatorPlatformBaseModel):
    platform: Literal[ConfigRotatorPlatformType.none] = ConfigRotatorPlatformType.none
    connection: ConfigRotatorNoneConnectionModel = ConfigRotatorNoneConnectionModel()
