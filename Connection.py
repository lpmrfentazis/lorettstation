from time import sleep
import serial
from serial.tools import list_ports
from serial.serialutil import SerialException
from datetime import datetime, timedelta
from Logger import Logger, DEBUG


class Connection:
    def __init__(self, logger : Logger, 
                       port : str,
                       baudrate : int = 38400,
                       azimuthCorrection: int = 0,
                       device: str = "PicoArduino - Board CDC",
                       timeout : float = 0.5) -> None:

        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        self.logger = logger
        self.device = device
        
        #print(3)
        
        self.serial = None

        self.azimuthCorrection = azimuthCorrection
        
        # Current orientation
        self.lastAzimuth = 0
        self.cource = 0
        self.lastElevation = 0
        self.connected: bool = False

        self.logger.info("init serial Connection class")


    def connect(self) -> bool:
        try:
            if self.port == "auto":
                for unit in list_ports.comports():
                    if self.device in unit.description:
                        self.port = unit.device # get port 
                        self.logger.info(f"The {self.device} device is detected automatically on {self.port}")
                        break
                    
                else:
                    self.logger.critical(f"The {self.device} device could not be detected. Perhaps the controller is not connected")
                    raise exit()

            self.serial = serial.Serial(port = self.port, baudrate = self.baudrate, timeout = self.timeout)
            self.logger.info(f"Successful connection with {self.port} {self.baudrate} b/s")
            
            self.connected = True

        except SerialException:
            self.logger.error(f"Failed connection with {self.port}")
            self.connected = False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.connect(): {e}")
            self.connected = False

        return self.connected
    
    def getPower(self) -> float:
        try:
            # =========================================
            self.serial.write('$pow;\n'.encode('UTF-8'))
            self.logger.debug(f'Send data: $pow;\n')
            # =========================================
            res = self.waitPowerInfo(10)
            
            #self.logger.verbose(f"Power info: {} {} {}")
            
        except SerialException:
            ...
        except Exception as e:
            ...

    def home(self) -> bool:
        # search for the home position
        try:
            # =========================================
            self.serial.write('$h;\n'.encode('UTF-8'))
            self.logger.debug(f'Send data: $h;\n')
            # =========================================
            
            self.logger.info("Go to home")
            
            res = self.waitOK(60)
            
            if res:
                self.lastAzimuth = 0
                self.lastElevation = 0

            self.logger.info("In home")

            return res

        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.home(): {e}")

            return False


    def comeback(self) -> bool:
        # returns the antenna to the home position
        self.logger.info("Untangling the wire")

        try:
            # =========================================
            self.serial.write('$cb;\n'.encode('UTF-8'))
            self.logger.debug(f'Send data: $cb;\n')
            # =========================================
            
            res = self.waitOK(60)
            
            if res:
                self.lastAzimuth = 0

            return res
        
        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.comeback(): {e}")
            
            return False

    def waitPowerInfo(self, timeout: int) -> bool:
        try:
            # waiting for the antenna to be infected with the target
            # for home, comeback, navigate if timeout=True, navigate relative 
            mes = ''
            timeStart = datetime.now()

            self.logger.debug("Start wait lowlevel answer")
            while 'OK' not in mes and int((datetime.now() - timeStart).total_seconds()) <= timeout:
                try:
                    mes = self.serial.readline()
                    mes = mes.decode('UTF-8').strip()
                    if len(mes):
                        self.logger.spam(mes)
                except:
                    self.logget.warning(f"Recived bad data {mes}")    
                sleep(0.01)

            if 'OK' in mes:
                self.logger.debug("Recived OK with lowlevel")
                return True

            self.logger.warning(f"Timeout exceeded - {timeout}")
            return False

        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.waitOK(): {e}")
            
            return False
    
    def waitOK(self, timeout: int) -> bool:
        try:
            # waiting for the antenna to be infected with the target
            # for home, comeback, navigate if timeout=True, navigate relative 
            mes = ''
            timeStart = datetime.now()

            self.logger.debug("Start wait lowlevel answer")
            while 'OK' not in mes and int((datetime.now() - timeStart).total_seconds()) <= timeout:
                try:
                    mes = self.serial.readline()
                    mes = mes.decode('UTF-8').strip()
                    if len(mes):
                        self.logger.spam(mes)
                except:
                    pass
                    #self.logger.warning(f"Recived bad data {mes}")    
                sleep(0.01)

            if 'OK' in mes:
                self.logger.debug("Recived OK with lowlevel")
                return True

            self.logger.warning(f"Wait OK timeout exceeded - {timeout}")
            return False

        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.waitOK(): {e}")
            
            return False

    def setDirectionPriority(self, azimuthDirection: int, elevationDirection: int):
        # only for XZ_inclin
        if azimuthDirection not in (0, 1, -1):
            azimuthDirection = 0
        if elevationDirection not in (1, -1):
            elevationDirection = 1

        mes = f'$sdp {azimuthDirection} {elevationDirection};\n'.encode('UTF-8')
        self.serial.write(mes)
        self.logger.debug(f'Send data: {mes}')



    def navigate(self, azimuth: float, elevation: float, fast: bool, timeout: bool = False) -> bool:
        # point antenna to coordinates
        try:
            # =========================================
            if fast:
                mes = f'$nf {((azimuth + self.azimuthCorrection) % 360):.2f} {elevation:.2f};\n'.encode('UTF-8')
            
            else: 
                mes = f'$n {((azimuth + self.azimuthCorrection) % 360):.2f} {elevation:.2f};\n'.encode('UTF-8')
            
            
            self.serial.write(mes)
            self.logger.debug(f'Send data: {mes} (true Azimuth: {azimuth})')
            # =========================================
            
            if timeout:
                res = self.waitOK(25)

                if res:
                    self.lastAzimuth = azimuth
                    self.lastElevation = elevation

                return res
            
            else:
                self.lastAzimuth = azimuth
                self.lastElevation = elevation
                return True
        
        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.navigate(): {e}")
            
            return False


    def navigateRelative(self, azimuth: float, elevation: float, correction: bool) -> bool:
        # movement of engines relative to the current position
        try:
            # =========================================
            if correction:
                mes = f'$nrc {azimuth:.2f} {elevation:.2f};\n'.encode('UTF-8')

            else:
                mes = f'$nr {azimuth:.2f} {elevation:.2f};\n'.encode('UTF-8')
                
            self.serial.write(mes)
            self.logger.debug(f'Send data: {mes}')
            # =========================================
            
            deg = max([azimuth, elevation])
            
            res = self.waitOK(deg * 2)

            if res:
                self.lastAzimuth += azimuth
                self.lastElevation += elevation

            return res
        
        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.navigateRel(): {e}")
            
            return False

    def navigateRelativeMath(self, azimuth: float, elevation: float) -> bool:
        # Movement of antenna relative to the current position
        try:
            
            mes = f'$nrm {azimuth:.2f} {elevation:.2f};\n'.encode('UTF-8')
                
            self.serial.write(mes)
            self.logger.debug(f'Send data: {mes}')

            deg = max([azimuth, elevation])
            
            res = self.waitOK(deg * 2)

            if res:
                self.lastAzimuth += azimuth
                self.lastElevation += elevation

            return res
        
        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.navigateRel(): {e}")
            
            return False
        
    def stop(self) -> bool:
        try:
            self.serial.write('$stop;\n'.encode('UTF-8'))
            self.logger.debug(f'Send data: $stop;\n')

            return True 
        
        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.waitOK(): {e}")
            
            return False


    def clearCorrection(self) -> bool:
        try:
            self.serial.write('$c;\n'.encode('UTF-8'))
            self.logger.debug(f'Send data: $c;\n')

            return self.waitOK(3)
        
        except SerialException:
            self.logger.error(f"Lost connection with {self.port}")

            return False

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Connection.waitOK(): {e}")
            
            return False
        
    
    def getGPSCoordinates(self):
        try:
            self.serial.write('$gps;\n'.encode('UTF-8'))
            self.logger.debug(f'Send data: $gps;\n')
            
            mes = ""

            timeStart = datetime.now()
            
            while (datetime.now() - timeStart < timedelta(seconds=2)) and mes.count("|") != 6:
                mes = self.serial.readline().decode('UTF-8').strip()
                if len(mes):
                    self.logger.spam(mes)

            try:
                if mes.count("|") == 6:
                    data = mes.split("|")

                    lat = float(data[1]) / 1000000
                    lon = float(data[2]) / 1000000
                    alt = float(data[3]) / 100

                    accuracy = int(data[4])
                    satellites = int(data[5])

                    gps = GPS(lat, lon, alt, accuracy, satellites)

                    return gps
            except:
                self.logger.warning("Bad data from gps")
            

            return None

        except SerialException:
            self.logger.error(f"Lost orientation connection with {self.port}")

            return None

        except Exception as e:
            self.logger.critical(f"Unexpected exception with Orientation.getGPS(): {e}")

            return None


    def disconnect(self, destructor: bool = False) -> bool:
        if isinstance(self.serial, serial.Serial):
            if self.serial.is_open:
                self.logger.info(f"Disconnect with {self.port}")
                self.serial.close()
            else:
                self.logger.warning(f"Attempt to disconnect again with {self.port}")
            return True

        if not destructor:
            self.logger.warning("Trying to disconnect before connecting")

        return False

    
    def __del__(self) -> None:
        self.disconnect(destructor=True)





if __name__ == '__main__':
    portName = '/dev/ttyACM0'
    con = Connection(Logger("test", "./", DEBUG), port=portName)
    
    con.connect()
    con.home()
    con.navigate(10, 10, True)
    
    
        
