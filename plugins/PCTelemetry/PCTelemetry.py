from psutil._common import sdiskpart, sdiskusage, scpufreq

from cpuinfo import get_cpu_info
from dataclasses import dataclass
from requests.exceptions import ConnectionError, ConnectTimeout, ReadTimeout

import platform
import psutil
from typing import Optional

import socket



@dataclass
class CPU:
    name: str = ""
    arch: str = ""
    usage: float = 0
    frequencies: scpufreq = None
    temperature: float = None

@dataclass  
class RAM:
    total: int = 0
    used: int = 0
    free: int = 0
    percent: float = 0

@dataclass
class DiskInfo:
    disk: sdiskpart
    usage: sdiskusage

class PCTelemetry:
    def __init__(self):
        # get OS info
        # =========================================
        self.uname: platform.uname_result = platform.uname()
        
        self.os = f"{self.uname.system} {self.uname.release}"
        
        # TODO add macos check
        if self.uname.system == "Linux":
            try:
                self.distro = platform.freedesktop_os_release()
            except OSError:
                self.distro = {}
                
            if 'PRETTY_NAME' in self.distro:
                self.os = self.distro['PRETTY_NAME']
            
        # =========================================
        
        self.python = platform.python_version()
        
        # get hardware info
        # =========================================
        self.cpuDictInfo = get_cpu_info()
        
        self.temperatures: dict = {}
        self.battery: Optional[float] = None
        
        self.cpu = CPU(name=self.cpuDictInfo["brand_raw"], arch=self.uname.machine, frequencies=psutil.cpu_freq())
        self.ram = RAM()
        
        # Need ignore snap's bullshit
        # Need ignore windows bullshit
        #FIXME
        self.disks: list[DiskInfo] = [DiskInfo(i, psutil.disk_usage(i.mountpoint)) 
                                               for i in psutil.disk_partitions(all=False) 
                                               if ("snap" not in i.mountpoint) and \
                                               ("Containers" not in i.mountpoint)]
        
        self.internet: bool = False
        
        
    def update(self):
        self.cpu.frequencies = psutil.cpu_freq()
        self.cpu.usage = psutil.cpu_percent()
        
        if self.uname.system == "Linux":
            try:
                self.temperatures = psutil.sensors_temperatures()
                self.cpu.temperature = sum([i.current for i in self.temperatures['coretemp']]) / len(self.temperatures['coretemp'])
                
            except (AttributeError, KeyError):
                ...
        
        try:
            self.battery = psutil.sensors_battery().percent 
        except (AttributeError, KeyError):
                ...
                
        tmp =  psutil.virtual_memory()
        self.ram.total = tmp.total
        self.ram.free = tmp.free
        self.ram.used = tmp.used
        self.ram.percent = tmp.percent
        
        # Need ignore snap's bullshit
        # Need ignore windows bullshit
        self.disks: list[DiskInfo] = [DiskInfo(i, psutil.disk_usage(i.mountpoint)) 
                                               for i in psutil.disk_partitions(all=False) 
                                               if ("snap" not in i.mountpoint) and \
                                               ("Containers" not in i.mountpoint)]
        # FIXME 
        # A crutch, to implement ping google dns
        try:
            _ = socket.create_connection(("8.8.8.8", 53), timeout=0.5)
            
        except (OSError):
            self.internet = False
            
        else:
            self.internet = True
        
    def getDict(self):
        out = {"os": self.os,
                "python": self.python,
                "CPU": {"name": self.cpu.name,
                        "arch": self.cpu.arch,
                        "usage": self.cpu.usage,
                        "frequencies": {"min": self.cpu.frequencies.min,
                                        "max": self.cpu.frequencies.max,
                                        "current": self.cpu.frequencies.current}
                        },
                "RAM": {"total": self.ram.total,
                        "used": self.ram.used,
                        "free": self.ram.free,
                        "percent": self.ram.percent},
                "disks": { i.disk.mountpoint: {"fstype": i.disk.fstype, 
                                             "total": i.usage.total, 
                                             "used": i.usage.used,
                                             "free": i.usage.free,
                                             "percent": i.usage.percent} 
                          for i in self.disks},
                "temperatures": {i: sum([i.current for i in self.temperatures[i]]) / len(self.temperatures[i])
                                 for i in self.temperatures},
                "internet": self.internet,
                "battery": self.battery
            }
        
        if self.uname.system == "Linux":
            out["CPU"]["temperature"] = self.cpu.temperature
            
        return out


if __name__ == "__main__":
    t = PCTelemetry()
    
    t.update()
    
    from pprint import pprint
    pprint(t.getDict())
    