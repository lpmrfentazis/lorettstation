from docopt import docopt
from datetime import datetime, timedelta
from verboselogs import VerboseLogger, VERBOSE
from threading import Thread, Lock, Event
from pathlib import Path
from sys import platform
from time import sleep, gmtime
from requests import Session

from typing import Callable, Annotated

import json
from pyorbital.orbital import Orbital

from logging import StreamHandler, Formatter
from subprocess import Popen, DEVNULL, TimeoutExpired
from dataclasses import dataclass, field

# try import Coloredlogs in getLogger



USAGE = '''
SDRReader.py - Script for recording signal by meteorolog satellite.
Example:
  SDRReader.py --sp '\$PATH' --s 'Noaa 19' --t 100
  SDRReader.py -h | --help
  ***Don't forget about " or ' then you have space in naming. Also you can see example, use --h or -help then you start program.
Usage:
  SDRReader.py [--sp='<satDumpPath>']  [--s='<name>'] [--t=<sec>] [--p=<name>]
  SDRReader.py -h | --help
Options:
  -h, --help             Show correct format parameters.
  --s='<name>'             Name of satellite you recording [default: calibr].
  --t=<sec>  filename            Time recording track your satellite [default: 120].
  --p=<name>             Name of tracks path [default: tracks].
  --sp=<name>             Name of satdump path [default: $PATH].
  '''

supportedSDR = [
    "airspy",
    "rtlsdr",
    "bladerf",
    # Not tested, but supported by satdump:
    #"mirisdr"
    #"airspyhf",
    #"hackrf",
    #"limesdr",
    #"plutosdr",
]

# linux cant use unlink for dirs
def rmtree(f: Path):
    if f.is_file():
        f.unlink()
    else:
        for child in f.iterdir():
            rmtree(child)
        f.rmdir()
        
def dirSize(root: Path):
    return sum(f.stat().st_size for f in root.glob('**/*') if f.is_file())


def getLogger() -> VerboseLogger:
    logger = VerboseLogger("logger")
    
    handler = StreamHandler()
    Formatter.converter = gmtime
    streamformat = Formatter(
            "%(asctime)s [%(levelname)s] - %(message)s")
    
    handler.setFormatter(streamformat)
    
    logger.addHandler(handler)
    
    logger.setLevel(VERBOSE)
    
    try:                
        from coloredlogs import install
        install(logger=logger, level=VERBOSE, fmt="%(asctime)s [%(levelname)s] - %(message)s")
        
    except ImportError:
        pass
    
    return logger


@dataclass
class RecordConfig:
    satellite: str
    frequency: float
    sampleRate: float
    gain: float
    
    format: str
    
    frequencyOffset: float = 0
    
    pipeline: str = "" # only for live
    demodTag: str = "" # only for live
    timeLimit: int = 240 # seconds. only for live
    minSize: float = 30 # only for live. Size of output dir
    biasTee: bool = False
    
@dataclass
class RecordLogMessage:
    timeMoment: datetime
    currentLevel: float
    signalLevel: float


defaultLConfig = {
    "NOAA 18": RecordConfig(satellite="NOAA 18", frequency=1707.0e6, sampleRate=3.0e6, gain=18, format="s8", pipeline="noaa_hrpt", demodTag="pm_demod", minSize=20),
    "NOAA 19": RecordConfig(satellite="NOAA 19", frequency=1698.0e6, sampleRate=3.0e6, gain=18, format="s8", pipeline="noaa_hrpt", demodTag="pm_demod", minSize=20),
    
    "METOP-B": RecordConfig(satellite="METOP-B", frequency=1701.3e6, sampleRate=6.0e6, gain=18, format="s8", pipeline="metop_ahrpt", demodTag="psk_demod", minSize=20),
    "METOP-C": RecordConfig(satellite="METOP-C", frequency=1701.3e6, sampleRate=6.0e6, gain=18, format="s8", pipeline="metop_ahrpt", demodTag="psk_demod", minSize=20),
    
    # "METEOR-M2 2": RecordConfig(satellite="METEOR-M2 2", frequency=1700.0e6, sampleRate=6.0e6, gain=18, format="s8", pipeline="meteor_hrpt", demodTag="pm_demod", minSize=15),
    "METEOR-M2 3": RecordConfig(satellite="METEOR-M2 3", frequency=1700.0e6, sampleRate=3.0e6, gain=18, format="s8", pipeline="meteor_hrpt", demodTag="pm_demod", minSize=15),
    "METEOR-M2 4": RecordConfig(satellite="METEOR-M2 4", frequency=1700.0e6, sampleRate=3.0e6, gain=18, format="s8", pipeline="meteor_hrpt", demodTag="pm_demod", minSize=15),
    
    "ARCTIC WEATHER SATELLITE": RecordConfig(satellite="ARCTIC WEATHER SATELLITE", frequency=1707.0e6, sampleRate=3.0e6, gain=18, format="s8", pipeline="aws_pfm", demodTag="psk_demod", minSize=10),
    #"FENGYUN 3C": RecordConfig(satellite="FENGYUN 3C", frequency=1701.4e6, sampleRate=6.0e6, gain=18, format="s8", pipeline="fengyun3_c_ahrpt", demodTag="psk_demod", minSize=20),
}

defaultAPTConfig = {
    "NOAA 15": RecordConfig(satellite="NOAA 15", frequency=137.62e6, sampleRate=3.0e6, gain=16, format="s8", pipeline="noaa_apt", demodTag="pm_demod", biasTee=True, minSize=2),
    "NOAA 18": RecordConfig(satellite="NOAA 18", frequency=137.9125e6, sampleRate=3.0e6, gain=16, format="s8", pipeline="noaa_apt", demodTag="pm_demod", biasTee=True, minSize=2),
    "NOAA 19": RecordConfig(satellite="NOAA 19", frequency=137.1e6, sampleRate=3.0e6, gain=16, format="s8", pipeline="noaa_apt", demodTag="pm_demod", biasTee=True, minSize=2),
    
    # "METEOR-M2 2": RecordConfig(satellite="METEOR-M2 2", frequency=137.1e6, sampleRate=3.0e6, gain=18, format="s8", pipeline="meteor_m2-x_lrpt", demodTag="psk_demod", biasTee=True, minSize=0),
    "METEOR-M2 3": RecordConfig(satellite="METEOR-M2 3", frequency=137.9e6, sampleRate=3.0e6, gain=16, format="s8", pipeline="meteor_m2-x_lrpt", demodTag="psk_demod", biasTee=True, minSize=0),
    "METEOR-M2 4": RecordConfig(satellite="METEOR-M2 4", frequency=137.9e6, sampleRate=3.0e6, gain=16, format="s8", pipeline="meteor_m2-x_lrpt", demodTag="psk_demod", biasTee=True, minSize=0),
}

defaultXConfig = {
    # "TERRA": RecordConfig(satellite="TERRA", frequency=8212.5e6, frequencyOffset= -6950e6, sampleRate=35e6+1e6, gain=16, format="s8", pipeline="terra_db", demodTag="terra_db_demod", minSize=-1),
    "AQUA": RecordConfig(satellite="AQUA", frequency=8160e6, frequencyOffset= -6950e6, sampleRate=15e6+1e6, gain=16, format="s8", pipeline="aqua_db", demodTag="psk_demod", minSize=300),
    "AURA": RecordConfig(satellite="AURA", frequency=8160e6, frequencyOffset= -6950e6, sampleRate=15e6+1e6, gain=16, format="s8", pipeline="aura_db", demodTag="psk_demod", minSize=-1),
    
    "FENGYUN 3D": RecordConfig(satellite="FENGYUN 3D", frequency=7820e6, frequencyOffset= -6950e6, sampleRate=45e6+1e6, gain=16, format="f32", pipeline="fengyun3_d_ahrpt", demodTag="psk_demod", timeLimit=600, minSize=-1),
    "FENGYUN 3E": RecordConfig(satellite="FENGYUN 3E", frequency=7860e6, frequencyOffset= -6950e6, sampleRate=50e6+1e6, gain=16, format="f32", pipeline="fengyun3_e_ahrpt", demodTag="psk_demod", timeLimit=600, minSize=-1),
    "FENGYUN 3F": RecordConfig(satellite="FENGYUN 3F", frequency=7790e6, frequencyOffset= -6950e6, sampleRate=50e6+1e6, gain=16, format="f32", pipeline="fengyun3_f_ahrpt", demodTag="psk_demod", timeLimit=600, minSize=-1),
    "FENGYUN 3G": RecordConfig(satellite="FENGYUN 3G", frequency=7790e6, frequencyOffset= -6950e6, sampleRate=12e6+1e6, gain=16, format="f32", pipeline="fengyun3_g_ahrpt", demodTag="psk_demod", timeLimit=600, minSize=200),
    
    "SUOMI NPP": RecordConfig(satellite="SUOMI NPP", frequency=7812e6, frequencyOffset= -6950e6, sampleRate=25e6+1e6, gain=16, format="f32", pipeline="npp_hrd", demodTag="psk_demod", minSize=400),
    "NOAA 20 (JPSS-1)": RecordConfig(satellite="NOAA 20 (JPSS-1)", frequency=7812e6, frequencyOffset= -6950e6, sampleRate=25e6+1e6, gain=16, format="f32", pipeline="npp_hrd", demodTag="psk_demod", minSize=400),
    "NOAA 21 (JPSS-2)": RecordConfig(satellite="NOAA 21 (JPSS-2)", frequency=7812e6, frequencyOffset= -6950e6, sampleRate=40e6+1e6, gain=16, format="f32", pipeline="jpss_hrd", demodTag="psk_demod", minSize=-1),
    
    "METOP-B": RecordConfig(satellite="METOP-B", frequency=7800e6, frequencyOffset= -6950e6, sampleRate=45e6+1e6, gain=16, format="f32", pipeline="metop_dump", demodTag="psk_demod", minSize=-1),
    "METOP-C": RecordConfig(satellite="METOP-C", frequency=7800e6, frequencyOffset= -6950e6, sampleRate=45e6+1e6, gain=16, format="f32", pipeline="metop_dump", demodTag="psk_demod", minSize=-1),
    
    # need check sampleRate
    "PVCC": RecordConfig(satellite="PVCC", frequency=8175.5e6, frequencyOffset= -6950e6, sampleRate=80e6+1e6, gain=16, format="f32", pipeline="psk_demod", demodTag="", minSize=-1),
    
    # "METEOR-M2 2": RecordConfig(satellite="METEOR-M2 2", frequency=8320e6, frequencyOffset= -6950e6, sampleRate=80e6+1e6, gain=16, format="f32", pipeline="meteor_m_dump_wide", demodTag="psk_demod", minSize=-1),
    "METEOR-M2 3": RecordConfig(satellite="METEOR-M2 3", frequency=8320e6, frequencyOffset= -6950e6, sampleRate=80e6+1e6, gain=16, format="f32", pipeline="meteor_m_dump_wide", demodTag="psk_demod", minSize=-1),
    "METEOR-M2 4": RecordConfig(satellite="METEOR-M2 4", frequency=8320e6, frequencyOffset= -6950e6, sampleRate=80e6+1e6, gain=16, format="f32", pipeline="meteor_m_dump_wide", demodTag="psk_demod", minSize=-1),
    
    # "ARKTIKA-M 1": RecordConfig(satellite="ARKTIKA-M 1", frequency=7865e6, frequencyOffset= -6950e6, sampleRate=80e6+1e6, gain=16, format="f32", pipeline="arktika_rdas", demodTag="psk_demod", minSize=-1),
    #"ARKTIKA-M 2": RecordConfig(satellite="ARKTIKA-M 2", frequency=7865e6, frequencyOffset= -6950e6, sampleRate=80e6+1e6, gain=16, format="f32", pipeline="arktika_rdas", demodTag="psk_demod", minSize=-1),
}

supportedBands = {
    "L": defaultLConfig,
    "X": defaultXConfig,
    "APT": defaultAPTConfig
}
    
@dataclass
class SDRConfig:
    lat: float
    lon: float
    alt: float
    
    statusPort: int
    
    satDumpPath: Path
    
    dataPath: Path
    
    sdrSource: str = "airspy"
    recordConfigs: dict = field(default_factory= lambda: defaultLConfig)
    
    packToZIP: bool = False
    
    logger: VerboseLogger = getLogger()
    
    def check(self, silent: bool = False) -> bool:
        result = 0
        
        if self.sdrSource not in supportedSDR:
            result += 1
            if not silent:
                self.logger.error(f"Given {self.lat} sdrSoure is not supported")
                
        
        # I was thinking about returning messages with incorrect parameters, 
        # but decided that it was redundant
        # validate coordinates
        if not (-90 <= self.lat <= 90):
            result += 1
            if not silent:
                self.logger.error(f"Incorrect latitude. Most be in [-90, 90], but given {self.lat}")
                
        if not (-180 <= self.lon <= 180):
            result += 1
            if not silent:
                self.logger.error(f"Incorrect longitude. Most be in [-180, 180], but given {self.lon}")
        
        # -431 is dead sea, 9000 is Everest
        if not (-431 <= self.lat <= 9000):
            result += 1
            if not silent:
                self.logger.error(f"Incorrect altitude. Most be in [-431, 9000], but given {self.alt}")
        
        if not (0 <= self.statusPort <= 65535):
            result += 1
            if not silent:
                self.logger.error(f"Incorrect statusPort. Most be in [0, 65535], but given {self.statusPort}")
                
        return not result
    
RecLogProcessor = Callable[[RecordLogMessage], list | tuple]
"""
    Function or Functor that will help you add additional values to the recordLog entry.
        
    Mast return a serialized collection!      
"""

def defaultRecLogProcessor(line: RecordLogMessage) -> tuple[str, float, float]:
    return line.timeMoment.isoformat(), line.currentLevel, line.signalLevel


class SDR:   ## Soapy based SDR class
    def __init__(self, config: SDRConfig) -> None:
        self.level: float = 0.0
        
        self.level: float = 0.0
        self.noiseLevel: float = 0.0
        self.signalLevel: float = 0.0
        
        self.dataPath: Path = config.dataPath
        
        self.config: SDRConfig = config
        
        self.logger: VerboseLogger = self.config.logger
        
        # Platform check
        if platform == "win32":
            self.satDump = "satdump.exe"
        else:
            self.satDump = "satdump"
        
        self.recordThread: Thread = None
        self.recordLock: Lock = Lock()
        self.process = None
        
        self.recordStopEvent: Event = Event()
        self.recordStopEvent.clear()
        
        # last log finished record
        self.recordLog: dict = {}
        
        self.status = "idle"
        
    def calibrate(self, satellite: str) -> None:
        if satellite not in self.config.recordConfigs.keys():
            self.logger.error(f"Unknown satellite: {satellite} in calibrate")
            return
        
        satConfig = self.config.recordConfigs[satellite]
        
        if satConfig.demodTag != "":
            self.logger.info("Calibration skiped, because used live demodulation mode")
        
    def record(self, passID: str, 
                recordConfig: str,
                satConfig: RecordConfig, 
                timeout: int, 
                saveLogFile: bool = True,
                recordLogPath: Path = Path(), 
                recLogProcessor: RecLogProcessor = defaultRecLogProcessor,
                logAdditional: dict = {}) -> None:
        #FIXME do better code format... idk.. looks bad
        try:
            self.status = "record"
                    
            url = f"http://localhost:{self.config.statusPort}/api"

            self.recordLog = logAdditional.copy()
            
            satDump = self.config.satDumpPath / self.satDump if self.config.satDumpPath != "$PATH" else self.satDump
            
            # for fix wait of start sdr
            timeout += 3
                    
            # record
            if satConfig.pipeline == "":
                command = f'"{satDump}" record "{self.dataPath / passID}" --source {self.config.sdrSource} --samplerate {satConfig.sampleRate} --frequency {satConfig.frequency + satConfig.frequencyOffset} --baseband_format {satConfig.format} --general_gain {satConfig.gain} --finish_processing --http_server 0.0.0.0:{self.config.statusPort} --fft_enable --fft_rate 300 --timeout {timeout}'    
            # live
            else:
                command = f'"{satDump}" live {satConfig.pipeline} "{self.dataPath / "decoded" / passID}" --source {self.config.sdrSource} --samplerate {satConfig.sampleRate} --frequency {satConfig.frequency + satConfig.frequencyOffset} --general_gain {satConfig.gain} --http_server 0.0.0.0:{self.config.statusPort} --fft_enable --fft_rate 300 --timeout {timeout}'
                
            if satConfig.biasTee:
                command += f" --bias"

            if "NOAA" in satConfig.satellite:
                command += " --finish_processing"
                
            self.logger.info("SatDump command: " + command)
            
            # self.recordLog can be overwritten if you start recording before the end of processing
            recordInfo = {"frequency": satConfig.frequency,
                            "recordConfig": recordConfig,
                            "frequencyOffset": satConfig.frequencyOffset,
                            "sampleRate": satConfig.sampleRate,
                            "gain": satConfig.gain,
                            "pipeline": satConfig.pipeline,
                            "timeFinish": datetime.utcnow(),
                            "log": []}
            
            if "recordInfo" in self.recordLog.keys():
                # the |= operator merges dictionaries and replaces overlapping values
                self.recordLog["recordInfo"] |= recordInfo
                
            else:
                self.recordLog["recordInfo"] = recordInfo

            with self.recordLock:
                with Popen(command, stdout=DEVNULL, stderr=DEVNULL, cwd=self.config.satDumpPath, shell=True) as process:
                    self.process = process
                    self.logger.info(f"Init {self.config.sdrSource} for {satConfig.satellite} on {satConfig.frequency / 10e5} MHz as {satConfig.format}")
                                    
                    # wait init sdr 
                    sleep(5)
                    
                    final = datetime.utcnow() + timedelta(seconds=timeout)
                    counter = 0
                    
                    with Session() as s:
                        while (final - datetime.utcnow()).total_seconds() > 2:
                            try:
                                if self.recordStopEvent.is_set():
                                    self.logger.info(f"SDR record stopped")
                                    return
                                    
                                response = s.get(url, timeout=1)
                                
                                # Check if the request was successful (status code 200)
                                if response.status_code == 200:
                                    # Parse the JSON data from the response content
                                    json = response.json()
                                    
                                    self.level = sum( json.get("fft_values", [float("-inf")]) ) / len( json.get("fft_values", [1]) )
                                    
                                    if satConfig.pipeline == "":
                                        self.signalLevel = self.level - self.noiseLevel
                                    
                                    elif satConfig.pipeline == "noaa_apt":
                                        spectr = json.get("fft_values", [self.level])
                                        middle = len(spectr) // 2

                                        if len(spectr) > 3:
                                            middle = len(spectr) // 2
                                            self.signalLevel = (sum(spectr[middle-1: middle+2]) / 3) - self.level
                                        else:
                                            self.signalLevel = 0

                                    else:
                                        self.signalLevel = json.get(satConfig.demodTag, {"snr": 0}).get("snr", 0)
                                    
                                    mes = RecordLogMessage(datetime.utcnow(), self.level, self.signalLevel)
                                    
                                    # Creating a RecordLogMessage and then unpacking slows down, but makes the code easier to modify.
                                    if counter >= 1:
                                        self.recordLog["recordInfo"]["log"].append(recLogProcessor(mes))
                                        self.logger.info(f"{self.level:.1f}\t{self.signalLevel:.1f}")
                                        
                                        counter = 0
                                
                                else:
                                    self.logger.error(f"Failed to retrieve status from satdump. Status code: {response.status_code}")
                                
                            except Exception as e:
                                self.logger.error(f"Exception in record method: {e}")
                            
                            sleep(0.1)
                            counter += 0.1
                    
                    # if satdump started with a delay
                    try:
                        code = process.wait(10)
                    except:
                        ...
                    
                    if saveLogFile: 
                        self.saveRecordLog(recordLogPath / (passID + ".json"))
                    
            self.status = "processing"
                    
            # Processing the cadu. Because satdump continues to occupy the web server while finish_processing is running
            # if recording is completed but processing is not
            if satConfig.pipeline != "":
                
                # only NOAA hrpt using raw16 format ( 
                suffix = ".raw16" if satConfig.pipeline == "noaa_hrpt" else ".cadu"
                inputFormat = "frames" if satConfig.pipeline == "noaa_hrpt" else "cadu"
                caduFile = (self.dataPath / "decoded" / passID / satConfig.pipeline).with_suffix(suffix)
                
                command = f'"{ self.satDump }" {satConfig.pipeline} {inputFormat} "{ caduFile }" "{ self.dataPath / "decoded" / passID }"'
                
                with Popen(command, stdout=DEVNULL, stderr=DEVNULL, cwd=self.config.satDumpPath, shell=True) as process:
                    self.process = process
                    self.logger.info(f"Start processing {satConfig.satellite} using {satConfig.pipeline}")
                    
                    try:
                        self.process.wait(satConfig.timeLimit)
                    except:
                        self.logger.warning(f"Processing took longer than the specified timeLimit ({satConfig.timeLimit} s)")
                        
                        #FIXME
                        try:
                            self.process.wait(satConfig.timeLimit)
                        except:
                            self.logger.error(f"Processing took too longer. Terminated")
                            self.process.kill()
                        
                
                if caduFile.stat().st_size / 1024 / 1024 < satConfig.minSize:
                    self.logger.info(f"No data for {passID}")
                    rmtree(self.dataPath / "decoded" / passID)
                
                else:
                    self.logger.info(f"Decoding successful {passID}")
                                            
            else:
                out = (self.dataPath / passID)
                if out.exists():
                    out.rename(out.with_suffix(".iq"))
                    
            self.status = "idle"        
            
        except Exception as e:
            self.logger.error(f"Exception in SDR.record: {e}")        
            
        self.recordStopEvent.set()
      
    def saveRecordLog(self, path: Path) -> None:
        try:
            with open(path, "w") as f:
                json.dump(self.recordLog, f, indent=2, default=str)
                
            self.logger.info(f"Save record log to {path}")
        
        except Exception as e:
            self.logger.error(f"Exception in SDR.saveRecordLog: {e}")
      
    def start(self, passID: str, 
              recordConfig: str, 
              timeout: int,  
              saveLogFile: bool = True,
              recordLogPath: Path = Path(), 
              recLogProcessor: RecLogProcessor = defaultRecLogProcessor,
              logAdditional: dict = {}) -> None:
        #FIXME add better annotation for logLineProcessor 
        '''
            logLineProcessor:
            
            Function that will help you add additional values to the recordLog entry.
            
            Mast return a serialized collection!
        '''
        
        if self.recordThread and self.recordThread.is_alive():
            self.logger.warning("Attempting to restart while work is still in progress")
            return
            
        if recordConfig not in self.config.recordConfigs.keys():
            self.logger.error(f"Unknown satellite: {recordConfig}")
            return
        
        self.recordThread = Thread(target=self.record, kwargs={"passID": passID, 
                                                             "recordConfig": recordConfig,
                                                             "satConfig": self.config.recordConfigs[recordConfig], 
                                                             "timeout": timeout, 
                                                             "saveLogFile": saveLogFile, 
                                                             "recordLogPath": recordLogPath, 
                                                             "recLogProcessor": recLogProcessor, 
                                                             "logAdditional": logAdditional}, daemon=True)
        
        self.clear()
        self.recordThread.start()
    
    def join(self) -> None:
        self.recordThread.join()
        
    def stop(self) -> None:
        self.status = "idle"
        if self.process:
            self.process.terminate()
            
        self.recordStopEvent.set()
        
        self.recordThread.join()
        
    def clear(self) -> None:
        #self.level: float = 0.0
        self.recordLog: dict = {}
        #self.noiseLevel: float = 0.0
        self.signalLevel: float = 0.0
        
        self.recordStopEvent.clear()
        
    def isStarted(self) -> bool:
        return not self.recordStopEvent.is_set()
        
        
if __name__ == "__main__":
    opts = docopt(USAGE)
    
    satellite = opts['--s']
    timeout = int(opts['--t'])
    path = opts['--p']
    satDumpPath = opts["--sp"]
    
    config = SDRConfig(lat=None, lon=None, alt=None, satDumpPath=Path(satDumpPath), statusPort=8080) 
    
    sdr = SDR(config)
    
    filename = datetime.utcnow().strftime("%Y%m%d_%H%M%S_") + satellite
    
    sdr.start(passID=filename, recordConfig=satellite, timeout=timeout, dataPath=Path(path), recordLogPath=Path(path), )
    sdr.join()
    
