from .almurnet import packets
#from .SDR import SDR
from verboselogs import VerboseLogger

import asyncio

class MasterReciever:
    def __init__(self, logger: VerboseLogger, host: str = "0.0.0.0", 
                 port: int = 6002, maxClients=20, 
                 connectionTimeout: int = 20*60*60) -> None:
        self.recievers: list = []
        self.connections: dict = {}  # Словарь для хранения времени подключения каждого клиента
        self.lastExtensionTime: dict = {}

        self.host = host
        self.port = port
        self.maxClients = maxClients
        self.semaphore = asyncio.Semaphore(self.maxClients)
        self.connectionTimeout = connectionTimeout  # Таймаут в секундах (20 часов)
        
        self.logger = logger
        self.server = None

    def addReciever(self, reciever) -> bool:
        #TODO: Реализовать валидацию
        self.recievers.append(reciever)
        return True

    async def _closeConnection(self, addr: str, writer: asyncio.StreamReader):
        self.logger.verbose(f"Connection {addr} closed")
        self.connections.pop(addr, None)
        
        writer.close()
    
    async def _handleClient(self, reader, writer):
        addr = writer.get_extra_info('peername')
        self.logger.verbose(f"New tcp client connected: {addr}")

        self.connections[addr] = asyncio.Event()
        self.connections[addr].set()  # Изначально устанавливаем событие

        """try:
            # FIXME add packets.TelemetryInitPacket bytes size
            data = await asyncio.wait_for(reader.readexactly(9), timeout=60)
            data = packets.TelemetryRequestPacket.fromBytes(data) 
            
            
            if data != packets.TelemetryInitPacket:
                self.logger.verbose(f"Bad init packet from {addr}")
                self._closeConnection(addr=addr, writer=writer)
            
            writer.write(b'.\xfdi\xb6\x04\x00\x08\x00/\x00\x00\x00\x00\x00\x00\x00')
            await writer.drain()    
            
        except asyncio.TimeoutError:
            self.logger.verbose(f"Timeout waiting init from {addr}.")
            self._closeConnection(addr=addr, writer=writer)"""
        
        try:
            while True:
                    
                if self.connectionTimeout != -1:
                    try:
                        await asyncio.wait_for(self.connections[addr].wait(), timeout=self.connectionTimeout)
                    
                    except asyncio.TimeoutError:
                        await self._closeConnection(addr=addr, writer=writer)
                        break
                    
                if reader.at_eof():
                    await self._closeConnection(addr=addr, writer=writer)
                    break
                
                data = await reader.read(100)     
                
                ping = None

                if data:
                    #self.logger.info(data)
                    try:
                        ping = packets.TelemetryPingPacket.fromBytes(data)
                        self.connections[addr].set()
                        
                        if len(self.recievers):
                            level = self.recievers[0].level * 10
                            snr = self.recievers[0].signalLevel * 10
                            packet = packets.TelemetryPacket(level1=int(level), 
                                                             level2=int(level), 
                                                             snr=int(snr))
                            
                            writer.write(packet.toBytes())
                            await writer.drain()
                        else:
                            writer.write(packets.ConfirmPacket(clientPacketID=clientPacketID, result=packets.ResultCodes.unknown).toBytes())
                            await writer.drain()
                        
                    except KeyError as e:
                        self.logger.verbose(e)   
                        clientPacketID = 0
                        if isinstance(ping, packets.TelemetryPingPacket):
                            clientPacketID = ping.packetID

                        writer.write(packets.ConfirmPacket(clientPacketID=clientPacketID, result=packets.ResultCodes.unknown).toBytes())
                        await writer.drain()

                else:
                    writer.write(packets.ConfirmPacket(clientPacketID=0, result=packets.ResultCodes.unknown).toBytes())
                    await writer.drain()
                        
        except (OSError, ):
            self.logger.verbose(f"Client {addr} close connection")
        
        except Exception as e:
            self.logger.error(f"{e} in MasterReciever")

    async def start(self):
        self.server = await asyncio.start_server(
            self._handleClient, self.host, self.port)

        addr = self.server.sockets[0].getsockname()
        self.logger.info(f'TCP server started in {self.host}:{self.port}')

        async with self.server:
            await self.server.serve_forever()
        
    async def stop(self):
        #FIXME ?
        if self.server is not None:
            self.server.close()
            await self.server.wait_closed()
            self.server = None
            self.connections = {}
            self.lastExtensionTime: dict = {}
            self.logger.info("TCP server stopped")

    async def startServer(self) -> bool:
        if self.statusServer:
            return True

        return False
    
    def __del__(self):
        if self.server:
            self.server.close()